<?php get_header() ?>
<?php while(have_posts()) : the_post(); ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading single-post-heading">
            <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <span>Publicado el <?php the_date(); ?></span>
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php if ( have_rows( 'contenido' ) ): ?>
<section class="section single-post-content-area">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'contenido' ) ) : the_row(); ?>
            <?php if ( get_row_layout() == 'texto_basico' ) : ?>
            <div class="block-content wysiwyg">
                <?php the_sub_field( 'texto_content' ); ?>
            </div>
            <?php elseif ( get_row_layout() == 'galeria_imagenes' ) : ?>
            <div class="slider-area">
                <?php if ( have_rows( 'slider_gal' ) ) : ?>
                <a href="#" id="gal-arrow-prev" class="arrow prev"><i class="icon-chevron-left"></i></a>
                <a href="#" id="gal-arrow-next" class="arrow next"><i class="icon-chevron-right"></i></a>
                <div class="slider-galeria">
                    <?php while ( have_rows( 'slider_gal' ) ) : the_row(); ?>
                    <div class="slide">
                        <?php $imagen_gal = get_sub_field( 'imagen_gal' ); ?>
                        <div class="imagen-gal cover" style="background-image: url(<?php echo $imagen_gal['url']; ?>);"
                            title="<?php echo $imagen_gal['alt']; ?>"></div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <?php if ( have_rows( 'slider_gal' ) ) : ?>
                <div class="slider-nav-galeria">
                    <?php while ( have_rows( 'slider_gal' ) ) : the_row(); ?>
                    <div class="slide">
                        <?php $imagen_gal = get_sub_field( 'imagen_gal' ); ?>
                        <div class="imagen-gal cover" style="background-image: url(<?php echo $imagen_gal['url']; ?>);"
                            title="<?php echo $imagen_gal['alt']; ?>"></div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="section comentarios-post">
    <div class="wrap-xl">
        <?php if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif; ?>
    </div>
</section>
<?php endwhile; ?>
<script>
$(document).ready(function() {
    $('.slider-galeria').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-nav-galeria'
    });
    $('.slider-nav-galeria').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-galeria',
        focusOnSelect: true
    });
    $('#gal-arrow-prev').click(function(e) {
        e.preventDefault();
        $('.slider-galeria').slick('slickPrev');
    });
    $('#gal-arrow-next').click(function(e) {
        e.preventDefault();
        $('.slider-galeria').slick('slickNext');
    });
});
</script>
<?php get_footer() ?>
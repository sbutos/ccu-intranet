<?php
/*
* Template Name: Manual de Marcas
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="m-marcas-area">
            <div class="m-marcas-box big-area">
                <div class="ref-image-area">
                    <?php $imagen_rel_marca = get_field( 'imagen_rel_marca' ); ?>
                    <?php if ( $imagen_rel_marca ) { ?>
                    <img src="<?php echo $imagen_rel_marca['url']; ?>" alt="<?php echo $imagen_rel_marca['alt']; ?>" />
                    <?php } ?>
                </div>
                <div class="m-marcas-info">
                    <h2><?php the_field( 'titulo_marca' ); ?></h2>
                    <div class="description">
                        <?php the_field( 'bajada_marca' ); ?>
                    </div>
                    <div class="button-area">
                        <?php $archivo_marca = get_field( 'archivo_marca' ); ?>
                        <?php $urlMarca = wp_get_attachment_url( $archivo_marca ); ?>
                        <a href="<?php echo $urlMarca; ?>" target="_blank" download
                            class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                class="icon-download"></i><span>Descargar</span></a>
                    </div>
                </div>
            </div>
            <div class="grid-column-2 gap-m">
                <div class="m-marcas-box">
                    <div class="ref-image-area">
                        <div class="icono">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                        </div>
                    </div>
                    <div class="m-marcas-info">
                        <div class="heading">
                            <?php
                            $archivo_template = get_field( 'archivo_template' );
                            $urlTemplate = wp_get_attachment_url( $archivo_template );
                            $titleTemplate = get_the_title( $archivo_template );
                            $filesizeTemplate = filesize( get_attached_file( $archivo_template ) );
                            $filesizeTemplate = size_format($filesizeTemplate, 2);
                            $path_infoTemplate = pathinfo( get_attached_file( $archivo_template ) );
                            ?>
                            <h2><?php the_field( 'titulo_template' ); ?></h2>
                            <span class="size"><?php echo $path_infoTemplate['extension']; ?>
                                <?php echo $filesizeTemplate; ?></span>
                        </div>
                        <div class="description">
                            <p><?php the_field( 'bajada_firma' ); ?></p>
                        </div>
                        <div class="button-area">
                            <a href="<?php echo $urlTemplate; ?>"
                                class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                    class="icon-download"></i><span>Descargar</span></a>
                        </div>
                    </div>
                </div>
                <div class="m-marcas-box">
                    <div class="ref-image-area">
                        <div class="icono">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                        </div>
                    </div>
                    <div class="m-marcas-info">
                        <div class="heading">
                            <?php
                            $archivo_firma = get_field( 'archivo_firma' );
                            $urlFirma = wp_get_attachment_url( $archivo_firma );
                            $titleFirma = get_the_title( $archivo_firma );
                            $filesizeFirma = filesize( get_attached_file( $archivo_firma ) );
                            $filesizeFirma = size_format($filesizeFirma, 2);
                            $path_infoFirma = pathinfo( get_attached_file( $archivo_firma ) );
                            ?>
                            <h2><?php the_field( 'titulo_firma' ); ?></h2>
                            <span class="size"><?php echo $path_infoFirma['extension']; ?>
                                <?php echo $filesizeFirma; ?></span>
                        </div>
                        <div class="description">
                            <?php the_field( 'bajada_firma' ); ?>
                        </div>
                        <div class="button-area">
                            <a href="<?php echo $urlFirma; ?>"
                                class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                    class="icon-download"></i><span>Descargar</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
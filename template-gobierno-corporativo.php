<?php
/*
* Template Name: Gobierno Corporativo
*/
get_header();
?>
<section class="section gob-content-area">
    <div class="wrap-xl">
        <div class="head-page">
            <h1><?php the_title(); ?></h1>
            <div class="bajada">
                <p><?php the_field( 'introduccion' ); ?></p>
            </div>
        </div>
        <div class="wysiwyg">
            <?php the_field( 'bajada' ); ?>
        </div>
    </div>
</section>
<?php if ( have_rows( 'documentos_descargables' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'documentos_descargables' ) ) : the_row(); ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_sub_field( 'titulo_caja_doc' ); ?></h3>
            </div>
            <?php if ( have_rows( 'documentos' ) ) : ?>
            <div class="files-area">
                <?php while ( have_rows( 'documentos' ) ) : the_row(); ?>
                <div class="file-box small">
                    <div class="file-size">
                        <div class="icono">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                        </div>
                        <div class="data">
                            <?php
                            $archivo_doc = get_sub_field( 'archivo_doc' );
                            $urlDoc = wp_get_attachment_url( $archivo_doc );
                            $titleDoc = get_the_title( $archivo_doc );
                            $filesizeDoc = filesize( get_attached_file( $archivo_doc ) );
                            $filesizeDoc = size_format($filesizeDoc, 2);
                            $path_infoDoc = pathinfo( get_attached_file( $archivo_doc ) );
                            ?>
                            <span class="size"><?php echo $path_infoDoc['extension']; ?>
                                <?php echo $filesizeDoc; ?></span>
                        </div>
                    </div>
                    <div class="file-info">
                        <div class="file-main-data">
                            <h4 class="file-name"><?php the_sub_field( 'titulo_doc' ); ?></h4>
                        </div>
                        <div class="file-link">
                            <a href="<?php echo $urlDoc; ?>"
                                class="btn is-verde size-xs is-rounded is-bordered has-icon"><i
                                    class="icon-download"></i><span>Descargar</span></a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if ( have_rows( 'directorio' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'directorio' ) ) : the_row(); ?>
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_sub_field( 'titulo_caja_dir' ); ?></h3>
            </div>

            <div class="sliders-area">
                <?php if ( have_rows( 'directorio_gc' ) ) : ?>
                <div class="directorio">
                    <div id="directorio-carousel" dir="rtl">
                        <?php while ( have_rows( 'directorio_gc' ) ) : the_row(); ?>
                        <div class="slide">
                            <div class="person-box">
                                <?php $foto_dir = get_sub_field( 'foto_dir' ); ?>
                                <div class="person-img cover"
                                    style="background-image: url(<?php echo $foto_dir['url']; ?>)"
                                    title="<?php echo $foto_dir['alt']; ?>">
                                    <div class="veil"></div>
                                    <div class="blur"></div>
                                </div>
                                <div class="person-info">
                                    <span class="cargo"><?php the_sub_field( 'cargo_dir' ); ?></span>
                                    <span class="nombre"><?php the_sub_field( 'nombre_dir' ); ?></span>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( have_rows( 'directorio_gc' ) ) : ?>
                <div class="single-person">
                    <div id="single-person-slider" dir="rtl">
                        <?php while ( have_rows( 'directorio_gc' ) ) : the_row(); ?>
                        <?php $foto_dir_b = get_sub_field( 'foto_dir' ); ?>
                        <div class="slide">
                            <div class="person-box">
                                <div class="person-img cover"
                                    style="background-image: url(<?php echo $foto_dir_b['url']; ?>)"
                                    title="<?php echo $foto_dir_b['alt']; ?>">
                                    <div class="veil"></div>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php if ( have_rows( 'directorio_gc' ) ) : ?>
                <div class="bio">
                    <div id="bio-slider" dir="rtl">
                        <?php while ( have_rows( 'directorio_gc' ) ) : the_row(); ?>
                        <div class="slide">
                            <div class="person-box">
                                <div class="person-info">
                                    <span class="cargo"><?php the_sub_field( 'cargo_dir' ); ?></span>
                                    <span class="nombre"><?php the_sub_field( 'nombre_dir' ); ?></span>
                                    <p><?php the_sub_field( 'bio_dir' ); ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <div id="directorio-arrows" class="arrows">
                        <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                        <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<script>
$(document).ready(function() {
    $('#directorio-carousel').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        focusOnSelect: true,
        asNavFor: '#single-person-slider,#bio-slider',
        rtl: true
    });
    $('#single-person-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#directorio-carousel,#bio-slider',
        dots: false,
        centerMode: false,
        arrows: false,
        rtl: true
    });
    $('#bio-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '#directorio-carousel,#single-person-slider',
        dots: false,
        centerMode: false,
        arrows: false,
        rtl: true
    });
    $('#directorio-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#directorio-carousel').slick('slickPrev');
                $('#bio-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#directorio-carousel').slick('slickNext');
                $('#bio-slider').slick('slickNext');
            });
        }
    });
});
</script>
<?php get_footer(); ?>
<?php
    require_once("../../../wp-load.php");

    $pais = $_POST["pais"];
    $tipo = $_POST['tipo'];
    $subtipo = $_POST['subtipo'];

    $posts_marcas = array();

    $args = array(
                'post_type' => array('marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy'),
                'posts_per_page' => -1
            );

    if($pais != '*'){
        $paisFilter = array(
            array(
                'taxonomy' => 'pais',
                'field'    => 'slug',
                'terms'    => $pais,
            )
        );
        $args['tax_query'] = $paisFilter;
    }
    
    if($tipo != '*'){
        $tipoFilter = array(
            array(
                'taxonomy' => 'tipo',
                'field'    => 'slug',
                'terms'    => $tipo,
            )
        );
        $args['tax_query'] = $tipoFilter;
    }

    if($subtipo != '*'){
        $subtipoFilter = array(
            array(
                'taxonomy' => 'tipo',
                'field'    => 'slug',
                'terms'    => $subtipo,
            )
        );
        $args['tax_query'] = $subtipoFilter;
    }
    

    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) :
        $text = "";

        while ( $the_query->have_posts() ) : $the_query->the_post();
            $nameMarca = get_the_title();
            $introMarca = get_field( 'intro_marca' );
            $logo = get_field( 'logo' );
            $text.= '<a href="'.get_the_permalink().'" class="marca-box">
                        <div class="marca-info">
                            <h5 class="name">'.$nameMarca.'</h5>
                            <p class="description">'.$introMarca.'</p>
                            <span class="btn size-s is-verde is-rounded is-bordered is-transparent">Ver
                                Producto</span>
                        </div>
                        <div class="marca-data">
                            <div class="img">
                                <img src="'.$logo['url'].'" alt="'.$logo['alt'].'"
                                    class="logo">
                            </div>
                            <span class="name">'.$nameMarca.'</span>
                        </div>
                    </a>';
        endwhile;
        
        $posts_marcas[0] = $the_query->post_count;
        $posts_marcas[1] = $text;
        wp_reset_query();
    else:
        $posts_marcas[0] = 0;
        $posts_marcas[1] = '<p class="content-not-found">'.__('No hay eventos', 'cultura-unab').'</p>';
    endif;


echo json_encode($posts_marcas);
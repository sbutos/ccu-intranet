<?php
/*
* Template Name: Ser CCU
*/
get_header();
?>

<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div id="about-us-boxes">
                <div class="flex justify-between align-center">
                    <div class="page-title-area">
                        <h1 title="Ser CCU">Ser</h1><img
                            src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu-c.svg" alt="">
                    </div>
                    <div class="about-box is-verde size-1 horizontal-box">
                        <div class="corner-title">
                            <span><?php the_field( 'etiqueta_verde' ); ?></span>
                        </div>
                        <div id="p1" class="box-content modal-trigger" data-id="modal-p1">
                            <div class="head-box-area">
                                <span class="numero text-right">P1</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p1' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content text-right">
                                <p class="font-11"><?php the_field( 'bajada_p1' ); ?>
                                </p>
                            </div>
                        </div>
                        <div id="p2" class="box-content modal-trigger" data-id="modal-p2">
                            <div class="head-box-area">
                                <span class="numero text-right">P2</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p2' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <p class="font-11"><?php the_field( 'bajada_p2' ); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flex justify-between align-center">
                    <div class="about-box is-burdeo size-1 vertical-box">
                        <div class="corner-title">
                            <span><?php the_field( 'etiqueta_burdeo' ); ?></span>
                        </div>
                        <div id="p3" class="box-content modal-trigger" data-id="modal-p3">
                            <div class="head-box-area">
                                <span class="numero text-right">P3</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p3' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <?php the_field( 'bajada_p3' ); ?>
                            </div>
                        </div>
                        <div id="p4" class="box-content modal-trigger" data-id="modal-p4">
                            <div class="head-box-area">
                                <span class="numero text-right">P4</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p4' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <?php the_field( 'bajada_p4' ); ?>
                            </div>
                        </div>
                    </div>
                    <div class="about-box is-celeste size-2 vertical-box">
                        <div class="corner-title">
                            <span><?php the_field( 'etiqueta_celeste' ); ?></span>
                        </div>
                        <div class="flex justify-between align-center horizontal-box">
                            <div id="p5" class="box-content modal-trigger" data-id="modal-p5">
                                <div class="head-box-area">
                                    <span class="numero text-right">P5</span>
                                    <h2 class="box-title text-right"><?php the_field( 'titulo_p5' ); ?></h2>
                                </div>
                                <div class="content-box-area simple-content">
                                    <?php the_field( 'bajada_p5' ); ?>
                                </div>
                            </div>
                            <div id="p6" class="box-content modal-trigger" data-id="modal-p6">
                                <div class="head-box-area">
                                    <span class="numero text-right">P6</span>
                                    <h2 class="box-title text-right"><?php the_field( 'titulo_p6' ); ?></h2>
                                </div>
                                <div class="content-box-area image-content">
                                    <?php $imagen_p6 = get_field( 'imagen_p6' ); ?>
                                    <img src="<?php echo $imagen_p6['url']; ?>" alt="<?php echo $imagen_p6['alt']; ?>">
                                </div>
                            </div>
                        </div>
                        <div id="p7" class="box-content modal-trigger" data-id="modal-p7">
                            <div class="head-box-area">
                                <span class="numero text-right">P7</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p7' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <div class="col-100 flex justify-between align-center">
                                    <?php if ( have_rows( 'grupo_1_p7' ) ) : ?>
                                    <div class="info-1">
                                        <?php while ( have_rows( 'grupo_1_p7' ) ) : the_row(); ?>
                                        <div class="col-100 flex justify-between align-center">

                                            <div class="data-1">
                                                <p class="font-13"><?php the_sub_field( 'texto_izquierda' ); ?></p>
                                            </div>
                                            <div class="arrow">
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/chevron-right.svg"
                                                    alt="">
                                            </div>
                                            <div class="data-2">
                                                <?php the_sub_field( 'texto_derecha' ); ?>
                                            </div>
                                        </div>
                                        <p class="paragraph font-13">
                                            <?php the_sub_field( 'texto_abajo' ); ?>
                                        </p>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                    <div class="info-2">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/sudamerica-icon.svg"
                                            alt="">
                                    </div>
                                    <?php if ( have_rows( 'grupo_2_p7' ) ) : ?>
                                    <div class="info-3">
                                        <?php while ( have_rows( 'grupo_2_p7' ) ) : the_row(); ?>
                                        <div class="flex justify-between align-center">
                                            <div class="data-1">
                                                <p class="font-13"><?php the_sub_field( 'texto_izquierda' ); ?></p>
                                            </div>
                                            <div class="arrow">
                                                <img src="<?php echo get_template_directory_uri(); ?>/img/chevron-right.svg"
                                                    alt="">
                                            </div>
                                            <div class="data-2">
                                                <p class="font-10"><?php the_sub_field( 'texto_derecha' ); ?></p>
                                            </div>
                                        </div>
                                        <div class="flex justify-between align-start">
                                            <p class="paragraph font-13">
                                                <?php the_sub_field( 'texto_abajo_uno' ); ?>
                                            </p>
                                            <p class="paragraph font-13">
                                                <?php the_sub_field( 'texto_abajo_dos' ); ?>
                                            </p>
                                            <p class="paragraph font-13">
                                                <?php the_sub_field( 'texto_abajo_tres' ); ?>
                                            </p>
                                        </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-box is-amarillo size-1 vertical-box">
                        <div class="corner-title">
                            <span><?php the_field( 'etiqueta_amarilla' ); ?></span>
                        </div>
                        <div id="p8" class="box-content modal-trigger" data-id="modal-p8">
                            <div class="head-box-area">
                                <span class="numero text-right">P8</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p8' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <div class="flex justify-start align-start">
                                    <?php the_field( 'bajada_p8' ); ?>
                                </div>
                            </div>
                        </div>
                        <div id="p9" class="box-content modal-trigger" data-id="modal-p9">
                            <div class="head-box-area">
                                <span class="numero text-right">P9</span>
                                <h2 class="box-title text-right"><?php the_field( 'titulo_p9' ); ?></h2>
                            </div>
                            <div class="content-box-area simple-content">
                                <div class="flex justify-start align-start">
                                    <?php the_field( 'bajada_p9' ); ?>
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/vision-ambiental.svg"
                                        alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="about-timeline" class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_historia_ccu' ); ?></h3>
            </div>

            <section class="slider-3-container col-100 left clearfix relative">
                <?php if ( have_rows( 'historia_ccu_timeline' ) ) : ?>
                <div class="slider-3 col-100 left clearfix relative">
                    <?php while ( have_rows( 'historia_ccu_timeline' ) ) : the_row(); ?>
                    <div class="slide col-100 left clearfix relative overflow-hidden">
                        <div class="content">
                            <div class="image-area col-45">
                                <?php $timeline_fotografia = get_sub_field( 'timeline_fotografia' ); ?>
                                <img src="<?php echo $timeline_fotografia['url']; ?>"
                                    alt="<?php echo $timeline_fotografia['alt']; ?>">
                            </div>
                            <div class="content-area col-45">
                                <p class="year"><?php the_sub_field( 'timeline_ano' ); ?></p>
                                <p class="title"><?php the_sub_field( 'timeline_titulo' ); ?></p>
                                <?php if ( have_rows( 'timeline_hitos' ) ) : ?>
                                <div class="description">
                                    <?php while ( have_rows( 'timeline_hitos' ) ) : the_row(); ?>
                                    <p><?php the_sub_field( 'timeline_hitos_hito' ); ?></p>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div><!-- content -->
                        <div class="veil"></div>
                        <img class="photo cover" src="<?php echo $timeline_fotografia['url']; ?>"
                            alt="<?php echo $timeline_fotografia['alt']; ?>">
                    </div><!-- slide -->
                    <?php endwhile; ?>
                </div><!-- slider-3 -->
                <?php endif; ?>
                <?php if ( have_rows( 'historia_ccu_timeline' ) ) : ?>
                <ul class="slider-nav-3">
                    <?php while ( have_rows( 'historia_ccu_timeline' ) ) : the_row(); ?>
                    <li><span><?php the_sub_field( 'timeline_ano' ); ?></span></li>
                    <?php endwhile; ?>
                </ul>
                <?php endif; ?>
            </section><!-- slider-3-container -->
        </div>
    </div>
</section>


<section id="ccu-global" class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_ccu_region' ); ?></h3>
            </div>
            <div class="tabs-area">
                <div id="map-canvas"></div>
                <div id="paises-ccu" class="tabs">
                    <?php if ( have_rows( 'paises' ) ) : $m = 0; ?>
                    <ul class="tabs-triggers">
                        <?php while ( have_rows( 'paises' ) ) : the_row(); ?>
                        <?php $currPais = get_sub_field( 'pais' ); ?>
                        <li><a href="#<?php echo strtolower($currPais); ?>-tab" class="map-trigger"
                                data-id-map="<?php echo $m; ?>"><?php echo $currPais; ?></a></li>
                        <?php $m++; endwhile; ?>
                    </ul>
                    <?php endif; ?>
                    <?php if ( have_rows( 'paises' ) ) : ?>
                    <?php while ( have_rows( 'paises' ) ) : the_row(); ?>
                    <?php $currPais = get_sub_field( 'pais' ); ?>
                    <div id="<?php echo strtolower($currPais); ?>-tab" class="tab-content">
                        <div class="content">
                            <div class="country-content">
                                <div class="country-slide">
                                    <a href="#" class="arrow prev">
                                        <i class="icon-chevron-left"></i>
                                    </a>
                                    <?php if ( have_rows( 'imagenes_slider' ) ) : ?>
                                    <div class="fotos-slider">
                                        <?php while ( have_rows( 'imagenes_slider' ) ) : the_row(); ?>
                                        <div class="slide">
                                            <?php $foto_pais = get_sub_field( 'foto_pais' ); ?>
                                            <div class="imagen cover"
                                                style="background-image: url(<?php echo $foto_pais['url']; ?>);"
                                                title="<?php echo $foto_pais['alt']; ?>">
                                            </div>
                                        </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <?php endif; ?>
                                    <a href="#" class="arrow next">
                                        <i class="icon-chevron-right"></i>
                                    </a>
                                </div>
                                <div class="country-info">
                                    <?php $logo_pais = get_sub_field( 'logo_pais' ); ?>
                                    <?php if ( $logo_pais ) { ?>
                                    <img src="<?php echo $logo_pais['url']; ?>" alt="<?php echo $logo_pais['alt']; ?>"
                                        class="logo-country" />
                                    <?php } ?>
                                    <p><?php the_sub_field( 'bajada_pais' ); ?></p>
                                    <?php $link_externo_pais = get_sub_field( 'link_externo_pais' ); ?>
                                    <?php if ( $link_externo_pais ) { ?>
                                    <div class="country-link">
                                        <a href="<?php echo $link_externo_pais['url']; ?>"
                                            target="<?php echo $link_externo_pais['target']; ?>"
                                            class="btn is-verde is-rounded"><?php echo $link_externo_pais['title']; ?></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="veil"></div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( have_rows( 'modal_p1' ) ) : ?>
<?php while ( have_rows( 'modal_p1' ) ) : the_row(); ?>
<div data-id="modal-p1" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-verde-claro">
                <span class="p-number">P1</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p2' ) ) : ?>
<?php while ( have_rows( 'modal_p2' ) ) : the_row(); ?>
<div data-id="modal-p2" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-verde-claro">
                <span class="p-number">P2</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p3' ) ) : ?>
<?php while ( have_rows( 'modal_p3' ) ) : the_row(); ?>
<div data-id="modal-p3" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-burdeo">
                <span class="p-number">P3</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p4' ) ) : ?>
<?php while ( have_rows( 'modal_p4' ) ) : the_row(); ?>
<div data-id="modal-p4" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-burdeo">
                <span class="p-number">P4</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p5' ) ) : ?>
<?php while ( have_rows( 'modal_p5' ) ) : the_row(); ?>
<div data-id="modal-p5" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-celeste">
                <span class="p-number">P5</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p6' ) ) : ?>
<?php while ( have_rows( 'modal_p6' ) ) : the_row(); ?>
<div data-id="modal-p6" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-celeste">
                <span class="p-number">P6</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <div class="slide-area">
                    <?php if ( have_rows( 'slide_modal' ) ) : ?>
                    <div id="slider-p6">
                        <?php while ( have_rows( 'slide_modal' ) ) : the_row(); ?>
                        <?php $imagen_slide_modal = get_sub_field( 'imagen_slide_modal' ); ?>
                        <div class="slide">
                            <div class="imagen-p"
                                style="background-image: url(<?php echo $imagen_slide_modal['url']; ?>)"
                                title="<?php echo $imagen_slide_modal['alt']; ?>"></div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p7' ) ) : ?>
<?php while ( have_rows( 'modal_p7' ) ) : the_row(); ?>
<div data-id="modal-p7" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-celeste">
                <span class="p-number">P7</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p8' ) ) : ?>
<?php while ( have_rows( 'modal_p8' ) ) : the_row(); ?>
<div data-id="modal-p8" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-amarillo">
                <span class="p-number">P8</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php if ( have_rows( 'modal_p9' ) ) : ?>
<?php while ( have_rows( 'modal_p9' ) ) : the_row(); ?>
<div data-id="modal-p9" class="modal ser-modal">
    <i class="close icon-equis"></i>
    <div class="content-modal p-modal">
        <div class="p-modal-area">
            <div class="intro-area bg-amarillo">
                <span class="p-number">P9</span>
                <div class="p-titulo">
                    <?php the_sub_field( 'titulo_modal' ); ?>
                </div>
                <div class="p-bajada">
                    <p><?php the_sub_field( 'bajada_modal' ); ?></p>
                </div>
            </div>
            <div class="content-area">
                <?php the_sub_field( 'contenido_modal' ); ?>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCK9PkyUNwQNhK4VMJI5n3RNCqtH9cJgGc"></script>
<script>
var gmarkers1 = [];
var markers1 = [];

// Our markers
markers1 = [
    <?php if ( have_rows( 'paises' ) ) { $count = 0;
            while ( have_rows( 'paises' ) ) { the_row(); 
                $currPais = get_sub_field( 'pais' );
                $location = get_sub_field('ubicacion_pais'); ?>['<?php echo $count; ?>', '<?php echo $currPais; ?>',
        <?php echo $location['lat']; ?>, <?php echo $location['lng']; ?>, <?php echo $count; ?>],

    <?php $count++; }
        } ?>
];

/**
 * Function to init map
 */


function initialize() {
    var mapOptions = {
        styles: [{
            "featureType": "administrative",
            "elementType": "all",
            "stylers": [{
                "saturation": "-100"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "all",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#e4e4e4"
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#ffffff"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#021019"
            }]
        }, {
            "featureType": "administrative.country",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "administrative.province",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#f0f0f0"
            }]
        }, {
            "featureType": "administrative.province",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#ffffff"
            }]
        }, {
            "featureType": "administrative.province",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#d52a2a"
            }]
        }, {
            "featureType": "administrative.province",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "all",
            "stylers": [{
                "visibility": "on"
            }, {
                "lightness": "0"
            }, {
                "gamma": "1.00"
            }, {
                "weight": "1.00"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "geometry",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "geometry.stroke",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#eb1e1e"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "labels",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.locality",
            "elementType": "labels.text",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "administrative.neighborhood",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#b72424"
            }]
        }, {
            "featureType": "administrative.neighborhood",
            "elementType": "geometry.fill",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#de3636"
            }]
        }, {
            "featureType": "administrative.land_parcel",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }, {
                "color": "#e02727"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "lightness": 85
            }, {
                "visibility": "on"
            }, {
                "color": "#e8e8e8"
            }]
        }, {
            "featureType": "landscape.man_made",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "landscape.natural.landcover",
            "elementType": "all",
            "stylers": [{
                "visibility": "on"
            }]
        }, {
            "featureType": "landscape.natural.landcover",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#963030"
            }]
        }, {
            "featureType": "landscape.natural.landcover",
            "elementType": "labels.text.stroke",
            "stylers": [{
                "color": "#9d0000"
            }]
        }, {
            "featureType": "landscape.natural.terrain",
            "elementType": "all",
            "stylers": [{
                "color": "#a4c2da"
            }, {
                "visibility": "off"
            }]
        }, {
            "featureType": "landscape.natural.terrain",
            "elementType": "labels.text",
            "stylers": [{
                "visibility": "on"
            }, {
                "color": "#ff0000"
            }]
        }, {
            "featureType": "landscape.natural.terrain",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#ff0000"
            }]
        }, {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "lightness": "50"
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": "-100"
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "all",
            "stylers": [{
                "lightness": "30"
            }]
        }, {
            "featureType": "road.local",
            "elementType": "all",
            "stylers": [{
                "lightness": "40"
            }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "visibility": "simplified"
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{
                "lightness": -25
            }, {
                "saturation": -97
            }, {
                "color": "#ffffff"
            }]
        }, {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [{
                "lightness": -25
            }, {
                "saturation": -100
            }]
        }],
        zoom: 9,
        disableDefaultUI: true,
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    for (i = 0; i < markers1.length; i++) {
        addMarker(markers1[i]);
    }

    $('.map-trigger').on('click', function() {
        google.maps.event.trigger(gmarkers1[$(this).data('id-map')], 'click');
    });

    google.maps.event.trigger(gmarkers1[0], 'click');

    // google.maps.event.trigger(map, 'resize');

    // setZoomByMarkers();
}

/**
 * Function to add marker to map
 */

function addMarker(marker) {
    var title = marker[1];
    var pos = new google.maps.LatLng(marker[2], marker[3]);
    var content = marker[1];
    var id = marker[4];


    var image = {
        url: '<?php echo get_template_directory_uri(); ?>/img/marker.svg',
        size: new google.maps.Size(30, 42),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(15, 42),
        scaledSize: new google.maps.Size(30, 42)
    };

    marker1 = new google.maps.Marker({
        title: title,
        position: pos,
        map: map,
        icon: image,
        id: id,
    });

    gmarkers1.push(marker1);

    google.maps.event.addListener(marker1, 'click', (function(marker1, content) {
        return function() {
            map.panTo(this.getPosition());
        }
    })(marker1, content));

}

$(document).ready(function() {
    $('.slider-3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        speed: 700,
        asNavFor: '.slider-nav-3',
        responsive: [{
            breakpoint: 851,
            settings: {
                arrows: false,
            }
        }]
    });

    $('.slider-nav-3').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-3',
        dots: false,
        centerMode: false,
        arrows: false,
        focusOnSelect: true,
        vertical: false,
        responsive: [{
                breakpoint: 851,
                settings: {
                    vertical: false,
                }
            },
            {
                breakpoint: 641,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    vertical: false,
                }
            }
        ]
    });
    equalOuterHeight($(".slider-3 .slide .content"));

    $('.fotos-slider').each(function(index, element) {
        $(this).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            speed: 700
        });
    });
    $('.country-slide').each(function(index, element) {
        $(this).children('.prev').click(function(e) {
            e.preventDefault();
            $(element).children('.fotos-slider').slick('slickPrev');
        });
        $(this).children('.next').click(function(e) {
            e.preventDefault();
            $(element).children('.fotos-slider').slick('slickNext');
        });
    });

    $('#slider-p6').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        speed: 700,
    });
    $('.modal-trigger[data-id="modal-p6"]').click(function(e) {
        e.preventDefault();
        $('#slider-p6').slick('slickGoTo', 0);
        $('#slider-p6').slick('setPosition', 0);
    });

    $("#paises-ccu").tabs({
        beforeActivate: function(event, ui) {
            $('.fotos-slider').each(function(index, element) {
                $(this).slick('slickGoTo', 0);
                $(this).slick('setPosition', 0);
            });
            // google.maps.event.trigger(map, 'resize');
        }
    });
    initialize();
});
</script>
<?php get_footer(); ?>
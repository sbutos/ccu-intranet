<?php
/*
* Template Name: Home
* 
*/
get_header();
?>
<?php if ( have_rows( 'modulos' ) ): ?>
<section class="home-main-container">
    <div class="wrap-xl">
        <?php while ( have_rows( 'modulos' ) ) : the_row(); ?>
        <?php if ( get_row_layout() == 'al_dia_slider_box' ) : ?>
        <div class="aldia-home box-section">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                    <?php $btn_all = get_sub_field( 'btn_all' ); ?>
                    <?php if ( $btn_all ) { ?>
                    <a href="<?php echo $btn_all['url']; ?>" target="<?php echo $btn_all['target']; ?>"
                        class="btn-ver-todas"><span><?php echo $btn_all['title']; ?></span><i
                            class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>
                <?php $feat_news = get_sub_field( 'feat_news' ); ?>
                <?php if ( $feat_news ): ?>
                <div class="slider-area border-radius-m">
                    <div id="aldia-arrows" class="arrows">
                        <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                        <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
                    </div>
                    <div id="aldia-home-slider">
                        <?php foreach ( $feat_news as $post ):  ?>
                        <?php setup_postdata ( $post );
                        $featThumb = get_the_post_thumbnail_url();
                        $featThumbID = get_post_thumbnail_ID();
                        $featThumbAlt = get_post_meta ( $featThumbID, '_wp_attachment_image_alt', true );
                        ?>
                        <div class="slide">
                            <div class="slide-content">
                                <?php
                                $categories = get_the_category();
                                $comma      = ' ';
                                $output     = '';
                                
                                if ( $categories ) {
                                    foreach ( $categories as $category ) {
                                        $output .= '<span class="category">#' . $category->cat_name . '</span>' . $comma;
                                    }
                                    echo trim( $output, $comma );
                                } ?>
                                <div class="photo cover" style="background-image: url(<?php echo $featThumb; ?>)"
                                    title="<?php echo $featThumbAlt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <div class="content-area">
                                        <h3 class="post-title">
                                            <?php the_title(); ?>
                                        </h3>
                                        <div class="post-excerpt">
                                            <?php the_excerpt(); ?>
                                        </div>
                                    </div>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>"
                                            class="btn is-verde is-rounded"><?php _e('Saber Más', 'ccu-intranet'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'otras_noticias_box' ) : ?>
        <div class="extra-news-home-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                </div>
                <div class="extra-news-area">
                    <?php $news = get_sub_field( 'news' ); ?>
                    <?php if ( $news ): ?>
                    <div class="grid-column-2 gap-m">
                        <?php foreach ( $news as $post ):  ?>
                        <?php setup_postdata ( $post );
                            $extraNewsThumbImg = get_the_post_thumbnail_url();
                            $extraNewsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $extraNewsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>

                        <div class="small-news-area border-radius-m">
                            <div class="photo cover" style="background-image: url(<?php echo $extraNewsThumbImg; ?>);">
                                <div class="veil"></div>
                            </div>
                            <div class="content">
                                <div class="post-cat-area">
                                    <?php $category_detail=get_the_category($post->ID);//$post->ID
                                    foreach($category_detail as $cd){
                                    echo '<span>'.$cd->cat_name.'</span>';
                                    } ?>
                                </div>
                                <div class="content-area">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>"
                                            class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'personalizado_box' ) : ?>
        <div class="custom-box-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                </div>
                <div class="big-content-container">
                    <div class="box-content-area border-radius-m">
                        <?php $bg_imagen = get_sub_field( 'bg_imagen' ); ?>
                        <div class="photo cover" style="background-image: url(<?php echo $bg_imagen['url']; ?>);"
                            title="<?php echo $bg_imagen['alt']; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="content-area">
                                <h3 class="post-title">
                                    <?php the_sub_field( 'titulo' ); ?>
                                </h3>
                                <div class="post-excerpt">
                                    <?php the_sub_field( 'bajada' ); ?>
                                </div>
                            </div>
                            <?php $boton_link = get_sub_field( 'boton_link' ); ?>
                            <?php if ( $boton_link ) { ?>
                            <div class="button-area">
                                <a href="<?php echo $boton_link['url']; ?>"
                                    target="<?php echo $boton_link['target']; ?>"
                                    class="btn is-verde is-rounded"><?php echo $boton_link['title']; ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'birthdays_box' ) : ?>
        <div class="birthday-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                    <?php $boton_link = get_sub_field( 'boton_link' ); ?>
                    <?php if ( $boton_link ) { ?>
                    <a id="cumpleanos-trigger" data-id="modal-cumpleanos" href="<?php echo $boton_link['url']; ?>"
                        target="<?php echo $boton_link['target']; ?>"
                        class="btn-ver-todas modal-trigger"><span><?php echo $boton_link['title']; ?></span><i
                            class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>
                <div class="birthday-area">
                    <div id="birthday-slider">
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="birthday-person">
                                <div class="avatar-box">
                                    <div class="avatar">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                            class="profile">
                                    </div>
                                </div>
                                <div class="info-box">
                                    <span class="day">Hoy, 19 de Septiembre</span>
                                    <span class="name">Javier González</span>
                                    <span class="range">Gerente Comercial</span>
                                    <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                        data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="birthday-dots"></div>
                </div>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'rrss_box' ) : ?>
        <div class="twitter-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                </div>
                <a class="twitter-timeline" data-height="300" data-chrome="noheader nofooter noborders transparent"
                    href="https://twitter.com/ccu_cl?ref_src=twsrc%5Etfw"></a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'concursos_box' ) : ?>
        <div class="concursos-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                </div>
                <div class="concursos-area">
                    <div class="box-content">
                        <h4 class="titulo"><?php the_sub_field( 'titulo' ); ?></h4>
                        <div class="bajada">
                            <?php the_sub_field( 'bajada' ); ?>
                        </div>
                        <div class="ref-imagen">
                            <?php $ref_imagen = get_sub_field( 'ref_imagen' ); ?>
                            <?php if ( $ref_imagen ) { ?>
                            <img src="<?php echo $ref_imagen['url']; ?>" alt="<?php echo $ref_imagen['alt']; ?>" />
                            <?php } ?>
                        </div>
                        <div class="boton-mas">
                            <?php $boton_link = get_sub_field( 'boton_link' ); ?>
                            <?php if ( $boton_link ) { ?>
                            <a href="<?php echo $boton_link['url']; ?>" target="<?php echo $boton_link['target']; ?>"
                                class="btn is-verde is-rounded size-s"><?php echo $boton_link['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php elseif ( get_row_layout() == 'organigrama_box' ) : ?>
        <div class="organigrama-container box-section box-<?php the_sub_field( 'box_size' ); ?>">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'cabecera' ); ?></h3>
                </div>
                <div class="organigrama-area">
                    <div class="box-content">
                        <?php $icon_imagen = get_sub_field( 'icon_imagen' ); ?>
                        <?php if ( $icon_imagen ) { ?>
                        <div class="icono-imagen">
                            <img src="<?php echo $icon_imagen['url']; ?>" alt="<?php echo $icon_imagen['alt']; ?>" />
                        </div>
                        <?php } ?>
                        <h4 class="titulo"><?php the_sub_field( 'titulo' ); ?></h4>
                        <div class="bajada">
                            <?php the_sub_field( 'bajada' ); ?>
                        </div>
                        <div class="boton-mas">
                            <?php $boton_link = get_sub_field( 'boton_link' ); ?>
                            <?php if ( $boton_link ) { ?>
                            <a href="<?php echo $boton_link['url']; ?>" target="<?php echo $boton_link['target']; ?>"
                                class="btn is-verde is-rounded size-s modal-trigger"
                                data-id="modal-cambios"><?php echo $boton_link['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php endwhile; ?>
    </div>
</section>
<?php else: ?>
<?php // no layouts found ?>
<?php endif; ?>
<div data-id="modal-cambios" class="modal modal-cambios-area">
    <i class="close icon-equis"></i>
    <div class="content-modal modal-cambios">
        <div class="modal-heading">
            <div class="title-area">
                <h4>Cambios Organizacionales</h4>
            </div>
            <div class="date-selector-area">
                <div class="date-selector-box">
                    <div class="select-box">
                        <select name="post-date-selector" id="post-date-selector">
                            <?php 
                            $posts = get_posts( array(
                                'post_type' => 'cambios',
                                'meta_key'  => 'fecha_cambios',
                                'orderby'   => 'meta_value_num',
                                'order'     => 'DESC',
                            ));
                            
                            if( $posts ) {
                                foreach( $posts as $post ) {
                                    echo '<option value="'.get_the_id($post).'">'.get_the_title($post).'</option>';
                                }
                            }
                            ?>
                        </select>
                        <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/organizacion-icon.svg" alt="">
                </div>
            </div>
        </div>
        <div id="post-cambios" class="cambios-container">
            <?php
            $the_query  = new WP_Query(array('post_type' => 'cambios', 'posts_per_page' => 1, 'orderby' => 'meta_value_num', 'meta_key' => 'fecha_cambios', 'order' => 'DESC'));

            if ($the_query->have_posts()) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    echo get_template_part('components/template-parts/cambio');
        
                }
            } 
            else {
                echo '<div id="postdata">'.__('Didnt find anything', THEME_NAME).'</div>';
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<div data-id="modal-cumpleanos" class="modal modal-cumpleanos-area">
    <i class="close icon-equis"></i>
    <div class="content-modal modal-cumpleanos">
        <div class="modal-heading">
            <div class="title-area">
                <h4>Cumpleaños de Hoy</h4>
            </div>
        </div>
        <div class="modal-content">
            <div id="cumpleanos-tabs">
                <ul class="tabs-triggers">
                    <li><a href="#cumple-hoy">Cumpleaños de Hoy</a></li>
                    <li><a href="#cumple-proximo">Próximos cumpleaños</a></li>
                </ul>
                <div id="cumple-hoy" class="tab-content">
                    <div class="slider-area">
                        <div id="birthday-slider-hoy">
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cumple-proximo" class="tab-content">
                    <div class="slider-area">
                        <div id="birthday-slider-prox">
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                            <div class="slide">
                                <div class="birthday-person">
                                    <div class="avatar-box">
                                        <div class="avatar">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg"
                                                alt="" class="profile">
                                        </div>
                                    </div>
                                    <div class="info-box">
                                        <span class="day">Hoy, 19 de Septiembre</span>
                                        <span class="name">Javier González</span>
                                        <span class="range">Gerente Comercial</span>
                                        <a href="" class="btn is-verde is-rounded has-icon modal-trigger"
                                            data-id="modal-saludo"><i class="icon-hands"></i><span>Saludar</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<div data-id="modal-saludo" class="modal modal-saludo">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-heading">
            <div class="title-area">
                <h4>Saludar</h4>
            </div>
        </div>
        <div class="cumpleanero-area">
            <div class="avatar-box">
                <div class="avatar">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt="" class="profile">
                </div>
            </div>
            <div class="info-box">
                <span class="day">Hoy, 19 de Septiembre</span>
                <span class="name">Javier González</span>
                <span class="range">Gerente Comercial</span>
                <div class="saludo-form">
                    <input type="text" name="saludo" id="saludo-box" placeholder="Salúdalo en su Cumpleaños">
                    <a href="" class="btn is-verde is-rounded has-icon size-s"><i
                            class="icon-hands"></i><span>Saludar</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<script>
$(document).ready(function() {
    $('#aldia-home-slider').slick({
        arrows: false,
        dots: false,
        speed: 750
    });
    $('#aldia-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#aldia-home-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#aldia-home-slider').slick('slickNext');
            });
        }
    });
    $('#birthday-slider').slick({
        rows: 3,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: $('#birthday-dots'),
        speed: 750
    });
    $('#birthday-slider-hoy').slick({
        rows: 3,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 750
    });
    $('#birthday-slider-prox').slick({
        rows: 3,
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        speed: 750
    });
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px',
        responsive: [{
            breakpoint: 480,
            settings: {
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newPanel = $('#slider-gerentes').slick('setPosition');
        }
    });
    $("#cumpleanos-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newPanel = $('#birthday-slider-hoy').slick('setPosition');
        }
    });
    $('#gerentes-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickNext');
            });
        }
    });
    $('#cumpleanos-trigger').click(function(e) {
        e.preventDefault();
        $('#birthday-slider-hoy').slick('slickGoTo', 0);
        $('#birthday-slider-hoy').slick('setPosition', 0);
    });
});
</script>
<script>
$(document).ajaxComplete(function() {
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px',
        responsive: [{
            breakpoint: 480,
            settings: {
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newTab = $('#slider-gerentes').slick('setPosition');
        }
    });
    $('#cumpleanos-trigger').click(function(e) {
        e.preventDefault();
        $('#birthday-slider-hoy').slick('slickGoTo', 0);
        $('#birthday-slider-hoy').slick('setPosition', 0);
        $('#birthday-slider-prox').slick('slickGoTo', 0);
        $('#birthday-slider-prox').slick('setPosition', 0);
    });
    $("#cumpleanos-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newPanel = $('#birthday-slider-hoy').slick('setPosition');
            newPanel = $('#birthday-slider-prox').slick('setPosition');
        }
    });
});
</script>
<?php get_footer(); ?>
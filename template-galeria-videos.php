<?php
/*
*
* Template Name: Galería de Videos
*
*/
?>
<?php get_header(); ?>
<section class="section videos-feat-area">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_feat_video' ); ?></h3>
            </div>
            <?php $feat_videos = get_field( 'feat_videos' ); ?>
            <?php if ( $feat_videos ): $c = 1; ?>
            <div class="videos-feat-layout">
                <?php foreach ( $feat_videos as $post ):  ?>
                <?php setup_postdata ( $post );
                $featVideoThumbImg = get_the_post_thumbnail_url();
                $featVideoThumbnailID = get_post_thumbnail_ID();
                $alt = get_post_meta ( $featVideoThumbnailID, '_wp_attachment_image_alt', true );
                ?>
                <?php if($c==1) { ?>
                <div class="main-feat-area">
                    <a href="<?php the_permalink(); ?>" class="small-news-area video-type border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $featVideoThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="play-area modal-trigger" data-id="modal-video-<?php echo $c; ?>"
                                data-video-url="<?php the_field('id_video_youtube'); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                            </div>
                            <div class="content-area">
                                <span class="fecha"><?php the_date(); ?></span>
                                <div class="post-info">
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } else { ?>
                <div class="small-feat-area">
                    <a href="<?php the_permalink(); ?>" class="small-news-area video-type border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $featVideoThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="content-area">
                                <div class="play-area modal-trigger" data-id="modal-video-<?php echo $c; ?>"
                                    data-video-url="<?php the_field('id_video_youtube'); ?>">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                </div>
                                <div class="post-info">
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php } ?>
                <?php $c++; endforeach; ?>
            </div>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section video-gal-page">
    <div class="wrap-xl">
        <div class="videos-slide-area">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_field( 'titulo_videos' ); ?></h3>
                    <?php $link_todas_videos = get_field( 'link_todas_videos' ); ?>
                    <?php if ( $link_todas_videos ) { ?>
                    <a href="<?php echo $link_todas_videos['url']; ?>"
                        target="<?php echo $link_todas_videos['target']; ?>"
                        class="btn-ver-todas"><span><?php echo $link_todas_videos['title']; ?></span><i
                            class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>

                <div class="slider-area">
                    <?php $videos_gal = get_field( 'videos_gal' ); ?>
                    <?php if ( $videos_gal ): $m = 1; ?>
                    <a href="#" id="video-arrow-prev" class="arrow prev"><i class="icon-chevron-left"></i></a>
                    <a href="#" id="video-arrow-next" class="arrow next"><i class="icon-chevron-right"></i></a>
                    <div id="videos-slider">
                        <?php foreach ( $videos_gal as $post ):  ?>
                        <?php setup_postdata ( $post );
                    $videoThumbImg = get_the_post_thumbnail_url();
                    $videoThumbnailID = get_post_thumbnail_ID();
                    $videoAlt = get_post_meta ( $videoThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                        <div class="slide">
                            <a href="<?php the_permalink(); ?>" class="small-news-area video-type border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $videoThumbImg; ?>);"
                                    title="<?php echo $videoAlt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="content-area">
                                        <div class="play-area modal-trigger" data-id="modal-video-gal-<?php echo $m; ?>"
                                            data-video-url="<?php the_field('id_video_youtube'); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php $m++; endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $feat_videos = get_field( 'feat_videos' ); ?>
<?php if ( $feat_videos ): $v = 1; ?>
<?php foreach ( $feat_videos as $post ):  ?>
<?php setup_postdata ( $post ); ?>
<div data-id="modal-video-<?php echo $v; ?>" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $v++; endforeach; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
<?php $videos_gal_modal = get_field( 'videos_gal' ); ?>
<?php if ( $videos_gal_modal ): $c = 1; ?>
<?php foreach ( $videos_gal_modal as $post ):  ?>
<?php setup_postdata ( $post ); ?>
<div data-id="modal-video-gal-<?php echo $c; ?>" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $c++; endforeach; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
<script>
$(document).ready(function() {
    $('#videos-slider').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1
    });
    $('#video-arrow-prev').click(function(e) {
        e.preventDefault();
        $('#videos-slider').slick('slickPrev');
    });
    $('#video-arrow-next').click(function(e) {
        e.preventDefault();
        $('#videos-slider').slick('slickNext');
    });
});
</script>
<?php get_footer(); ?>
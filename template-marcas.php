<?php
/*
* Template Name: Nuestras Marcas
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="heading-image bodegon">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="filters-area">
            <h1 class="filter-title">Nuestro Portafolio de Marcas</h1>
            <div id="filters">
                <div class="filter-box">
                    <div class="select-box">
                        <?php
                        $filter_terms = get_terms( array(
                            'taxonomy'      => 'pais',
                            'parent'        => '0',
                            'hide_empty'    => false,
                        ) );
                        ?>
                        <?php if ($filter_terms) { ?>
                        <select name="pais" id="pais" class="select-input">
                            <option value="*">País</option>
                            <?php foreach ($filter_terms as $filter_term) { 
                                $taxSlug = $filter_term->slug;
                                $taxName = $filter_term->name;
                            ?>
                            <option value="<?php echo $taxSlug; ?>"><?php echo $taxName; ?></option>
                            <?php } ?>
                        </select>
                        <span class="icon"><i class="icon-chevron-down"></i></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="filter-box">
                    <div class="select-box">
                        <?php
                        $filter_terms = get_terms( array(
                            'taxonomy'      => 'tipo',
                            'parent'        => '0',
                            'hide_empty'    => false,
                        ) );
                        ?>
                        <?php if ($filter_terms) { ?>
                        <select name="tipo" id="tipo" class="select-input">
                            <option value="*">Tipo</option>
                            <?php foreach ($filter_terms as $filter_term) { 
                                $taxSlug = $filter_term->slug;
                                $taxName = $filter_term->name;
                            ?>
                            <option value="<?php echo $taxSlug; ?>"><?php echo $taxName; ?></option>
                            <?php } ?>
                        </select>
                        <span class="icon"><i class="icon-chevron-down"></i></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="filter-box">
                    <div class="select-box">
                        <select name="subtipo" id="subtipo" class="select-input" disabled>
                            <option value="*">Categoría</option>
                        </select>
                        <span class="icon"><i class="icon-chevron-down"></i></span>
                    </div>
                </div>
                <div class="filter-box">
                    <a href="#" id="filter-btn" class="btn is-verde-oscuro is-rounded">Filtrar</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div id="all-marcas">
            <div class="marcas-container">
                <!-- <div class="heading-area">
                    <h3>Nacionales</h3>
                </div> -->
                <div class="marcas-area">

                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="all-marcas-news">
            <div class="marcas-news-area">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_field( 'titulo_slide_left' ); ?></h3>
                        <?php $link_cat_exp_term = get_field( 'link_cat_exp' ); ?>
                        <?php if ( $link_cat_exp_term) { ?>
                        <a href="<?php echo site_url('/'); ?>repositorio/"
                            data-this-tax="<?php echo $link_cat_exp_term->slug; ?>" class="btn-ver-todas"><span>Ver
                                Todas</span><i class="icon-chevron-right"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php $experiencias_noticias = get_field( 'experiencias_noticias' ); ?>
                <?php if ( $experiencias_noticias ): ?>
                <div class="mini-news-slide">
                    <?php foreach ( $experiencias_noticias as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="slide">
                        <div class="news-box">
                            <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                title="<?php echo $alt; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content">
                                <div class="news-cat-area">
                                    <?php
                                    $categories = get_the_category();
                                    $comma      = ' ';
                                    $output     = '';
                                    
                                    if ( $categories ) {
                                        foreach ( $categories as $category ) {
                                            $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                        }
                                        echo trim( $output, $comma );
                                    } ?>
                                </div>
                                <div class="content-area">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>" class="btn is-verde is-rounded">Ver Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="marcas-news-area">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_field( 'titulo_slide_right' ); ?></h3>
                        <?php $link_cat_con_term = get_field( 'link_cat_exp' ); ?>
                        <?php if ( $link_cat_con_term) { ?>
                        <a href="<?php echo site_url('/'); ?>repositorio/"
                            data-this-tax="<?php echo $link_cat_con_term->slug; ?>" class="btn-ver-todas"><span>Ver
                                Todas</span><i class="icon-chevron-right"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php $concursos_noticias = get_field( 'concursos_noticias' ); ?>
                <?php if ( $concursos_noticias ): ?>
                <div class="mini-news-slide">
                    <?php foreach ( $concursos_noticias as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="slide">
                        <div class="news-box">
                            <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                title="<?php echo $alt; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content">
                                <div class="news-cat-area">
                                    <?php
                                    $categories = get_the_category();
                                    $comma      = ' ';
                                    $output     = '';
                                    
                                    if ( $categories ) {
                                        foreach ( $categories as $category ) {
                                            $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                        }
                                        echo trim( $output, $comma );
                                    } ?>
                                </div>
                                <div class="content-area">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                    <div class="button-area">
                                        <a href="<?php the_permalink(); ?>" class="btn is-verde is-rounded">Ver Más</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<script>
var options = {
    <?php
    $tipo_terms = get_terms( array(
        'taxonomy'      => 'tipo',
        'parent'        => '0',
        'hide_empty'    => false,
    ) );
    ?>
    <?php foreach ($tipo_terms as $tipo_term) { 
        $taxSlug = $tipo_term->slug;
        $taxType = $tipo_term->taxonomy;
        $taxId = $tipo_term->term_id;

        $child_terms = get_terms( array(
            'taxonomy' => $taxType, // you could also use $taxonomy as defined in the first lines
            'child_of' => $taxId,
            'parent' => $taxId, // disable this line to see more child elements (child-child-child-terms)
            'hide_empty' => false,
        ) );
        ?>
    <?php echo $taxSlug ?>: [<?php foreach($child_terms as $child_term) {
            $childTaxSlug = $child_term->slug;
            $childTaxName = $child_term->name;
            echo '"'.$childTaxSlug.'",';
        } ?>],
    <?php } ?>
}
<?php $term = get_term_by('slug', 'element', 'tipo'); $name = $term->name; ?>
var fillSecondary = function() {
    var selected = $('#tipo').val();

    if (selected == '*') {
        $('#subtipo').empty();
        $('#subtipo').prop('disabled', 'disabled');
        $('#subtipo').parent().addClass('disabled');
        $('#subtipo').append('<option value="*">Categoría</option>');
    } else {
        $('#subtipo').empty();
        $('#subtipo').removeAttr("disabled");
        $('#subtipo').parent().removeClass('disabled');
        $('#subtipo').append('<option value="*">Categoría</option>');
        options[selected].forEach(function(element, index) {
            $('#subtipo').append('<option value="' + element + '">' + element + '</option>');
        });
    }
}

function searchMarcas(pais, tipo, subtipo) {
    $("#all-marcas .marcas-container .marcas-area").fadeOut(250);
    setTimeout(function() {
        $('.spinner').fadeIn(250);
    }, 251);

    $.ajax({
        method: 'POST',
        url: '<?php echo get_template_directory_uri(); ?>/searchMarcas.php',
        data: {
            'pais': pais,
            'tipo': tipo,
            'subtipo': subtipo,
        },
        success: function(data) {
            posts = $.parseJSON(data);
            $("#all-marcas .marcas-container .marcas-area").html(posts[1]);
            $('.spinner').fadeOut(250);
            setTimeout(function() {
                $("#all-marcas .marcas-container .marcas-area").fadeIn(250);
            }, 251);

        },
        error: function(data) {
            console.log(data + 'error');
        }
    });
}
$(document).ready(function() {
    $('.mini-news-slide').slick({
        slideToShow: 1,
        arrows: false,
        dots: true
    });
    $('#tipo').change(fillSecondary);
    fillSecondary();
    $('#filter-btn').click(function(e) {
        e.preventDefault();

        let tipoVal = $('#tipo').val();
        let paisVal = $('#pais').val();
        let subtipoVal = $('#subtipo').val();

        ;
        searchMarcas(paisVal, tipoVal, subtipoVal);
        console.log(tipoVal + paisVal + subtipoVal);
    });
    $('.btn-ver-todas').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let permalink = $(this).attr('href');
            let taxonomia = $(this).data('this-tax');
            $("<form method='POST' action='" + permalink +
                    "'><input type='hidden' name='thistax' value='" + taxonomia + "'/></form>")
                .appendTo("body").submit();
        });
    });
});
</script>
<?php get_footer(); ?>
<?php
/*
* Template Name: Subpágina Sustentabilidad
*/
get_header();
?>
<?php $color_base = get_field( 'color_base' ); ?>
<section class="section shortcuts-area">
    <div class="wrap-xl">
        <div class="shortcuts-boxes">
            <?php $boton_eje_personas = get_field( 'boton_eje_personas', 14 ); ?>
            <?php if ( $boton_eje_personas ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_personas['url']; ?>"
                    target="<?php echo $boton_eje_personas['target']; ?>"
                    class="btn size-s is-rounded is-burdeo <?php echo ($color_base == 'burdeo' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_personas['title']; ?></a>
            </div>
            <?php } ?>
            <?php $boton_eje_marcas = get_field( 'boton_eje_marcas', 14 ); ?>
            <?php if ( $boton_eje_marcas ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_marcas['url']; ?>" target="<?php echo $boton_eje_marcas['target']; ?>"
                    class="btn size-s is-rounded is-celeste <?php echo ($color_base == 'celeste' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_marcas['title']; ?></a>
            </div>
            <?php } ?>
            <?php $boton_eje_planeta = get_field( 'boton_eje_planeta', 14 ); ?>
            <?php if ( $boton_eje_planeta ) { ?>
            <div class="shortcut-box">
                <a href="<?php echo $boton_eje_planeta['url']; ?>" target="<?php echo $boton_eje_planeta['target']; ?>"
                    class="btn size-s is-rounded is-amarillo <?php echo ($color_base == 'amarillo' ? '' : 'is-bordered'); ?>"><?php echo $boton_eje_planeta['title']; ?></a>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<section class="section page-content-area">
    <div class="wrap-xl">
        <div class="page-content">
            <div class="wysiwyg force-<?php the_field( 'color_base' ); ?>">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <?php the_field( 'contenido' ); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
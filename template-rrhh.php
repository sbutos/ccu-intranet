<?php
/*
* Template Name: Recursos Humanos
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading rrhh-heading">
            <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <div class="intro-page">
                    <blockquote>
                        <p><?php the_field( 'introduccion_c' ); ?></p>
                    </blockquote>
                </div>
                <div class="rrhh-logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/rrhh-logo.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( have_rows( 'rrhh_areas' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="icono-color-area">
                <?php while ( have_rows( 'rrhh_areas' ) ) : the_row(); ?>
                <a href="<?php the_sub_field( 'link_rrhh_a' ); ?>" class="icono-color-box">
                    <?php $fondo_rrhh_a = get_sub_field( 'fondo_rrhh_a' ); ?>
                    <div class="photo-bg cover" style="background-image: url(<?php echo $fondo_rrhh_a['url']; ?>);"
                        title="<?php echo $fondo_rrhh_a['alt']; ?>">
                        <div class="veil color-<?php the_sub_field( 'color_rrhh_a' ); ?>"></div>
                    </div>
                    <div class="content-box">
                        <?php $icono_rrhh_a = get_sub_field( 'icono_rrhh_a' ); ?>
                        <?php if ( $icono_rrhh_a ) { ?>
                        <img src="<?php echo $icono_rrhh_a['url']; ?>" alt="<?php echo $icono_rrhh_a['alt']; ?>"
                            class="icono-rel" />
                        <?php } ?>
                        <h4 class="titulo"><?php the_sub_field( 'titulo_rrhh_a' ); ?></h4>
                        <div class="bajada">
                            <p><?php the_sub_field( 'descripcion_rrhh_a' ); ?></p>
                        </div>
                    </div>
                </a>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'concursos_internos' ) ) : ?>
            <div class="concursos-container box-section box-2-wide">
                <div class="content">
                    <?php while ( have_rows( 'concursos_internos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_ci' ); ?></h3>
                    </div>
                    <div class="concursos-area">
                        <div class="box-content">
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_ci' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_ci' ); ?></p>
                            </div>
                            <?php $ofertas_ci = get_sub_field( 'ofertas_ci' ); ?>
                            <?php if ( $ofertas_ci ) { ?>
                            <div class="ref-imagen">
                                <img src="<?php echo $ofertas_ci['url']; ?>" alt="<?php echo $ofertas_ci['alt']; ?>" />
                            </div>
                            <?php } ?>
                            <?php $link_externo_ci = get_sub_field( 'link_externo_ci' ); ?>
                            <?php if ( $link_externo_ci ) { ?>
                            <div class="boton-mas">
                                <a href="<?php echo $link_externo_ci['url']; ?>"
                                    target="<?php echo $link_externo_ci['target']; ?>"
                                    class="btn is-verde is-rounded size-s"><?php echo $link_externo_ci['title']; ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'cambios_organizacionales' ) ) : ?>
            <div class="organigrama-container box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'cambios_organizacionales' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_co' ); ?></h3>
                    </div>
                    <div class="organigrama-area">
                        <div class="box-content">
                            <div class="icono-imagen">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/organizacion-icon.svg"
                                    alt="" />
                            </div>
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_co' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_co' ); ?></p>
                            </div>
                            <div class="boton-mas">
                                <a href="#<?php the_sub_field( 'link_popup_co' ); ?>" target="_blank"
                                    class="btn is-verde is-rounded size-s modal-trigger" data-id="modal-cambios">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'mi_unidad' ) ) : ?>
            <div class="organigrama-container box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'mi_unidad' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_mu' ); ?></h3>
                    </div>
                    <div class="organigrama-area">
                        <div class="box-content">
                            <div class="icono-imagen">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/organizacion-icon.svg"
                                    alt="" />
                            </div>
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_mu' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_mu' ); ?></p>
                            </div>
                            <div class="boton-mas">
                                <a href="#<?php the_sub_field( 'link_popup_mu' ); ?>" target="_blank"
                                    class="btn is-verde is-rounded size-s modal-trigger" data-id="modal-cambios">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <div class="contacto-person-small box-section box-2-wide">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title">Contacto</h3>
                    </div>
                    <div class="contact-link-box layout-dos">
                        <div class="icono-area">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.svg" alt="">
                        </div>
                        <div class="content-area">
                            <h4><?php the_field( 'encabezado_crh' ); ?></h4>
                            <p><?php the_field( 'bajada_crh' ); ?></p>
                            <?php $link_crh = get_field( 'link_crh' ); ?>
                            <?php if ( $link_crh ) { ?>
                            <a href="<?php echo $link_crh['url']; ?>" target="<?php echo $link_crh['target']; ?>"
                                target="_blank"
                                class="btn is-verde is-rounded size-s"><?php echo $link_crh['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ( have_rows( 'vivamos_bien' ) ) : ?>
            <div class="organigrama-container box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'vivamos_bien' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_vb' ); ?></h3>
                    </div>
                    <div class="organigrama-area">
                        <div class="box-content">
                            <div class="heading-image">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/LogoVivamosBien.svg" alt="">
                            </div>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_vb' ); ?></p>
                            </div>
                            <?php $link_externo_vb = get_sub_field( 'link_externo_vb' ); ?>
                            <?php if ( $link_externo_vb ) { ?>
                            <div class="boton-mas">
                                <a href="<?php echo $link_externo_vb['url']; ?>"
                                    target="<?php echo $link_externo_vb['target']; ?>"
                                    class="btn is-verde is-rounded size-s"><?php echo $link_externo_vb['title']; ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>

<div data-id="modal-cambios" class="modal modal-cambios-area">
    <i class="close icon-equis"></i>
    <div class="content-modal modal-cambios">
        <div class="modal-heading">
            <div class="title-area">
                <h4>Cambios Organizacionales</h4>
            </div>
            <div class="date-selector-area">
                <div class="date-selector-box">
                    <div class="select-box">
                        <select name="post-date-selector" id="post-date-selector">
                            <?php 
                            $posts = get_posts( array(
                                'post_type' => 'cambios',
                                'meta_key'  => 'fecha_cambios',
                                'orderby'   => 'meta_value_num',
                                'order'     => 'DESC',
                            ));
                            
                            if( $posts ) {
                                foreach( $posts as $post ) {
                                    echo '<option value="'.get_the_id($post).'">'.get_the_title($post).'</option>';
                                }
                            }
                            ?>
                        </select>
                        <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div class="icon-box">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/organizacion-icon.svg" alt="">
                </div>
            </div>
        </div>
        <div id="post-cambios" class="cambios-container">
            <?php
            $the_query  = new WP_Query(array('post_type' => 'cambios', 'posts_per_page' => 1, 'orderby' => 'meta_value_num', 'meta_key' => 'fecha_cambios', 'order' => 'DESC'));

            if ($the_query->have_posts()) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    echo get_template_part('components/template-parts/cambio');
        
                }
            } 
            else {
                echo '<div id="postdata">'.__('Didnt find anything', THEME_NAME).'</div>';
            }
            wp_reset_postdata();
            ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<script>
$(document).ready(function() {


    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newPanel = $('#slider-gerentes').slick('setPosition');
        }
    });
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px'
    });
    $('#gerentes-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#slider-gerentes').slick('slickNext');
            });
        }
    });
});
</script>
<script>
$(document).ajaxComplete(function() {
    $("#cambios-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newTab = $('#slider-gerentes').slick('setPosition');
        }
    });
    $('#slider-gerentes').slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '20px'
    });
});
</script>
<?php get_footer(); ?>
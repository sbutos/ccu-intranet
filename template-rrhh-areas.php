<?php
/*
* Template Name: Recursos Humanos - Areas
*/
get_header();
?>
<?php $tipo_pagina = get_field( 'tipo_pagina' ); ?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading rrhh-areas-heading bg-<?php the_field( 'wave_color' ); ?>">
            <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <?php $icono_rrhh_a = get_field( 'icono_rrhh_a' ); ?>
                <?php if ( $icono_rrhh_a ) { ?>
                <div class="icono-box">
                    <img src="<?php echo $icono_rrhh_a['url']; ?>" alt="<?php echo $icono_rrhh_a['alt']; ?>" />
                </div>
                <?php } ?>
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <p><?php the_field( 'bajada_rrhh_a' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if($tipo_pagina == 'burdeo') { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'rrhh_a_concursos_internos' ) ) : ?>
            <div class="concursos-container box-section box-2-wide">
                <div class="content">
                    <?php while ( have_rows( 'rrhh_a_concursos_internos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_ci' ); ?></h3>
                    </div>
                    <div class="concursos-area">
                        <div class="box-content">
                            <h4 class="titulo"><?php the_sub_field( 'titulo_interno_ci' ); ?></h4>
                            <div class="bajada">
                                <p><?php the_sub_field( 'bajada_ci' ); ?></p>
                            </div>
                            <?php $ofertas_ci = get_sub_field( 'ofertas_ci' ); ?>
                            <?php if ( $ofertas_ci ) { ?>
                            <div class="ref-imagen">
                                <img src="<?php echo $ofertas_ci['url']; ?>" alt="<?php echo $ofertas_ci['alt']; ?>" />
                            </div>
                            <?php } ?>
                            <?php $link_externo_ci = get_sub_field( 'link_externo_ci' ); ?>
                            <?php if ( $link_externo_ci ) { ?>
                            <div class="boton-mas">
                                <a href="<?php echo $link_externo_ci['url']; ?>"
                                    target="<?php echo $link_externo_ci['target']; ?>"
                                    class="btn is-verde is-rounded size-s"><?php echo $link_externo_ci['title']; ?></a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'portal_referidos' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'portal_referidos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_pr' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_pr = get_sub_field( 'fondo_pr' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_pr['url']; ?>);"
                                title="<?php echo $fondo_pr['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_pr' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_pr' ); ?></p>
                                </div>
                                <?php $link_externo_pr = get_sub_field( 'link_externo_pr' ); ?>
                                <?php if ( $link_externo_pr ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_pr['url']; ?>"
                                        target="<?php echo $link_externo_pr['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_pr['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'trabaja_nosotros' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'trabaja_nosotros' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_tn' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_tn = get_sub_field( 'fondo_tn' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_tn['url']; ?>);"
                                title="<?php echo $fondo_tn['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_tn' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_tn' ); ?></p>
                                </div>
                                <?php $link_externo_tn = get_sub_field( 'link_externo_tn' ); ?>
                                <?php if ( $link_externo_tn ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_tn['url']; ?>"
                                        target="<?php echo $link_externo_tn['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_tn['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php if ( have_rows( 'contacto_recursos_humanos' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <?php while ( have_rows( 'contacto_recursos_humanos' ) ) : the_row(); ?>
            <div class="heading-box-area">
                <h3 class="head-title">Contacto</h3>
            </div>
            <div class="contact-link-box layout-dos">
                <div class="icono-area">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.svg" alt="">
                </div>
                <div class="content-area">
                    <h4><?php the_sub_field( 'titulo_contacto_rs' ); ?></h4>
                    <p><?php the_sub_field( 'bajada_contacto_rs' ); ?></p>
                    <?php $link_contacto_rs = get_sub_field( 'link_contacto_rs' ); ?>
                    <?php if ( $link_contacto_rs ) { ?>
                    <a href="<?php echo $link_contacto_rs['url']; ?>"
                        target="<?php echo $link_contacto_rs['target']; ?>"
                        class="btn is-verde is-rounded size-s"><?php echo $link_contacto_rs['title']; ?></a>
                    <?php } ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif; ?>
<?php } ?>
<?php if($tipo_pagina == 'verde-oscuro') { ?>
<?php if ( have_rows( 'archivos_externos_bi' ) ) : $m = 1; ?>
<section class="section manuales-section">
    <div class="wrap-xl">
        <div class="content">
            <div class="manuales-area">
                <?php while ( have_rows( 'archivos_externos_bi' ) ) : the_row(); ?>
                <a href="#" class="manual-box modal-trigger" data-id="inciativa-modal-<?php echo $m; ?>">
                    <?php $imagen_rel_ae_bi = get_sub_field( 'imagen_rel_ae_bi' ); ?>
                    <div class="rel-image cover" style="background-image: url(<?php echo $imagen_rel_ae_bi['url']; ?>);"
                        title="<?php echo $imagen_rel_ae_bi['alt']; ?>">
                        <?php $icono_rel_ae_bi = get_sub_field( 'icono_rel_ae_bi' ); ?>
                        <?php if ( $icono_rel_ae_bi ) { ?>
                        <div class="icono-box">
                            <img src="<?php echo $icono_rel_ae_bi['url']; ?>"
                                alt="<?php echo $icono_rel_ae_bi['alt']; ?>" />
                        </div>
                        <?php } ?>
                    </div>
                    <div class="title-box">
                        <h2><?php the_sub_field( 'titulo_ae_bi' ); ?></h2>
                    </div>
                </a>
                <?php $m++; endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php } ?>
<?php if($tipo_pagina == 'celeste') { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'plataforma_aprendizaje' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'plataforma_aprendizaje' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_pa' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_pa = get_sub_field( 'fondo_pa' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_pa['url']; ?>);"
                                title="<?php echo $fondo_pa['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_pa' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_pa' ); ?></p>
                                </div>
                                <?php $link_externo_pa = get_sub_field( 'link_externo_pa' ); ?>
                                <?php if ( $link_externo_pa ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_pa['url']; ?>"
                                        target="<?php echo $link_externo_pa['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_pa['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'politicas_procedimientos' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'politicas_procedimientos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_pp' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_pp = get_sub_field( 'fondo_pp' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_pp['url']; ?>);"
                                title="<?php echo $fondo_pp['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_pp' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_pp' ); ?></p>
                                </div>
                                <?php $link_externo_pp = get_sub_field( 'link_externo_pp' ); ?>
                                <?php if ( $link_externo_pp ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_pp['url']; ?>"
                                        target="<?php echo $link_externo_pp['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_pp['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'shortcut_links' ) ) : ?>
            <div class="direct-links-area box-section box-1-wide">
                <div class="content">
                    <div class="direct-links">
                        <?php while ( have_rows( 'shortcut_links' ) ) : the_row(); ?>
                        <?php $curso_link = get_sub_field( 'curso_link' ); ?>
                        <?php if ( $curso_link ) { ?>
                        <a href="<?php echo $curso_link['url']; ?>" target="<?php echo $curso_link['target']; ?>"
                            class="direct-link-box">
                            <div class="content-box">
                                <div class="icono-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/familia-icon.svg" alt="">
                                </div>
                                <h4><?php echo $curso_link['title']; ?></h4>
                            </div>
                        </a>
                        <?php } ?>
                        <?php $faq_link = get_sub_field( 'faq_link' ); ?>
                        <?php if ( $faq_link ) { ?>
                        <a href="<?php echo $faq_link['url']; ?>" target="<?php echo $faq_link['target']; ?>"
                            class="direct-link-box">
                            <div class="content-box">
                                <div class="icono-box">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/faq-icon.svg" alt="">
                                </div>
                                <h4><?php echo $faq_link['title']; ?></h4>
                            </div>
                        </a>
                        <?php } ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'indicadores_formación' ) ) : ?>
            <div class="indicadores-formacion-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'indicadores_formación' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_f_if' ); ?></h3>
                    </div>
                    <div class="indicadores-box">
                        <?php $imagen_f_if = get_sub_field( 'imagen_f_if' ); ?>
                        <?php if ( $imagen_f_if ) { ?>
                        <img src="<?php echo $imagen_f_if['url']; ?>" alt="<?php echo $imagen_f_if['alt']; ?>" />
                        <?php } ?>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'contacto_recursos_humanos' ) ) : ?>
            <div class="contacto-person-small box-section box-2-wide">
                <div class="content">
                    <?php while ( have_rows( 'contacto_recursos_humanos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title">Contacto</h3>
                    </div>
                    <div class="contact-link-box layout-dos">
                        <div class="icono-area">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.svg" alt="">
                        </div>
                        <div class="content-area">
                            <h4><?php the_sub_field( 'titulo_contacto_rs' ); ?></h4>
                            <p><?php the_sub_field( 'bajada_contacto_rs' ); ?></p>
                            <?php $link_contacto_rs = get_sub_field( 'link_contacto_rs' ); ?>
                            <?php if ( $link_contacto_rs ) { ?>
                            <a href="<?php echo $link_contacto_rs['url']; ?>"
                                target="<?php echo $link_contacto_rs['target']; ?>"
                                class="btn is-verde is-rounded size-s"><?php echo $link_contacto_rs['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section noticias-formacion-area">
    <div class="wrap-xl">
        <?php if ( have_rows( 'noticias_formacion' ) ) : ?>
        <div class="news-area layout-one-four-feat">
            <?php while ( have_rows( 'noticias_formacion' ) ) : the_row(); ?>
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'titulo_f' ); ?></h3>
                    <?php $link_todos_f_term = get_sub_field( 'link_todos_f' ); ?>
                    <?php if ( $link_todos_f_term ) { ?>
                    <a href="<?php echo site_url('/'); ?>repositorio/"
                        data-this-tax="<?php echo $link_todos_f_term->slug; ?>"
                        class="btn-ver-todas color-celeste"><span>Ver
                            Todas</span><i class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>

                <div class="layout-news-area">
                    <?php $noticias_f = get_sub_field( 'noticias_f' ); ?>
                    <?php if ( $noticias_f ): ?>
                    <?php foreach ( $noticias_f as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="small-news-area border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="post-cat-area">
                                <?php $category_detail=get_the_category($post->ID);//$post->ID
                            foreach($category_detail as $cd){
                            echo '<span>'.$cd->cat_name.'</span>';
                            } ?>
                            </div>
                            <div class="content-area">
                                <div class="post-info">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                                <div class="button-area">
                                    <a href="<?php the_permalink(); ?>"
                                        class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php } ?>
<?php if($tipo_pagina == 'amarillo') { ?>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'clima_orgnizacional' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'clima_orgnizacional' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_co' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_co = get_sub_field( 'fondo_co' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_co['url']; ?>);"
                                title="<?php echo $fondo_co['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_co' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_co' ); ?></p>
                                </div>
                                <?php $link_externo_co = get_sub_field( 'link_externo_co' ); ?>
                                <?php if ( $link_externo_co ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_co['url']; ?>"
                                        target="<?php echo $link_externo_co['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_co['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'gestion_desempeno' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'gestion_desempeno' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_gd' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_gd = get_sub_field( 'fondo_gd' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_gd['url']; ?>);"
                                title="<?php echo $fondo_gd['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_gd' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_gd' ); ?></p>
                                </div>
                                <?php $link_externo_gd = get_sub_field( 'link_externo_gd' ); ?>
                                <?php if ( $link_externo_gd ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_gd['url']; ?>"
                                        target="<?php echo $link_externo_gd['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_gd['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'guia_lideres' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'guia_lideres' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_gl' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_gl = get_sub_field( 'fondo_gl' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_gl['url']; ?>);"
                                title="<?php echo $fondo_gl['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_gl' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_gl' ); ?></p>
                                </div>
                                <?php $link_externo_gl = get_sub_field( 'link_externo_gl' ); ?>
                                <?php if ( $link_externo_gl ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_gl['url']; ?>"
                                        target="<?php echo $link_externo_gl['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_gl['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'evaluacion_ascendente' ) ) : ?>
            <div class="custom-small-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'evaluacion_ascendente' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_ea' ); ?></h3>
                    </div>
                    <div class="custom-small-box">
                        <div class="box-content">
                            <?php $fondo_ea = get_sub_field( 'fondo_ea' ); ?>
                            <div class="photo cover" style="background-image: url(<?php echo $fondo_ea['url']; ?>);"
                                title="<?php echo $fondo_ea['alt']; ?>">
                                <div class="veil"></div>
                            </div>
                            <div class="content-box">
                                <h4><?php the_sub_field( 'titulo_interno_ea' ); ?></h4>
                                <div class="bajada">
                                    <p><?php the_sub_field( 'bajada_ea' ); ?></p>
                                </div>
                                <?php $link_externo_ea = get_sub_field( 'link_externo_ea' ); ?>
                                <?php if ( $link_externo_ea ) { ?>
                                <div class="boton-mas">
                                    <a href="<?php echo $link_externo_ea['url']; ?>"
                                        target="<?php echo $link_externo_ea['target']; ?>"
                                        class="btn is-verde is-rounded size-s"><?php echo $link_externo_ea['title']; ?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="row-area">
            <?php if ( have_rows( 'manual_principios' ) ) : ?>
            <div class="manual-file-area box-section box-1-wide">
                <div class="content">
                    <?php while ( have_rows( 'manual_principios' ) ) : the_row(); ?>
                    <?php $archivo_mp = get_sub_field( 'archivo_mp' );
                    $url = wp_get_attachment_url( $archivo_mp );
                    $title = get_the_title( $archivo_mp );
                    $filesize = filesize( get_attached_file( $archivo_mp ) );
                    $filesize = size_format($filesize, 2);
                    $path_info = pathinfo( get_attached_file( $archivo_mp ) );
                    ?>
                    <div class="heading-box-area">
                        <h3 class="head-title"><?php the_sub_field( 'titulo_caja_mp' ); ?></h3>
                    </div>
                    <div class="file-box">
                        <div class="file-size">
                            <div class="icono">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/file-icon.svg" alt="">
                            </div>
                            <div class="data">
                                <span class="title">Tamaño</span>
                                <span class="size"><?php echo $path_info['extension']; ?>
                                    <?php echo $filesize; ?></span>
                            </div>
                        </div>
                        <div class="file-info">
                            <div class="file-main-data">
                                <h4 class="file-name"><?php the_sub_field( 'titulo_interno_mp' ); ?></h4>
                                <p class="file-desc"><?php the_sub_field( 'bajada_mp' ); ?></p>
                            </div>
                            <div class="file-link">
                                <a href="<?php echo $url; ?>"
                                    class="btn is-verde size-s is-rounded is-bordered has-icon"><i
                                        class="icon-descargar"></i><span>Descargar</span></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if ( have_rows( 'contacto_recursos_humanos' ) ) : ?>
            <div class="contacto-person-small box-section box-2-wide">
                <div class="content">
                    <?php while ( have_rows( 'contacto_recursos_humanos' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title">Contacto</h3>
                    </div>
                    <div class="contact-link-box layout-dos">
                        <div class="icono-area">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.svg" alt="">
                        </div>
                        <div class="content-area">
                            <h4><?php the_sub_field( 'titulo_contacto_rs' ); ?></h4>
                            <p><?php the_sub_field( 'bajada_contacto_rs' ); ?></p>
                            <?php $link_contacto_rs = get_sub_field( 'link_contacto_rs' ); ?>
                            <?php if ( $link_contacto_rs ) { ?>
                            <a href="<?php echo $link_contacto_rs['url']; ?>"
                                target="<?php echo $link_contacto_rs['target']; ?>"
                                class="btn is-verde is-rounded size-s"><?php echo $link_contacto_rs['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php } ?>
<?php if ( have_rows( 'archivos_externos_bi' ) ) : $i = 1;?>
<?php while ( have_rows( 'archivos_externos_bi' ) ) : the_row(); ?>
<div data-id="inciativa-modal-<?php echo $i; ?>" class="modal modal-iniciativa">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-heading" style="background-color: <?php the_sub_field( 'color_rel_ae_bi' ); ?>;">
            <div class="title-box">
                <h4><?php the_sub_field( 'titulo_ae_bi' ); ?></h4>
            </div>
            <?php $icono_rel_ae_bi = get_sub_field( 'icono_rel_ae_bi' ); ?>
            <?php if ( $icono_rel_ae_bi ) { ?>
            <div class="icono-box">
                <img src="<?php echo $icono_rel_ae_bi['url']; ?>" alt="<?php echo $icono_rel_ae_bi['alt']; ?>" />
            </div>
            <?php } ?>
        </div>
        <div class="modal-contenido wysiwyg">
            <?php the_sub_field( 'intro_ae_bi' ); ?>
            <?php the_sub_field( 'descripcion_ae_bi' ); ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $i++; endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
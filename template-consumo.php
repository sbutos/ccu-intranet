<?php
/*
* Template Name: Consumo Responsable
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading consumo-heading">
            <?php
            $pageThumbImg = get_the_post_thumbnail_url();
            $pageThumbnailID = get_post_thumbnail_ID();
            $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <h2><?php the_field( 'intro_titulo' ); ?></h2>
                    <p><?php the_field( 'intro_texto' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section content-page-area-consumo">
    <div class="wrap-xl">
        <div class="block-content wysiwyg">
            <?php the_field( 'descripcion_sus_cons' ); ?>
        </div>
        <?php $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
        if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false) || preg_match('/Edge/i', $ua) ) { ?>
        <style type="text/css">
        .pie {
            clip: rect(0em, 30vh, 60vh, 0em);
        }

        .hold {
            clip: rect(0em, 60vh, 60vh, 30vh);
        }
        </style>
        <?php } ?>

        <?php if( have_rows('item_grafico_consumo_responsable')): ?>
        <div class="grafico-consumo">
            <div class="consumo-responsable relative text-center">
                <?php
					$items = get_field('item_grafico_consumo_responsable');
			    	$size = 360/count($items);
			    	$slice_rotate = 0;
			    	$count = 0;
			    	?>

                <div class="box-pie col-100 clearfix text-center">
                    <div class="velo-container-characteristics"></div>
                    <div class="pieContainer transition-slower">
                        <div class="pieBackground"></div>
                        <?php
				            	foreach ($items as $key => $item): ?>
                        <div id="pieSlice<?php echo $key; ?>" class="hold"
                            style="transform: rotate(<?php echo $slice_rotate; ?>deg)">
                            <div class="pie"
                                style="background-color: <?php echo $item['color_item']; ?>; transform:rotate(<?php echo $size ?>deg);">
                            </div>
                        </div>
                        <?php $slice_rotate += $size;
					            endforeach; ?>

                        <div class="innerCircle">
                            <div class="content">
                                <?php echo get_field('texto_central_grafico_light_consumo_responsable'); ?></br><b><?php echo get_field('texto_central_grafico_bold_consumo_responsable'); ?></b>
                            </div>
                        </div>

                        <?php
							$quarter_size = $size/4;
				        	$slice_rotate = 0;

				        	?>
                        <ul class="circle-container-icons">
                            <?php
				        		foreach ($items as $key => $item):
				        			$slice_rotate_update = $slice_rotate - 3*$quarter_size;
	                                $slice_rotate_update_2 = $slice_rotate_update*-1;
				        		?>
                            <li class="item-graphic bold uppercase" data-item="<?php echo $key; ?>"
                                style="color:<?php echo $item['color_item'] ?>;transform: rotate(<?php echo $slice_rotate_update; ?>deg) translate(40vh, -10vh) rotate(<?php echo $slice_rotate_update_2; ?>deg);">
                                <div>

                                    <span class="transition"><?php echo $item['titulo_item'] ?></span>
                                    <div class="veil background transition-slow"
                                        style="background-color:<?php echo $item['color_item'] ?>">
                                    </div>
                            </li>
                            <?php  $slice_rotate += $size;
					            endforeach; ?>
                        </ul>
                    </div>
                </div>

                <div class="container-characteristics">
                    <?php
		        		wp_reset_postdata();
		        		$count = 0;
		        		while ( have_rows('item_grafico_consumo_responsable') ) : the_row(); ?>

                    <div class="characteristics characteristic-<?php echo $count; ?>">
                        <span class="close transition" style="color: <?php echo get_sub_field('color_item'); ?>;">
                            <i class="icon-equis"></i>
                        </span>
                        <div class="title">
                            <div class="icono-area">
                                <img src="<?php echo get_sub_field('icono_item'); ?>" alt="">
                            </div>
                            <div class="title-area">
                                <h1 style="color: <?php echo get_sub_field('color_item'); ?>;">
                                    <?php echo get_sub_field('titulo_item'); ?>
                                </h1>
                            </div>

                        </div>

                        <?php if( have_rows('caracteristicas')): ?>
                        <ul class="list regular">
                            <?php while ( have_rows('caracteristicas') ) : the_row(); ?>
                            <li>
                                <?php echo get_sub_field('caracteristica'); ?>
                            </li>
                            <?php endwhile; ?>
                        </ul>
                        <?php endif; ?>
                        <style>
                        .characteristic-<?php echo $count.' ';

                        ?>ul li:before {
                            background-color: <?php echo get_sub_field('color_item');
                            ?> !important;
                        }
                        </style>
                    </div>
                    <?php $count++; endwhile; ?>
                </div><!-- container-characteristics -->
            </div><!-- consumo-responsable -->
        </div><!-- col-100 -->

        <script type="text/javascript">
        $(document).ready(function() {
            $('.item-graphic').click(function() {
                $('.pieContainer').addClass('activePie');
                $('.item-graphic').removeClass('active-item');
                $(this).addClass('active-item');
                numItem = $(this).attr('data-item');
                $('.characteristics').hide();
                $('.characteristic-' + numItem).fadeIn();
                $('.velo-container-characteristics').addClass('aparecer');
            });

            $('.container-characteristics .close').click(function() {
                $('.pieContainer').removeClass('activePie');
                $('.item-graphic').removeClass('active-item');
                $('.characteristics').fadeOut();
                $('.velo-container-characteristics').removeClass('aparecer');
            });
        });
        </script>
        <?php endif; ?>
        <?php /* if ( have_rows( 'iniciativas' ) ) : $m = 1; ?>
        <div class="content manuales-section">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_iniciativas' ); ?></h3>
            </div>
            <div class="manuales-area">
                <?php while ( have_rows( 'iniciativas' ) ) : the_row(); ?>
                <a href="#" class="manual-box modal-trigger" data-id="inciativa-modal-<?php echo $m; ?>">
                    <?php $iniciativas_fotografia = get_sub_field( 'iniciativas_fotografia' ); ?>
                    <div class="rel-image cover"
                        style="background-image: url(<?php echo $iniciativas_fotografia['url']; ?>);"
                        title="<?php echo $iniciativas_fotografia['alt']; ?>">

                        <?php $iniciativas_icono = get_sub_field( 'iniciativas_icono' ); ?>
                        <?php if ( $iniciativas_icono ) { ?>
                        <div class="icono-box">
                            <img src="<?php echo $iniciativas_icono['url']; ?>"
                                alt="<?php echo $iniciativas_icono['alt']; ?>" />
                        </div>
                        <?php } ?>
                    </div>
                    <div class="title-box">
                        <h2><?php the_sub_field( 'iniciativas_titulo' ); ?></h2>
                    </div>
                </a>
                <?php $m++; endwhile; ?>
            </div>
        </div>
        <?php endif; */ ?>
        <div class="politica-consumo col-100 text-center">
            <p><?php the_field( 'politica_consumo_responsable_CTA' ); ?><br></p>
            <?php $politica_consumo_responsable_link = get_field( 'politica_consumo_responsable_link' ); ?>
            <?php if ( $politica_consumo_responsable_link ) { ?>
            <a class="btn is-verde is-rounded size-s" href="<?php echo $politica_consumo_responsable_link['url']; ?>"
                target="<?php echo $politica_consumo_responsable_link['target']; ?>"><?php echo $politica_consumo_responsable_link['title']; ?></a>
            <?php } ?>
        </div>
    </div>
</section>
<?php /* if ( have_rows( 'timeline' ) ) : ?>
<section id="campanas-timeline" class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title">Campañas</h3>
            </div>
            <section class="slider-3-container col-100 left clearfix relative">
                <div class="slider-3 col-100 left clearfix relative">
                    <?php while ( have_rows( 'timeline' ) ) : the_row(); ?>
                    <div class="slide col-100 left clearfix relative overflow-hidden">
                        <div class="content">
                            <div class="content-area col-45">
                                <p class="year"><?php the_sub_field( 'timeline_ano' ); ?></p>
                                <?php if ( have_rows( 'timeline_hitos' ) ) : ?>
                                <div class="description">
                                    <?php while ( have_rows( 'timeline_hitos' ) ) : the_row(); ?>
                                    <p><?php the_sub_field( 'timeline_hitos_hito' ); ?></p>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                            <?php $timeline_fotografia = get_sub_field( 'timeline_fotografia' ); ?>
                            <div class="image-area col-45">
                                <img src="<?php echo $timeline_fotografia['url']; ?>"
                                    alt="<?php echo $timeline_fotografia['alt']; ?>">
                            </div>
                        </div><!-- content -->
                        <div class="veil"></div>
                        <img class="photo cover" src="<?php echo $timeline_fotografia['url']; ?>"
                            alt="<?php echo $timeline_fotografia['alt']; ?>">
                    </div><!-- slide -->
                    <?php endwhile; ?>
                </div><!-- slider-3 -->

                <ul class="slider-nav-3">
                    <?php while ( have_rows( 'timeline' ) ) : the_row(); ?>
                    <li><span><?php the_sub_field( 'timeline_ano' ); ?></span></li>
                    <?php endwhile; ?>
                </ul>
            </section><!-- slider-3-container -->
        </div>
    </div>
</section>
<?php endif; */ ?>
<?php if ( have_rows( 'links_externos' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="form-link-area">
            <div class="grid-column-2 gap-m">
                <?php while ( have_rows( 'links_externos' ) ) : the_row(); ?>
                <div class="form-link-box">
                    <div class="content">
                        <h4><?php the_sub_field( 'titulo_lex' ); ?></h4>
                        <p><?php the_sub_field( 'descripcion_lex' ); ?></p>
                    </div>
                    <?php $link_lex = get_sub_field( 'link_lex' ); ?>
                    <?php if ( $link_lex ) { ?>
                    <div class="button-area">
                        <a href="<?php echo $link_lex['url']; ?>" target="<?php echo $link_lex['target']; ?>"
                            class="btn is-verde size-s is-rounded"><?php echo $link_lex['title']; ?></a>
                    </div>
                    <?php } ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section class="section noticias-consumo-area">
    <div class="wrap-xl">
        <?php if ( have_rows( 'noticias_consumo' ) ) : ?>
        <div class="news-area layout-one-last-feat">
            <?php while ( have_rows( 'noticias_consumo' ) ) : the_row(); ?>
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_sub_field( 'titulo_cr' ); ?></h3>
                    <?php $link_todos_con_term = get_sub_field( 'link_todos_con' ); ?>
                    <?php if ( $link_todos_con_term ) { ?>
                    <a href="<?php echo site_url('/'); ?>repositorio/"
                        data-this-tax="<?php echo $link_todos_con_term->slug; ?>" class="btn-ver-todas"><span>Ver
                            Todas</span><i class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>

                <div class="layout-news-area">
                    <?php $noticias_cr = get_sub_field( 'noticias_cr' ); ?>
                    <?php if ( $noticias_cr ): ?>
                    <?php foreach ( $noticias_cr as $post ):  ?>
                    <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="small-news-area border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="post-cat-area">
                                <?php $category_detail=get_the_category($post->ID);//$post->ID
                            foreach($category_detail as $cd){
                            echo '<span>'.$cd->cat_name.'</span>';
                            } ?>
                            </div>
                            <div class="content-area">
                                <div class="post-info">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                                <div class="button-area">
                                    <a href="<?php the_permalink(); ?>"
                                        class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php if ( have_rows( 'iniciativas' ) ) : $i = 1;?>
<?php while ( have_rows( 'iniciativas' ) ) : the_row(); ?>
<div data-id="inciativa-modal-<?php echo $i; ?>" class="modal modal-iniciativa">
    <i class="close icon-equis"></i>
    <div class="content-modal">
        <div class="modal-heading" style="background-color: <?php the_sub_field( 'iniciativas_color' ); ?>;">
            <div class="title-box">
                <h4><?php the_sub_field( 'iniciativas_titulo' ); ?></h4>
            </div>
            <?php $iniciativas_icono = get_sub_field( 'iniciativas_icono' ); ?>
            <?php if ( $iniciativas_icono ) { ?>
            <div class="icono-box">
                <img src="<?php echo $iniciativas_icono['url']; ?>" alt="<?php echo $iniciativas_icono['alt']; ?>" />
            </div>
            <?php } ?>
        </div>
        <div class="modal-contenido wysiwyg">
            <?php the_sub_field( 'iniciativas_introduccion' ); ?>
            <?php the_sub_field( 'iniciativas_descripcion' ); ?>
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $i++; endwhile; ?>
<?php endif; ?>
<script>
$(document).ready(function() {
    $('.slider-3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        speed: 700,
        asNavFor: '.slider-nav-3',
        responsive: [{
            breakpoint: 851,
            settings: {
                arrows: false,
            }
        }]
    });

    $('.slider-nav-3').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-3',
        dots: false,
        centerMode: false,
        arrows: false,
        focusOnSelect: true,
        vertical: false,
        responsive: [{
                breakpoint: 851,
                settings: {
                    vertical: false,
                }
            },
            {
                breakpoint: 641,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    vertical: false,
                }
            }
        ]
    });
    equalOuterHeight($(".slider-3 .slide .content"));
});
</script>
<?php get_footer(); ?>
<!doctype html>
<html lang="es" <?php language_attributes() ?>>

<head profile="http://gmpg.org/xfn/11">
    <title><?php echo esc_html( get_bloginfo('name'), 1 ); wp_title( '|', true, 'left' ); ?></title>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
    <?php wp_head() ?>

    <!-- API YouTube -->
    <script src="https://www.youtube.com/iframe_api"></script>

    <!-- Scripts utilizados en el tema -->
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/androidstudio.css" />
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/highlight.pack.js"
        type="text/javascript"></script>
    <script>
    hljs.initHighlightingOnLoad();
    </script>


    <!-- Responsive -->
    <meta name="viewport"
        content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
</head>

<body class="login-page">
    <header id="login-header">
        <div class="brand-area">
            <a href="<?php echo site_url('/'); ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu-c.svg" alt="" class="logo-login">
            </a>
        </div>
    </header>

    <main>
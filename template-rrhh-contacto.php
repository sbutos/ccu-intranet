<?php
/*
* Template Name: Recursos Humanos - Contacto
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="contact-link-box">
                <div class="icono-area">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/phone-icon.svg" alt="">
                </div>
                <div class="heading-area">
                    <h4><?php the_field( 'titulo_rrhh_c' ); ?></h4>
                </div>
                <div class="info-area">
                    <p><?php the_field( 'bajada_rrhh_c' ); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if ( have_rows( 'personas_rrhh_c' ) ) : ?>
<section class="section contact-persons-area">
    <div class="wrap-xl">
        <div class="content">
            <div class="persons-area">
                <?php while ( have_rows( 'personas_rrhh_c' ) ) : the_row(); ?>
                <div class="contact-person-box col-32">
                    <div class="cargo-person">
                        <h3><?php the_sub_field( 'cargo_p_rrhh_c' ); ?></h3>
                    </div>
                    <div class="info-person">
                        <h4 class="nombre"><?php the_sub_field( 'nombre_p_rrhh_c' ); ?></h4>
                        <a href="tel:<?php the_sub_field( 'telefono_p_rrhh_c' ); ?>"
                            class="info-link"><?php the_sub_field( 'telefono_p_rrhh_c' ); ?></a>
                        <a href="mailto:<?php the_sub_field( 'correo_p_rrhh_c' ); ?>"
                            class="info-link"><?php the_sub_field( 'correo_p_rrhh_c' ); ?></a>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php get_footer(); ?>
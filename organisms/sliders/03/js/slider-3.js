$(document).ready(function() {
	$('.slider-3').slick({
		slidesToShow: 1,
	  slidesToScroll: 1,
	  arrows: false,
	  fade: false,
		speed: 700,
		arrows: true,
		asNavFor: '.slider-nav-3',
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
				arrows: false,
		  }
		}
		]
	});

	$('.slider-nav-3').slick({
		slidesToShow: 3,
	  slidesToScroll: 1,
	  asNavFor: '.slider-3',
	  dots: false,
	  centerMode: true,
		arrows: false,
	  focusOnSelect: true,
		vertical: false,
		responsive: [
		{
		  breakpoint: 851,
		  settings: {
				vertical: false,
		  }
		},
		{
		  breakpoint: 641,
		  settings: {
		    slidesToShow: 2,
				slidesToScroll: 2,
				vertical: false,
		  }
		}
		]
	});

	equalOuterHeight($(".slider-3 .slide .content"));

});

$(document).ready(function() {
	var i = 0;
	$( ".count" ).each(function() {
		var idCounter = "counter"+ ++i;
		var cifra1 = $(this).text(); // Cifra ingresada back
		var primerCaracter = cifra1.charAt(0); // Primer carácter de la cifra
		var ultimoCaracter = cifra1.charAt(cifra1.length-1);  // Úlitmo carácter de la cifra
		var cifraClean = cifra1.replace(/[^0-9]/g,''); // Cifra sanitizada (solo números)
		var CifraConPunto = cifra1.replace(/,/g, '.'); // Cifra con coma pasada a puntos
		var StrAfterComa = cifra1.substr(cifra1.indexOf(",") + 1); // string despues de la coma
		var StrAfterComaClean = StrAfterComa.replace(/[^0-9]/g,''); // string sanitizado despues de la coma
		var decimalPlaces = StrAfterComaClean.replace(/ /g,'').length // número de digitos después de la coma

		//Si el primer caracter es numérico pasa a ser prefijo :
		if($.isNumeric(primerCaracter)){
				var prefix = "";
		}else {
				var prefix = primerCaracter;
		}

		//Si el último caracter es numérico pasa a ser sufijo :
		if($.isNumeric(ultimoCaracter)){
				var sufix = "";
		}else {
				var sufix = ultimoCaracter;
		}

		//Si la cifra contiene una coma :
		if (cifra1.indexOf(',') !== -1){
				var separator = "";
				var numeroDecimales = decimalPlaces;
				var decimal = ","
				var CifraFinal = CifraConPunto
				// tiene ,
		}else {
				var separator = ".";
				var numeroDecimales = "";
				var decimal = "";
				var CifraFinal = cifraClean
		}

		// counter, cifrainicial, cifrafinal, numerodecimales, duración
		var c = new CountUp(this,0,CifraFinal,numeroDecimales,0,{
				separator: separator,
				decimal: decimal,
				prefix: prefix,
				suffix: sufix

		});

		$(this).one('inview', function(event, isInView) {
			if (isInView) {
				c.start();
			}//endif
		});

	});
})

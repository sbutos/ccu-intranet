<!doctype html>
<html lang="es" <?php language_attributes() ?>>

<head profile="http://gmpg.org/xfn/11">
    <title><?php echo esc_html( get_bloginfo('name'), 1 ); wp_title( '|', true, 'left' ); ?></title>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url') ?>" />
    <?php wp_head() ?>

    <!-- API YouTube -->
    <script src="https://www.youtube.com/iframe_api"></script>

    <!-- Scripts utilizados en el tema -->
    <link rel="stylesheet" type="text/css"
        href="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/androidstudio.css" />
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/components/custom/js/highlight/highlight.pack.js"
        type="text/javascript"></script>
    <script>
    hljs.initHighlightingOnLoad();
    </script>


    <!-- Responsive -->
    <meta name="viewport"
        content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
</head>

<body>
    <header id="side-header">
        <div class="brand-area">
            <a href="<?php echo site_url('/'); ?>" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu.svg" alt="" class="logo-site">
            </a>
        </div>
        <div class="main-menu-area">
            <ul class="main-menu">
                <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu principal', 'container_class' => false, 'container' => false));?>
            </ul>
        </div>
        <div class="extra-links-area">
            <div class="menu-heading">
                <p>Link de Interés</p>
            </div>
            <div class="extra-menu-area">
                <ul class="extra-menu">
                    <?php wp_nav_menu(array('items_wrap' => '%3$s', 'theme_location'=>'Menu extra', 'container_class' => false, 'container' => false));?>
                </ul>
            </div>
        </div>

        <div id="hamburger" class="right relative clearfix menu-trigger menu-right-button right transition-slower">
            <span class="line-1"></span>
            <span class="line-2"></span>
            <span class="line-3"></span>
        </div>
    </header>

    <main>
        <div class="top-header">
            <div class="pre-head">
                <div class="wrap-xl">
                    <div class="content">
                        <div class="currency-area">
                            <div class="dolar-valor">
                                <i class="icon-indicators"></i>
                                <span class="name">Valor del dolar</span>
                                <span class="value">1 USD</span>
                                <i class="icon-arrow-right"></i>
                                <span class="value">856,23 CLP</span>
                            </div>
                            <div class="splitter"></div>
                            <div class="uf-valor">
                                <span class="name">Valor UF</span>
                                <span class="value">1 UF</span>
                                <i class="icon-arrow-right"></i>
                                <span class="value">28,664 CLP</span>
                            </div>
                        </div>
                        <div id="time-area" class="time-area">
                            <i class="icon-clock"></i>
                            <a href="#" class="prev arrow"><i class="icon-chevron-left"></i></a>
                            <div class="time-slider-area">
                                <div id="time-slider">
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Chile</span>
                                            <span id="chile" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="0">
                                            <span>Hora Local</span>
                                            <span class="country">Argentina</span>
                                            <span id="argentina" class="time"></span>
                                        </div>
                                    </div>
                                    <div class="slide">
                                        <div class="time-data" data-timezone="-1">
                                            <span>Hora Local</span>
                                            <span class="country">Bolivia</span>
                                            <span id="bolivia" class="time"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="#" class="next arrow"><i class="icon-chevron-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mid-head">
                <div class="wrap-xl">
                    <div class="content">
                        <div class="today-div">
                            <span id="current-date"></span>
                        </div>
                        <div class="countries-area">
                            <p class="slogan">Nos apasiona crear experiencia para compartir juntos un mejor vivir</p>
                            <div class="flags-area">
                                <ul class="flag-list">
                                    <li class="current-flag">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-chile.svg" alt=""
                                            class="flag"><span>Chile</span>
                                    </li>
                                    <li>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-argentina.svg"
                                            alt="" class="flag"><span>Argentina</span>
                                    </li>
                                    <li>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-bolivia.svg"
                                            alt="" class="flag"><span>Bolivia</span>
                                    </li>
                                    <li>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-colombia.svg"
                                            alt="" class="flag"><span>Colombia</span>
                                    </li>
                                    <li>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-paraguay.svg"
                                            alt="" class="flag"><span>Paraguay</span>
                                    </li>
                                    <li>
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/flag-uruguay.svg"
                                            alt="" class="flag"><span>Uruguay</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bot-head">
                <div class="wrap-xl">
                    <div class="content">
                        <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <div class="search-input-area">
                                <input type="search"
                                    placeholder="<?php echo esc_attr_x( 'Realiza una búsqueda', 'placeholder', 'base' ); ?>"
                                    value="<?php echo get_search_query(); ?>" name="s" id="search-box">
                                <button type="submit" class="btn-search"><i class="icon-buscar"></i></button>
                            </div>
                        </form>
                        <div class="user-area">
                            <div class="avatar-box">
                                <div class="avatar">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/avatar.jpeg" alt=""
                                        class="profile">
                                </div>
                            </div>
                            <div class="info-box">
                                <div class="name"><span>Tamara Rodríguez P.</span><span class="icono"><i
                                            class="icon-chevron-down"></span></i>
                                </div>
                                <div class="range"><span>Encargada Logistica</span></div>
                            </div>
                        </div>
                        <div class="user-menu">
                            <div class="links">
                                <a href="#" class="out-link">
                                    <i class="icon-out-link"></i><span>Portal Psoft</span>
                                </a>
                                <a href="#" class="out-link">
                                    <i class="icon-out-link"></i><span>Mi Gestión de Desempeño</span>
                                </a>
                                <a href="#" class="out-link">
                                    <i class="icon-out-link"></i><span>Mi Formación</span>
                                </a>
                                <a href="#" class="out-link">
                                    <i class="icon-out-link"></i><span>Mi Pons</span>
                                </a>
                            </div>
                            <div class="btn-area">
                                <a href="<?php echo site_url('/'); ?>escribenos/"
                                    class="btn is-verde is-rounded size-s">Escríbenos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
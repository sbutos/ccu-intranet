<?php
/*
* Template Name: Sustentabilidad
*/
get_header();
?>
<section class="section sub-heading-area">
    <div class="wrap-xl">
        <div class="head-page">
            <h1><?php the_title(); ?></h1>
        </div>
    </div>
</section>
<section class="section page-content-area">
    <div class="wrap-xl">
        <div class="page-content">
            <div class="wysiwyg">
                <?php the_field( 'parrafo_intro' ); ?>
                <div id="modelo-gestion">
                    <?php $ua = htmlentities($_SERVER['HTTP_USER_AGENT'], ENT_QUOTES, 'UTF-8');
                if (preg_match('~MSIE|Internet Explorer~i', $ua) || (strpos($ua, 'Trident/7.0') !== false && strpos($ua, 'rv:11.0') !== false) || preg_match('/Edge/i', $ua) ) { ?>
                    <style type="text/css">
                    .pie {
                        clip: rect(0em, 30vh, 60vh, 0em);
                    }

                    .hold {
                        clip: rect(0em, 60vh, 60vh, 30vh);
                    }
                    </style>
                    <?php } ?>

                    <?php if( have_rows('item_grafico_sustentabilidad')): ?>
                    <div class="col-100 left clearfix relative text-center background-gray">
                        <div class="consumo-responsable relative text-center">
                            <?php
                            $items = get_field('item_grafico_sustentabilidad');
                            $size = 360/count($items);
                            $slice_rotate = 0;
                            $count = 0;
                            ?>

                            <div class="box-pie col-100 clearfix text-center transition-slower">
                                <div class="velo-container-characteristics"></div>
                                <!--Gráfico SVG -->
                                <?php if ( get_field('svg_modelo_sustentabilidad') ) { ?>
                                <div id="model" class="col-100 left transition-slower">
                                    <div class="wrap clearfix">
                                        <div class="img">
                                            <p><?php echo the_field('svg_modelo_sustentabilidad') ;?></p>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <!--Fin Gráfico SVG-->
                            </div>

                            <div class="container-characteristics">
                                <?php
                                wp_reset_postdata();
                                $count = 1;
                                while ( have_rows('item_grafico_sustentabilidad') ) : the_row(); ?>

                                <div class="characteristics characteristics-sustentability-<?php echo $count; ?>">
                                    <span class="close transition"
                                        style="color: <?php echo get_sub_field('color_item'); ?>;">
                                        <i class="icon-equis"></i>
                                    </span>
                                    <div class="title">
                                        <div class="icono-area">
                                            <img src="<?php echo get_sub_field('icono_item'); ?>" alt="">
                                        </div>
                                        <div class="title-area">
                                            <h1 style="color: <?php echo get_sub_field('color_item'); ?>;">
                                                <?php echo get_sub_field('titulo_item'); ?>
                                            </h1>
                                        </div>

                                    </div>

                                    <?php
                                        if( have_rows('caracteristicas')): ?>
                                    <ul class="list roboto regular">
                                        <?php
                                            while ( have_rows('caracteristicas') ) : the_row(); ?>
                                        <li>
                                            <a
                                                href="<?php the_sub_field( 'enlace' ); ?>"><?php echo get_sub_field('caracteristica'); ?></a>
                                        </li>
                                        <?php endwhile; ?>
                                    </ul>
                                    <?php
                                        endif; ?>
                                    <style>
                                    .characteristics-sustentability-<?php echo $count.' ';

                                    ?>ul li:before {
                                        background-color: <?php echo get_sub_field('color_item');
                                        ?> !important;
                                    }
                                    </style>
                                </div>

                                <?php $count++; endwhile; ?>
                            </div><!-- container-characteristics -->
                        </div><!-- consumo-responsable -->
                    </div><!-- col-100 -->

                    <script type="text/javascript">
                    $(document).ready(function() {
                        $(".item-sustentabilidad").click(function(argument) {
                            dataItem = $(this).attr('data-item');
                            console.log(dataItem);
                            $('#model').addClass('activePie');
                            $(".characteristics").hide();
                            $('.characteristics-sustentability-' + dataItem)
                                .fadeIn(); //cambiar a characteristiacs-st7 por ejemplo
                            $('.velo-container-characteristics').addClass('aparecer');
                        });

                        $('.container-characteristics .close').click(function() {
                            $('#model').removeClass('activePie');
                            $('.item-graphic').removeClass('active-item');
                            $('.characteristics').fadeOut();
                            $('.velo-container-characteristics').removeClass('aparecer');
                        });
                    });
                    </script>
                    <?php endif; ?>
                </div>
                <?php $link_externo_sus = get_field( 'link_externo_sus' ); ?>
                <p class="text-center"><?php the_field( 'cta_link_externo_sus' ); ?></p>
                <?php if ( $link_externo_sus ) { ?>
                <p class="text-center">
                    <a href="<?php echo $link_externo_sus['url']; ?>"
                        target="<?php echo $link_externo_sus['target']; ?>"
                        class="btn is-rounded is-verde-oscuro size-s"><?php echo $link_externo_sus['title']; ?></a>
                </p>
                <?php } ?>
                <?php the_field( 'intro_compromiso' ); ?>
                <?php if ( have_rows( 'cajas_ods' ) ) : ?>
                <div class="ods-boxes">
                    <?php while ( have_rows( 'cajas_ods' ) ) : the_row(); ?>
                    <?php $imagen_ods = get_sub_field( 'imagen_ods' ); ?>
                    <?php if ( $imagen_ods ) { ?>
                    <img src="<?php echo $imagen_ods['url']; ?>" alt="<?php echo $imagen_ods['alt']; ?>"
                        class="ods-box" />
                    <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="shortcuts-area">
                <h4><?php the_field( 'titulo_indicadores' ); ?></h4>
                <div class="shortcuts-boxes">
                    <?php $boton_eje_personas = get_field( 'boton_eje_personas' ); ?>
                    <?php if ( $boton_eje_personas ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_personas['url']; ?>"
                            target="<?php echo $boton_eje_personas['target']; ?>"
                            class="btn size-s is-rounded is-burdeo"><?php echo $boton_eje_personas['title']; ?></a>
                    </div>
                    <?php } ?>
                    <?php $boton_eje_marcas = get_field( 'boton_eje_marcas' ); ?>
                    <?php if ( $boton_eje_marcas ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_marcas['url']; ?>"
                            target="<?php echo $boton_eje_marcas['target']; ?>"
                            class="btn size-s is-rounded is-celeste"><?php echo $boton_eje_marcas['title']; ?></a>
                    </div>
                    <?php } ?>
                    <?php $boton_eje_planeta = get_field( 'boton_eje_planeta' ); ?>
                    <?php if ( $boton_eje_planeta ) { ?>
                    <div class="shortcut-box">
                        <a href="<?php echo $boton_eje_planeta['url']; ?>"
                            target="<?php echo $boton_eje_planeta['target']; ?>"
                            class="btn size-s is-rounded is-amarillo"><?php echo $boton_eje_planeta['title']; ?></a>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_field( 'titulo_informes' ); ?></h3>
                    <?php $link_ver_todos_sus = get_field( 'link_ver_todos_sus' ); ?>
                    <?php if ( $link_ver_todos_sus ) { ?>
                    <a href="<?php echo $link_ver_todos_sus['url']; ?>"
                        target="<?php echo $link_ver_todos_sus['target']; ?>" class="btn-ver-todas">
                        <span><?php echo $link_ver_todos_sus['title']; ?></span><i class="icon-chevron-right"></i>
                    </a>
                    <?php } ?>
                </div>
                <?php if ( have_rows( 'archivo_informes' ) ) : ?>
                <div class="informes-area">
                    <?php while ( have_rows( 'archivo_informes' ) ) : the_row(); ?>
                    <?php if ( get_sub_field( 'archivo_pdf' ) ) { ?>
                    <a href="<?php the_sub_field( 'archivo_pdf' ); ?>" download="download" class="informe-box">
                        <?php $portada = get_sub_field( 'portada' ); ?>
                        <div class="portada">
                            <div class="portada-img cover" style="background-image: url(<?php echo $portada['url']; ?>)"
                                title="<?php echo $portada['alt']; ?>"></div>
                        </div>
                        <div class="titulo">
                            <h4>Informe de Sustentabilidad</h4>
                        </div>
                    </a>
                    <?php } ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <div class="content videos-charlas-area">
                <div class="news-area layout-one-four-feat">
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title"><?php the_field( 'titulo_charlas' ); ?></h3>
                            <?php $link_todos_v_term = get_field( 'link_todos_v' ); ?>
                            <?php if ( $link_todos_v_term) { ?>
                            <a href="<?php echo site_url('/'); ?>repositorio-videos/"
                                data-this-tax="<?php echo $link_todos_v_term->slug; ?>" class="btn-ver-todas"><span>Ver
                                    Todas</span><i class="icon-chevron-right"></i></a>
                            <?php } ?>
                        </div>

                        <div class="layout-news-area">
                            <?php $videos_charlas = get_field( 'videos_charlas' ); ?>
                            <?php if ( $videos_charlas ): ?>
                            <?php foreach ( $videos_charlas as $post ): $c = 1; ?>
                            <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <a href="<?php the_permalink($post); ?>" class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php
                                        $categories = get_the_category();
                                        $comma      = ' ';
                                        $output     = '';
                                        
                                        if ( $categories ) {
                                            foreach ( $categories as $category ) {
                                                $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                            }
                                            echo trim( $output, $comma );
                                        } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="play-area modal-trigger" data-id="modal-video-<?php echo $c; ?>"
                                            data-video-url="<?php the_field('id_video_youtube'); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <?php $c++; endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content noticias-sustentabilidad-area">
                <div class="news-area layout-one-third">
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title"><?php the_field( 'titulo_noticias_sus' ); ?></h3>
                            <?php $news_cat_ids = get_field( 'news_cat' ); ?>
                            <?php if ( $news_cat_ids) { ?>
                            <a href="<?php echo site_url('/'); ?>repositorio/"
                                data-this-tax="<?php echo $news_cat_ids->slug; ?>" class="btn-ver-todas"><span>Ver
                                    Todas</span><i class="icon-chevron-right"></i></a>
                            <?php } ?>
                        </div>

                        <div class="layout-news-area">
                            <?php $noticias_sus = get_field( 'noticias_sus' ); ?>
                            <?php if ( $noticias_sus ): ?>
                            <?php foreach ( $noticias_sus as $post ):  ?>
                            <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <div class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php
                                        $categories = get_the_category();
                                        $comma      = ' ';
                                        $output     = '';
                                        
                                        if ( $categories ) {
                                            foreach ( $categories as $category ) {
                                                $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                            }
                                            echo trim( $output, $comma );
                                        } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="post-info">
                                            <span class="fecha"><?php the_date(); ?></span>
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                        <div class="button-area">
                                            <a href="<?php the_permalink(); ?>"
                                                class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="comite-area">
                <h4 class="titulo"><?php the_field( 'titulo_comite' ); ?></h4>
                <div class="descripcion">
                    <?php the_field( 'parrafo_comite' ); ?>
                </div>
                <?php if ( have_rows( 'personas_comite' ) ) : ?>
                <div class="person-group-area">
                    <?php while ( have_rows( 'personas_comite' ) ) : the_row(); ?>
                    <div class="person-box">
                        <?php $foto_pc = get_sub_field( 'foto_pc' ); ?>
                        <div class="person-img cover" style="background-image: url(<?php echo $foto_pc['url']; ?>)"
                            title="<?php echo $foto_pc['alt']; ?>">
                        </div>
                        <div class="person-info">
                            <span class="cargo"><?php the_sub_field( 'cargo_pc' ); ?></span>
                            <h4 class="nombre"><?php the_sub_field( 'nombre_pc' ); ?></h4>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php $videos_gal_modal = get_field( 'videos_charlas' ); ?>
<?php if ( $videos_gal_modal ): $c = 1; ?>
<?php foreach ( $videos_gal_modal as $post ):  ?>
<?php setup_postdata ( $post ); ?>
<div data-id="modal-video-<?php echo $c; ?>" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $c++; endforeach; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>
<script>
$(document).ready(function() {
    $('.btn-ver-todas').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let permalink = $(this).attr('href');
            let taxonomia = $(this).data('this-tax');
            $("<form method='POST' action='" + permalink +
                    "'><input type='hidden' name='thistax' value='" + taxonomia + "'/></form>")
                .appendTo("body").submit();
        });
    });
});
</script>
<?php get_footer(); ?>
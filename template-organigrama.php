<?php
/*
* Template Name: Organigrama
*/
get_header();
?>
<section class="section buscador-organigrama">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title">Organigrama</h3>
            </div>
            <div class="buscador-heading">
                <div class="photo cover"
                    style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/buscador-organigrama.jpg)"
                    title="">
                    <div class="veil"></div>
                </div>
                <div class="buscador-box">
                    <h2>Buscar una persona</h2>
                    <div class="search-input-area">
                        <input type="search" name="global-search" id="search-box" placeholder="Buscar">
                        <a href="#" class="btn-search"><i class="icon-buscar"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section resultado-busqueda">
    <div class="wrap-xl">
        <div class="resultado-area">
            <div class="heading-area">
                <a href="#" class="back-btn"><i class="icon-chevron-left"></i><span>Volver</span></a>
                <div class="search-input-area">
                    <input type="search" name="global-search" id="search-box" placeholder="Buscar por Persona">
                    <a href="#" class="btn-search"><i class="icon-buscar"></i></a>
                </div>
            </div>
            <div class="resultado-content">
                <div class="person-data">
                    <div class="person-avatar-box">
                        <div class="profile-img cover"
                            style="background-image: url(<?php echo get_template_directory_uri() ?>/img/person-3.jpg);"
                            title=""></div>
                    </div>
                    <div class="person-info-box">
                        <h3 class="nombre">Francisca Alejandra Miranda Montes</h3>
                        <h4 class="cargo">Coordinador Departamento, Sub Dirección Oficina Uno</h4>
                        <div class="contact-info">
                            <a href="#" class="contact-link">
                                <span class="icono"><i class="icon-chat"></i></span><span
                                    class="texto">f.miranda@ccu.cl</span>
                            </a>
                            <a href="#" class="contact-link">
                                <span class="icono"><i class="icon-phone"></i></span><span class="texto">(56) 9 3452
                                    1212</span>
                            </a>
                            <a href="#" class="contact-link">
                                <span class="icono"><i class="icon-envelope"></i></span><span class="texto">Av. Vitacura
                                    2670,
                                    Santiago, Las Condes, Región Metropolitana, Chile</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="organigrama-data">
                    <div class="organigrama-container">
                        <div class="person-box level-one">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_template_directory_uri() ?>/img/person-1.jpg);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo">Gerente de Procesos Industriales</h4>
                                <h3 class="nombre">Luis Santibañez</h3>
                            </div>
                        </div>
                        <div class="person-box level-two current-person">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_template_directory_uri() ?>/img/person-3.jpg);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo">Coordinador Departamento</h4>
                                <h3 class="nombre">Francisca Miranda</h3>
                            </div>
                        </div>
                        <div class="person-box level-three">
                            <div class="person-avatar-box">
                                <div class="profile-img cover"
                                    style="background-image: url(<?php echo get_template_directory_uri() ?>/img/person-2.jpg);"
                                    title=""></div>
                            </div>
                            <div class="person-info-box">
                                <h4 class="cargo">Cargo Procesos</h4>
                                <h3 class="nombre">Sofia Gonzalez</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
<?php
/* Template Name: Átomos */
get_header();
?>

	<section id="contenedores">
		<div class="tabs">
			<p class="primary-title">Layout</p>
			<p class="secondary-title">Contenedores</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<p>Existen 4 contenedores:</p>
					<ul>
						<li><strong>.wrap-xl: </strong>Contenedor al 90% del ancho</li>
						<li><strong>.wrap-l: </strong>Contenedor al 80% del ancho</li>
						<li><strong>.wrap-m: </strong>Contenedor al 70% del ancho</li>
						<li><strong>.wrap-s: </strong>Contenedor al 60% del ancho</li>
					</ul>
					<p>Estos se adaptan automáticamente para dispositivos móviles. Las condicionantes son:</p>
					<ul>
						<li>En <strong>_responsive_tablet.scss</strong>, .wrap-m y .wrap-s pasan a tener 80% del ancho.</li>
						<li>En <strong>_responsive_phone.scss</strong>, .wrap-m y .wrap-s pasan a tener 90% del ancho.</li>
					</ul>
				</div>
				
				<div class="block wrap-xl text-center">.wrap-xl</div>
				<div class="block wrap-l text-center">.wrap-l</div>
				<div class="block wrap-m text-center">.wrap-m</div>
				<div class="block wrap-s text-center">.wrap-s</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="wrap-xl"&gt;Contenedor XL (genera un contenedor al 90% del sitio)&lt;/div&gt;
&lt;div class="wrap-l"&gt;Contenedor L (genera un contenedor al 80% del sitio)&lt;/div&gt;
&lt;div class="wrap-m"&gt;Contenedor M (genera un contenedor al 70% del sitio)&lt;/div&gt;
&lt;div class="wrap-s"&gt;Contenedor S (genera un contenedor al 60% del sitio)&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */
/*------------------------------------------------------*/
/*------------------- CONTENEDORES ---------------------*/
/*------------------------------------------------------*/
.wrap-xl,
.wrap-l,
.wrap-m,
.wrap-s{
  @extend .margin-center;
  @include clearfix;
}

.wrap-xl{
  @extend .col-90;
}

.wrap-l{
  @extend .col-80;
}

.wrap-m{
  @extend .col-70;
}

.wrap-s{
  @extend .col-60;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #contenedores -->


	<section id="float">
		<div class="tabs">
			<p class="secondary-title">Floats, margin y padding</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
					<div class="block left">Elemento a la izquierda</div>
					<div class="block right">Elemento a la derecha</div>
					<div class="block margin-center">Elemento al centro</div>
					<div class="block no-margin text-center">Elemento sin margen</div>
					<div class="block no-padding text-center">Elemento sin padding</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="left"&gt;Elemento a la izquierda&lt;/div&gt;
&lt;div class="right"&gt;Elemento a la derecha&lt;/div&gt;
&lt;div class="margin-center"&gt;Elemento al centro&lt;/div&gt;
&lt;div class="no-margin"&gt;Elemento sin margen&lt;/div&gt;
&lt;div class="no-padding"&gt;Elemento sin padding&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*------------- FLOATS, MARGIN Y PADDING ---------------*/
/*------------------------------------------------------*/
.left{
  float: left;
}

.right{
  float: right;
}

.margin-center{
  margin: 0 auto;
  display: table;
}

.no-margin{
  margin: 0 !important;
}

.no-padding{
  padding: 0 !important;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
p{
  @extend .left;
}

p{
  @extend .right;
}

p{
  @extend .margin-center;
}

p{
  @extend .no-margin;
}

p{
  @extend .no-padding;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #float -->



	<section id="position">
		<div class="tabs">
			<p class="secondary-title">Position</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Existen tres clases para las posiciones: <strong>.relative</strong>, <strong>.absolute</strong> y <strong>.fixed</strong>. De esta manera, y dependiendo de las necesidades del elemento, se le puede asignar una o varias posiciones para diferentes comportamientos.</p>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="relative"&gt;div con posición relative&lt;/div&gt;
&lt;div class="absolute"&gt;div con posición absolute&lt;/div&gt;
&lt;div class="fixed"&gt;div con posición fixed&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*--------------------- POSICIONES ---------------------*/
/*------------------------------------------------------*/
.relative{
  position: relative;
}

.absolute{
  position: absolute;
}

.fixed{
  position: fixed;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
div{
  @extend .relative;
}

div{
  @extend .absolute;
}

div{
  @extend .fixed;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #position -->




	<section id="columns">
		<div class="tabs">
			<p class="secondary-title">Columnas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Existe la clase <strong>.col-$i</strong>, donde <strong>$i</strong> se recorre <strong>100 veces</strong>, asignando un ancho por cada vez.</p>
				</div>

				<div class="block col-16">columna al 16%</div>
				<div class="block col-37">columna al 37%</div>
				<div class="block col-50">columna al 50%</div>
				<div class="block col-72">columna al 72%</div>
				<div class="block col-88">columna al 88%</div>
				<div class="block col-100">columna al 100%</div>

			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="col-16"&gt;columna al 16%&lt;/div&gt;
&lt;div class="col-37"&gt;columna al 37%&lt;/div&gt;
&lt;div class="col-50"&gt;columna al 50%&lt;/div&gt;
&lt;div class="col-72"&gt;columna al 72%&lt;/div&gt;
&lt;div class="col-88"&gt;columna al 88%&lt;/div&gt;
&lt;div class="col-100"&gt;columna al 100%&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
.col-1{
  width: 1%;
}

/*...y así sucesivamente, hasta llegar al 100*/

.col-100{
  width: 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */
/*------------------------------------------------------*/
/*--------------------- COLUMNAS -----------------------*/
/*------------------------------------------------------*/
/* Se realiza un for que recorre del 1 al 100. A cada una le aplica un width del for que recorre, obteniendo así 100 tamaños de columnas. */
@for $i from 1 through 100{
  .col-#{$i}{
    width: $i * 1%;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #columns -->



	<section id="grids">
		<div class="tabs">
			<p class="secondary-title">Grillas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<ul>
						<li>Existe la clase <strong>.grid-column-$i</strong>, donde <strong>$i</strong> se recorre <strong>12 veces</strong>, generando una grilla por este valor.</li>
						<li>Además, a este mismo div se le pueden agregar la clases <strong>.gap-s, .gap-m, .gap-l ó .gap-xl</strong>, la cual le añadirá un espaciado entre las columnas de valor <strong>$font*10, $font*20, $font*35 ó $font*50</strong> respectivamente.</li>
						<li>
							En <strong>_responsive_desktop.scss:</strong>
							<ul>
								<li>Desde grid-column-10 hasta grid-column-12 pasan a tener <strong>8 columnas</strong>.</li>
							</ul>
						</li>
						<li>
							En <strong>_responsive_tablet.scss:</strong>
							<ul>
								<li>Desde grid-column-6 hasta grid-column-12 pasan a tener <strong>4 columnas</strong>.</li>
								<li>grid-column-4, grid-column-5 pasan a tener <strong>3 columnas</strong>.</li>
							</ul>
						</li>
						<li>
							En <strong>_responsive_phone.scss:</strong>
							<ul>
								<li>Desde grid-column-2 hasta grid-column-12 pasan a tener <strong>1 columna</strong>.</li>
							</ul>
						</li>
						<li>
							También se pueden incorporar elementos que quiebren la grilla:
							<ul>
								<li>Si se quiere que un elemento tenga el 100% de ancho, incorporarle la clase <strong>.grid-column-full</strong></li>
							</ul>
						</li>
					</ul>
				</div>

				<div class="grid-column-1 block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-1</p>
					<div class="block"></div>
				</div>

				<div class="grid-column-2 gap-xl block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-2 .gap-xl</p>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-3 gap-l block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-3 .gap-l</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-4 gap-l block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-4 .gap-l</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-5 gap-m block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-5 .gap-m</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-6 gap-m block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-6 .gap-m</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-7 gap-m block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-7 .gap-m</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>
		
				<div class="grid-column-8 gap-s block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-8 .gap-s</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-9 gap-s block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-9 .gap-s</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-10 gap-s block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-10 .gap-s</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-11 gap-s block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-11 .gap-s</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-12 gap-s block" style="margin:20px 0;">
					<p class="grid-column-full text-center">.grid-column-12 .gap-s</p>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>

				<div class="grid-column-6 gap-s block" style="margin:20px 0;">
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block grid-column-full text-center">Elemento a todo el ancho, de clase .grid-column-full</div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
					<div class="block"></div>
				</div>
								
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="grid-column-1"&gt;
  &lt;div&gt;1 columna&lt;/div&gt;
&lt;/div&gt;

&lt;div class="grid-column-2 gap-xl"&gt;
  &lt;div&gt;2 columnas, con espaciado muy grande&lt;/div&gt;
  &lt;div&gt;2 columnas, con espaciado muy grande&lt;/div&gt;
&lt;/div&gt;

&lt;div class="grid-column-6 gap-m"&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
  &lt;div&gt;6 columnas, con espaciado mediano&lt;/div&gt;
&lt;/div&gt;


&lt;div class="grid-column-12"&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
  &lt;div&gt;12 columnas, sin espaciado&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */
/* Se realiza un for que recorre del 1 al 12. A cada una le aplica un grid-template-columns: repeat, asignándole el valor del for. Además, existen 4 distintos espaciados para dar entre cada elemento. */
/*------------------------------------------------------*/
/*---------------------- GRILLAS -----------------------*/
/*------------------------------------------------------*/
@for $i from 1 through 12{
  .gallery-columns-#{$i},
  .grid-column-#{$i}{
    display: grid;
    grid-template-columns: repeat($i, 1fr);
  }
}

.gap-s{
  grid-gap: $font * 10;
}
.gap-m{
  grid-gap: $font * 20;
}
.gap-l{
  grid-gap: $font * 35;
}
.gap-xl{
  grid-gap: $font * 50;
}

.grid-column-full{
  grid-column: 1 / -1;  
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #grids -->




	<section id="text-columns">
		<div class="tabs">
			<p class="secondary-title">Columnas de texto</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Existe la clase <strong>.text-columns-$i</strong>, donde <strong>$i</strong> se recorre <strong>4 veces</strong> (desde el 2 al 5), asignando una cantidad de columnas para separar el texto.</p>
				</div>

				<div class="block text-columns-2">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block text-columns-3">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block text-columns-4">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>

				<div class="block text-columns-5">
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="text-columns-2"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-3"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-4"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;

&lt;div class="text-columns-5"&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_general.scss */

/*------------------------------------------------------*/
/*----------------- COLUMNAS DE TEXTO ------------------*/
/*------------------------------------------------------*/
.text-columns-2{
  column-count: 2;
  -webkit-column-count: 2;
  -moz-column-count: 2;
  column-gap: 50px;
  -webkit-column-gap: 50px;
  -moz-column-gap: 50px;
}

/*...y así sucesivamente, hasta llegar al 5*/

.text-columns-5{
  column-count: 5;
  -webkit-column-count: 5;
  -moz-column-count: 5;
  column-gap: 50px;
  -webkit-column-gap: 50px;
  -moz-column-gap: 50px;
}
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Se realiza un for que recorre del 2 al 5. A cada una le aplica un column-count del for que recorre, obteniendo así 4 tamaños de columnas. Además, se obtiene el ancho de la columna multiplicando * 50 el valor de $font */

@for $i from 2 through 5{
  .text-columns-#{$i}{
    column-count: $i;
    -webkit-column-count: $i;
    -moz-column-count: $i;
    column-gap: $font * 50;
    -webkit-column-gap: $font * 50;
    -moz-column-gap: $font * 50;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #text-columns -->


	<section id="flex-same-width">
		<div class="tabs">
			<p class="secondary-title">Flex (Elementos del mismo ancho).</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center clearfix">
					<p>Para que un elemento tenga la propiedad <strong>display: flex;</strong> simplemente debe incorporarse la clase <strong>.flex</strong>.</p>
					<br>
					<p>Del mismo modo, para que todos los hijos tengan el mismo ancho, al elemento que los contiene también debe incorporársele la clase <strong>.same-width</strong>.</p>
				</div>

				<ul class="flex same-width block text-center">
					<li>Elemento 1</li>
					<li>Elemento 2</li>
					<li>Elemento 3</li>
					<li>Elemento 4</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex same-width"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_flex.scss */
.flex{
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -webkit-flex;
  display: flex;
  &.same-width > *{
    flex-grow: 1;
    flex-basis: 0;
    flex: 1 1 0;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-same-width -->
	
	
	
	
	<section id="flex-justify-content">
		<div class="tabs">
			<p class="secondary-title">Flex (Justificar contenido)</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block clearfix">
					<p>Para que un elemento tenga la propiedad <strong>display: flex;</strong> simplemente debe incorporarse la clase <strong>.flex</strong></p>
					<br>
					<p>En cuanto a la "justificación" de los hijos, debe asignársele alguna de estas clases para:</p>
					<ol>
						<li><strong>.justify-center (el del ejemplo):</strong> Distribuir los hijos al centro</li>
						<li><strong>.justify-between:</strong> Distribuirlos a lo largo del contenedor</li>
						<li><strong>.justify-evenly:</strong> Distribuirlos de manera uniforme y tengan el mismo espacio alrededor</li>
						<li><strong>.justify-start:</strong> Distribuirlos desde la izquierda</li>
						<li><strong>.justify-end:</strong> Distribuirlos desde la derecha</li>
						<li><strong>.justify-around:</strong> Distribuirlos de manera uniforme. Tienen un espacio de tamaño medio en cada extremo</li>
					</ol>
				</div>

				<ul class="flex justify-center block">
					<li>Elemento 1</li>
					<li>Elemento 2</li>
					<li>Elemento 3</li>
					<li>Elemento 4</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex justify-center"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->


			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin just($position){
  justify-content: $position;
}

/* Ubicación: components/merlin/scss/_flex.scss */
.justify-center{
    @include just(center);
}
.justify-between{
    @include just(space-between);
}
.justify-evenly{
    @include just(space-evenly);
}
.justify-start{
    @include just(flex-start);
}
.justify-end{
    @include just(flex-end);
}
.justify-around{
    @include just(space-around);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-justify-content -->



	<section id="flex-align">
		<div class="tabs">
			<p class="secondary-title">Flex (alinear)</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block clearfix">
					<p>Para que un div tenga la propiedad <strong>display: flex;</strong> simplemente debe incorporarse la clase <strong>.flex</strong></p>
					<br>
					<p>Para alinear los ítems, debe asignársele alguna de estas clases para:</p>
					<ol>
						<li><strong>.align-center (el del ejemplo):</strong> Alinear los hijos al centro</li>
						<li><strong>.align-between:</strong> Distribuirlos a lo largo del contenedor</li>
						<li><strong>.align-start:</strong> Distribuirlos desde el top del contenedor</li>
						<li><strong>.align-end:</strong> Distribuirlos desde el bottom del contenedor</li>
						<li><strong>.align-around:</strong> Distribuirlos con espacio antes, entre y después de las líneas</li>
					</ol>
				</div>

				<ul class="block flex align-center same-width">
					<li class="block">Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</li>
					<li class="block">Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</li>
					<li class="block">Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando.</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;ul class="flex align-center same-width"&gt;
  &lt;li&gt;Elemento 1&lt;/li&gt;
  &lt;li&gt;Elemento 2&lt;/li&gt;
  &lt;li&gt;Elemento 3&lt;/li&gt;
  &lt;li&gt;Elemento 4&lt;/li&gt;
&lt;/ul&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin align($position) {
  align-items: $position;
}

/* Ubicación: components/merlin/scss/_flex.scss */
.align-center{
    @include align(center);
}
.align-between{
    @include align(space-between);
}
.align-start{
    @include align(flex-start);
}
.align-end{
    @include align(flex-end);
}
.align-around{
    @include align(space-around);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-align -->


	<section id="modal">
		<div class="tabs">
			<p class="secondary-title">Modal</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block clearfix">
					Si el modal incluye un video que se quiere reproducir de manera automática, hay que incluir en el trigger, el tag <strong>data-video-url="id_video_yt"</strong>
					<br>
					Este módulo requiere de la llamada en el header del script externo <a href="https://www.youtube.com/iframe_api">iframe_api</a>, además del <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/modal.js">modal.js</a> incluído en Merlín.
				</div>

				<ul class="grid-column-3 gap-l">
					<li class="modal-trigger block text-center" data-id="modal-numero-uno">
						Modal con texto
					</li>
					<li class="modal-trigger block text-center" data-id="modal-numero-dos" data-video-url="_OkvnfzL93o">
						Modal con video
					</li>
					<li class="modal-trigger block text-center" data-id="modal-numero-dos" data-video-url="llqDrq00E28">
						Modal con video
					</li>
				</ul>


				<div data-id="modal-numero-uno" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<p class="title">Modal número uno</p>
						<p>
							Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>Texto del modal número uno<br>
						</p>
					</div><!-- content-modal -->
					<div class="modal-background"></div>
				</div><!-- modal -->

				<div data-id="modal-numero-dos" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<div class="iframeVideo relative">
							<div id="player"></div>
						</div>
					</div><!-- content -->
					<div class="modal-background"></div>
				</div><!-- modal -->

				<div data-id="modal-numero-tres" class="modal">
					<i class="close icon-equis"></i>
					<div class="content-modal contenido wp-content">
						<div class="iframeVideo relative">
							<div id="player"></div>
						</div>
					</div><!-- content -->
					<div class="modal-background"></div>
				</div><!-- modal -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;!-- Así se construyen los triggers. El primero es para un modal normal, el segundo, para uno con video de YouTube--&gt;
&lt;div class="modal-trigger" data-id="modal-numero-uno"&gt;
  Modal con texto
&lt;/div&gt;

&lt;div class="modal-trigger" data-id="modal-numero-dos" data-video-url="_OkvnfzL93o"&gt;
  Modal con video
&lt;/div&gt;

&lt;!-- Así se construyen los modals. El primero es para un modal normal, el segundo, para uno con video de YouTube--&gt;
&lt;div data-id="modal-numero-uno" class="modal"&gt;
  &lt;i class="close icon-equis"&gt;&lt;/i&gt;
  &lt;div class="content-modal contenido wp-content"&gt;
    &lt;p class="title"&gt;Modal número uno&lt;/p&gt;
    &lt;p&gt;
      Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;Texto del modal número uno&lt;br&gt;
    &lt;/p&gt;
  &lt;/div&gt;
  &lt;div class="modal-background"&gt;&lt;/div&gt;
&lt;/div&gt;

&lt;div data-id="modal-numero-dos" class="modal"&gt;
  &lt;i class="close icon-equis"&gt;&lt;/i&gt;
  &lt;div class="content-modal contenido wp-content"&gt;
    &lt;div class="iframeVideo relative"&gt;
      &lt;div id="player"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="modal-background"&gt;&lt;/div&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="scss">
/*
Existe un archivo .scss específico para el modal. Este se encuentra en components/custom/scss/_modal.scss
¿Por qué en custom? Porque se entiende que hay aspectos que deben ser modificados en relación al proyecto, por ejemplo, el color de fondo de la X (cerrar).
*/

.modal{
  overflow: hidden;
  z-index: 10;
  display: none;
  top: 0;
  left: 0;
  @extend .z-index-10;
  @extend .col-100;
  @extend .height-100;
  @extend .fixed;
  @include clearfix;
  .close {
    position: absolute;
    z-index: 10;
    background-color: $negro;
    color: $blanco;
    padding: 10px;
    right: calc(20% - 20px);
    top: calc(20% - 20px);
    font-size: 30px;
    cursor: pointer;
    @extend .transition-slow;
    @include border-radius(50px);
    &:hover{
      @include rotate(-90deg);
    }
  }
  .modal-background{
    @extend .col-100;
    @extend .height-100;
    @extend .fixed;
    background-color: rgba($negro, 0.7);
  }
  .content-modal{
    border: 15px solid $blanco;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    margin: auto;
    width: 60%;
    height: 60vh;
    background-color: $blanco;
    overflow-y: auto;
    @extend .z-index-1;
    @extend .absolute;
    @extend .border-radius-m;
    @include border-radius(5px);
    iframe{
      border: none;
      width: 100%;
      height: calc(60vh - 30px);
      @include border-radius(5px);
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #flex-align -->


	<section id="mixins">
		<div class="tabs">
			<p class="primary-title">@Mixins</p>
			<ul class="tabs-triggers">
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="sass" class="tab-content">
				<div class="message-block text-center">
					<p>Los <strong>@mixin</strong> son porciones de códigos que ayudan a no re-inventar la rueda cada vez y agrupan una serie de líneas de código para poder ser llamadas de una sola forma. Este archivo se encuentra en <strong>components/merlin/scss/_mixins.scss</strong>. A continuación está el detalle y la descripción de cada una:</p>
				</div>
				<pre>
					<code class="scss">
//Puede ser utilizado para un elemento que posea float entre sus estilos, tenga el alto que le corresponde 
@mixin clearfix{
  &:before,
  &:after{
    content: " ";
    display: table;
  }

  &:after{
    display: block;
    clear: both;
    height: 1px;
    margin-top: -1px;
    visibility: hidden;
  }
  &{
    *zoom: 1;
  }
}

//Mixin que puede ser utilizado cuando un elemento tiene display:flex;
@mixin align($position) {
  align-items: $position;
}

//Mixin que puede ser utilizado cuando un elemento tiene display:flex;
@mixin just($position){
  justify-content: $position;
}

//Mixin que puede ser utilizado cuando un elemento tiene display:flex;
@mixin direction($position) {
  flex-direction: $position;
}

//Mixin para añadir transiciones
@mixin transition($ms) {
  -webkit-transition: all $ms ease-in-out;
  -moz-transition: all $ms ease-in-out;
  -ms-transition: all $ms ease-in-out;
  -o-transition: all $ms ease-in-out;
  transition: all $ms ease-in-out;
}

//Mixin para generar descalce en el eje Y del elemento
@mixin translateY($rem) {
  -webkit-transform: translateY($rem);
  -moz-transform: translateY($rem);
  -ms-transform: translateY($rem);
  -o-transform: translateY($rem);
  transform: translateY($rem);
}

//Mixin para generar descalce en el eje X del elemento
@mixin translateX($rem) {
  -webkit-transform: translateX($rem);
  -moz-transform: translateX($rem);
  -ms-transform: translateX($rem);
  -o-transform: translateX($rem);
  transform: translateX($rem);
}

//Mixin para generar rotación del elemento
@mixin rotate($deg) {
  -webkit-transform: rotate($deg);
  -moz-transform: rotate($deg);
  -ms-transform: rotate($deg);
  -o-transform: rotate($deg);
  transform: rotate($deg);
}

//Mixin para un generar un delay en la transición del elemento
@mixin transition-delay($ms){
  -webkit-transition-delay: $ms;
  -moz-transition-delay: $ms;
  -ms-transition-delay: $ms;
  -o-transition-delay: $ms;
  transition-delay: $ms;
}

//Mixin para escalar un elemento
@mixin scale($scale){
  -webkit-transform: scale($scale);
  -moz-transform: scale($scale);
  -ms-transform: scale($scale);
  -o-transform: scale($scale);
  transform: scale($scale);
}

//Mixin para hacer borroso un elemento
@mixin blur($rem){
  -webkit-filter: blur($rem);
  -moz-filter: blur($rem);
  -ms-filter: blur($rem);
  -o-filter: blur($rem);
  filter: blur($rem);
}

//Mixin para la opacidad de un elemento
@mixin opacity($opacity) {
  opacity: $opacity;
  $opacity-ie: $opacity * 100;
  filter: alpha(opacity=$opacity-ie);
}

/*
Mixin para llamar a un ícono. El llamado sería, por ejemplo:
li{
  @include icomoon("icon-flecha-derecha-mediana");
}
*/
@mixin icomoon($icon, $position: "before", $replace: false) {
  @if $replace {
    font-size: 0;
  }
  &:#{$position} {
    @extend .#{$icon};
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    @if $replace {
      font-size: $font * 16;
    }
    @content;
  }
}

//Mixin para generar gradiente en un elemento
@mixin linear-gradient($color-one,$color-two,$degrees) {
  background: -moz-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -webkit-gradient(linear, left top, right top, color-stop(0%, $color-one), color-stop(100%, $color-two));
  background: -webkit-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -o-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -ms-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: linear-gradient($degrees, $color-one 0%, $color-two 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$color-one', endColorstr='$color-two',GradientType=1 );
}

//Mixin para generar bordes redondeados de un elemento
@mixin border-radius($top-left:null, $top-right:null, $bottom-right:null, $bottom-left:null){
  -webkit-border-radius: $top-left $top-right $bottom-right $bottom-left;
  -moz-border-radius: $top-left $top-right $bottom-right $bottom-left;
  -ms-border-radius: $top-left $top-right $bottom-right $bottom-left;
  border-radius: $top-left $top-right $bottom-right $bottom-left;  
}
					</code>
				</pre>
			</div>
		</div>
	</section>
	
	<section id="pesos">
		<div class="tabs">
			<p class="primary-title">Tipografía</p>
			<p class="secondary-title">Pesos</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="block thin">Thin</p>
				<p class="block extra-light">Extra light</p>
				<p class="block light">Light</p>
				<p class="block regular">Regular</p>
				<p class="block medium">Medium</p>
				<p class="block semi-bold">Semi bold</p>
				<p class="block bold">Bold</p>
				<p class="block extra-bold">Extra bold</p>
				<p class="block black">Black</p>
				<p class="block italic">Italic</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="thin"&gt;Thin&lt;/p&gt;
&lt;p class="extra-light"&gt;Extra light&lt;/p&gt;
&lt;p class="light"&gt;Light&lt;/p&gt;
&lt;p class="regular"&gt;Regular&lt;/p&gt;
&lt;p class="medium"&gt;Medium&lt;/p&gt;
&lt;p class="semi-bold"&gt;Semi bold&lt;/p&gt;
&lt;p class="bold"&gt;Bold&lt;/p&gt;
&lt;p class="extra-bold"&gt;Extra bold&lt;/p&gt;
&lt;p class="black"&gt;Black&lt;/p&gt;
&lt;p class="italic"&gt;Italic&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */

/*------------------------------------------------------*/
/*------------------------ PESOS -----------------------*/
/*------------------------------------------------------*/
.thin{
  font-weight: 100;
}

.extra-light,
.extralight{
  font-weight: 200;
}

.light{
  font-weight: 300;
}

.regular{
  font-weight: 400;
}

.medium{
  font-weight: 500;
}

.semi-bold,
.semibold{
  font-weight: 600;
}

.bold{
  font-weight: 700;
}

.extra-bold,
.extrabold{
  font-weight: 800;
}


.black{
  font-weight: 900;
}

.italic{
  font-style: italic;
}

/* Para llamar vía SASS las clases con los pesos, simplemente se debe colocar la clase del peso deseado. Por ejemplo: */
p{
  @extend .thin;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #pesos -->


	<section id="alineacion">
		<div class="tabs">
			<p class="secondary-title">Alineación</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="block uppercase">Texto en mayúsculas</p>
				<p class="block text-center">Texto centrado</p>
				<p class="block text-left">Texto alineado a la izquierda</p>
				<p class="block text-right">Texto alineado a la derecha</p>
				<p class="block text-justify">Ejemplo de un texto justificado: Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto. Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo. Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. - ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual. Sobre la mesa había desparramado un muestrario de paños</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="uppercase"&gt;Texto en mayúsculas&lt;/p&gt;
&lt;p class="text-center"&gt;Texto centrado&lt;/p&gt;
&lt;p class="text-left"&gt;Texto alineado a la izquierda&lt;/p&gt;
&lt;p class="text-right"&gt;Texto alineado a la derecha&lt;/p&gt;
&lt;p class="text-justify"&gt;Texto justificado&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */

/*------------------------------------------------------*/
/*------------------ ALINEACIÓN TEXTO ------------------*/
/*------------------------------------------------------*/
.uppercase{
  text-transform: uppercase;
}

.text-center{
  text-align: center;
}

.text-left{
  text-align: left;
}

.text-right{
  text-align: right
}

.text-justify{
  text-align: justify;
}

/* Para llamar vía SASS las clases con la alineación de texto deseada, simplemente se debe colocar la clase. Por ejemplo: */
p{
  @extend .uppercase;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #alineacion -->


	<section id="tamanos">
		<div class="tabs">
			<p class="secondary-title">Tamaños</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-left">
					<p>Existen 90 tamaños tipográficos de clase <strong>.font-$i</strong>, donde:</p>
					<ul>
						<li>Se crea la variable <strong>$font = 1px</strong>.</li>
						<li>Se genera un loop <strong>($i)</strong> el cual se recorre 90 veces, del <strong>10 al 100</strong>.</li>
						<li>En este loop <strong>.font-#{$i}</strong> se ejecuta cada vez <strong>font-size: $font*$i;</strong> asignando un tamaño único cada vez.</li>
						<li>El loop comienza desde el <strong>10 (.font-10)</strong> y termina en <strong>100 (.font-100)</strong>. Por ejemplo, entonces: <strong>.font-27 = font-size:27px</strong>.</li>
						<li>Si la tipografía es mayor a 21, el line-height cambia de 140% a 120% (este porcentaje depende de la familia tipográfica).</li>
					</ul>
					
					<p>En el caso del responsive <strong>(components/custom/scss/_responsive_tablet.scss)</strong>, se empezarán a generar agrupaciones de tipografías, donde:</p>
					<ul>
						<li>.font-10 / .font-13 = font-size: 13px</li>
						<li>.font-14 / .font-18 = font-size: 16px</li>
						<li>.font-19 / .font-25 = font-size: 18px</li>
						<li>.font-26 / .font-30 = font-size: 21px</li>
						<li>.font-31 / .font-36 = font-size: 24px</li>
						<li>.font-37 / .font-42 = font-size: 28px</li>
						<li>.font-43 / .font-52 = font-size: 32px</li>
						<li>.font-53 / .font-100 = font-size: 36px</li>
					</ul>
				</div>

				<p class="block font-10">Tipografía</p>
				<p class="block font-11">Tipografía</p>
				<p class="block font-12">Tipografía</p>
				<p class="block font-13">Tipografía</p>
				<p class="block font-14">Tipografía</p>
				<p class="block font-15">Tipografía</p>
				<p class="block font-16">Tipografía</p>
				<p class="block font-17">Tipografía</p>
				<p class="block font-18">Tipografía</p>
				<p class="block font-19">Tipografía</p>
				<p class="block font-20">Tipografía</p>

				<p class="block font-21">Tipografía</p>
				<p class="block font-22">Tipografía</p>
				<p class="block font-23">Tipografía</p>
				<p class="block font-24">Tipografía</p>
				<p class="block font-25">Tipografía</p>
				<p class="block font-26">Tipografía</p>
				<p class="block font-27">Tipografía</p>
				<p class="block font-28">Tipografía</p>
				<p class="block font-29">Tipografía</p>
				<p class="block font-30">Tipografía</p>

				<p class="block font-31">Tipografía</p>
				<p class="block font-32">Tipografía</p>
				<p class="block font-33">Tipografía</p>
				<p class="block font-34">Tipografía</p>
				<p class="block font-35">Tipografía</p>
				<p class="block font-36">Tipografía</p>
				<p class="block font-37">Tipografía</p>
				<p class="block font-38">Tipografía</p>
				<p class="block font-39">Tipografía</p>
				<p class="block font-40">Tipografía</p>

				<p class="block font-41">Tipografía</p>
				<p class="block font-42">Tipografía</p>
				<p class="block font-43">Tipografía</p>
				<p class="block font-44">Tipografía</p>
				<p class="block font-45">Tipografía</p>
				<p class="block font-46">Tipografía</p>
				<p class="block font-47">Tipografía</p>
				<p class="block font-48">Tipografía</p>
				<p class="block font-49">Tipografía</p>
				<p class="block font-50">Tipografía</p>

				<p class="block font-51">Tipografía</p>
				<p class="block font-52">Tipografía</p>
				<p class="block font-53">Tipografía</p>
				<p class="block font-54">Tipografía</p>
				<p class="block font-55">Tipografía</p>
				<p class="block font-56">Tipografía</p>
				<p class="block font-57">Tipografía</p>
				<p class="block font-58">Tipografía</p>
				<p class="block font-59">Tipografía</p>
				<p class="block font-60">Tipografía</p>

				<p class="block font-61">Tipografía</p>
				<p class="block font-62">Tipografía</p>
				<p class="block font-63">Tipografía</p>
				<p class="block font-64">Tipografía</p>
				<p class="block font-65">Tipografía</p>
				<p class="block font-66">Tipografía</p>
				<p class="block font-67">Tipografía</p>
				<p class="block font-68">Tipografía</p>
				<p class="block font-69">Tipografía</p>
				<p class="block font-70">Tipografía</p>

				<p class="block font-71">Tipografía</p>
				<p class="block font-72">Tipografía</p>
				<p class="block font-73">Tipografía</p>
				<p class="block font-74">Tipografía</p>
				<p class="block font-75">Tipografía</p>
				<p class="block font-76">Tipografía</p>
				<p class="block font-77">Tipografía</p>
				<p class="block font-78">Tipografía</p>
				<p class="block font-79">Tipografía</p>
				<p class="block font-80">Tipografía</p>

				<p class="block font-81">Tipografía</p>
				<p class="block font-82">Tipografía</p>
				<p class="block font-83">Tipografía</p>
				<p class="block font-84">Tipografía</p>
				<p class="block font-85">Tipografía</p>
				<p class="block font-86">Tipografía</p>
				<p class="block font-87">Tipografía</p>
				<p class="block font-88">Tipografía</p>
				<p class="block font-89">Tipografía</p>
				<p class="block font-90">Tipografía</p>

				<p class="block font-91">Tipografía</p>
				<p class="block font-92">Tipografía</p>
				<p class="block font-93">Tipografía</p>
				<p class="block font-94">Tipografía</p>
				<p class="block font-95">Tipografía</p>
				<p class="block font-96">Tipografía</p>
				<p class="block font-97">Tipografía</p>
				<p class="block font-98">Tipografía</p>
				<p class="block font-99">Tipografía</p>
				<p class="block font-100">Tipografía</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="font-10"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-11"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-12"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-13"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-14"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-15"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-16"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-17"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-18"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-19"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-20"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-21"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-22"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-23"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-24"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-25"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-26"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-27"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-28"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-29"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-30"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-31"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-32"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-33"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-34"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-35"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-36"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-37"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-38"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-39"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-40"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-41"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-42"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-43"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-44"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-45"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-46"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-47"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-48"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-49"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-50"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-51"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-52"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-53"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-54"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-55"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-56"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-57"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-58"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-59"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-60"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-61"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-62"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-63"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-64"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-65"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-66"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-67"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-68"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-69"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-70"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-71"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-72"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-73"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-74"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-75"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-76"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-77"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-78"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-79"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-80"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-81"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-82"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-83"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-84"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-85"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-86"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-87"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-88"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-89"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-90"&gt;Tipografía&lt;/p&gt;

&lt;p class="font-91"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-92"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-93"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-94"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-95"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-96"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-97"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-98"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-99"&gt;Tipografía&lt;/p&gt;
&lt;p class="font-100"&gt;Tipografía&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_fonts.scss */
				
/*
Se realiza un for que recorre del 10 al 100. A cada una le aplica un font-size del for que recorre, obteniendo así 90 tamaños tipográficos.

Existe la excepción de cuando la fuente es mayor a 21, el line-height cambia de 140% a 120%
*/

$font: 1px;
@for $i from 10 through 100{
    .font-#{$i}{
        font-size: $font*$i;
        @if $i < 21{
            line-height: 140%;
        }@else{
            line-height: 120%;
        }
    }
}

/* Para usarlo, por ejemplo: */
p{
  @extend .font-32;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #tamanos -->



	<section id="titles">
		<div class="tabs">
			<p class="primary-title">Editor WYSIWYG</p>
				<div class="message-block text-center">
					<p>Para que los estilos desplegados a continuación sean aplicados se deben contener en la clase <strong>.wysiwyg</strong>.</p>
				</div>
				
			<p class="secondary-title">Títulos</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="wysiwyg">
					<h1>Título número h1</h1>
					<h2>Título número h2</h2>
					<h3>Título número h3</h3>
					<h4>Título número h4</h4>
					<h5>Título número h5</h5>
					<h6>Título número h6</h6>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;h1&gt;Título número h1&lt;/h1&gt;
  &lt;h2&gt;Título número h2&lt;/h2&gt;
  &lt;h3&gt;Título número h3&lt;/h3&gt;
  &lt;h4&gt;Título número h4&lt;/h4&gt;
  &lt;h5&gt;Título número h5&lt;/h5&gt;
  &lt;h6&gt;Título número h6&lt;/h6&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
.wysiwyg{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  /*------------------------------------------------------*/
  /*----------------------- TÍTULOS ----------------------*/
  /*------------------------------------------------------*/
  @for $i from 1 through 6{
    h#{$i}{
      @extend .bold;
      @if $i == 1{
        font-size: $font * 42;
      }
      @elseif $i < 3{
        font-size: $font * 42 / ($i/1.7);
      }@else{
        font-size: ($font * 42) / ($i/2.25);
      }
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #titles -->




	<section id="paragraph">
		<div class="tabs">
			<p class="secondary-title">Párrafo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="wysiwyg">
					<p>Este es un párrafo simulado. Acá simularemos cómo se vería un <a href="http://google.cl">link</a>, un texto en <strong>negrita</strong> y también en <em>itálica</em>. <del>Texto tachado</del>.</p>
					<p>Estaba echado de espaldas sobre este es un link un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
					<p style="text-align: right;">Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>
					<p style="text-align: center;">Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>
					<div style="width: 457px" class="wp-caption alignleft">
						<img src="https://i.etsystatic.com/13030919/r/il/792e79/1146518143/il_1588xN.1146518143_f6ui.jpg" alt="Texto alternativo #1" width="447" height="486">
						<p class="wp-caption-text">Esta es la leyenda de la fotografía</p>
					</div>
					<div id="idTextPanel" class="jqDnR">
						<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
						<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
						<p>Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>
						<p>Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>
						<p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p>
						<p>«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas. Intentó en vano esta operación numerosas veces; cerró los ojos para no tener que ver aquella confusa agitación de patas, que no cesó hasta que notó en el costado un dolor leve y punzante, un dolor jamás sentido hasta entonces. – ¡Qué cansada es la profesión que he elegido! -se dijo-. Siempre de viaje. Las preocupaciones son mucho mayores cuando se trabaja</p>
					</div>
					<div id="idTextPanel" class="jqDnR">
					<div style="width: 375px" class="wp-caption alignright">
						<img src="https://www.ancient-origins.net/sites/default/files/Merlin-the-wizard.jpg" alt="Texto alternativo #2" width="365" height="462">
						<p class="wp-caption-text">Esta es la leyenda de la fotografía</p>
					</div>
					<p>Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.</p>
					<p>Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.</p>
					<p>Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.</p>
					<p>Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.</p>
					<p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p>
					<p>«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas.</p>				
					<blockquote>
						<p>La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.</p>
					</blockquote>				
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<div class="message-block text-center">
					<p>Existen estilos inline y clases e id's puestos en el código dentro del <strong>section class="wysiwyg"</strong>. Esto se debe a que Wordpress los añade automáticamente.</p>
				</div>

				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;p&gt;Este es un párrafo simulado. Acá simularemos cómo se vería un &lt;a href="http://google.cl"&gt;link&lt;/a&gt;, un texto en &lt;strong&gt;negrita&lt;/strong&gt; y también en &lt;em&gt;itálica&lt;/em&gt;. &lt;del&gt;Texto tachado&lt;/del&gt;.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre este es un link un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
  &lt;p style="text-align: right;"&gt;Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.&lt;/p&gt;
  &lt;p style="text-align: center;"&gt;Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.&lt;/p&gt;
  &lt;div style="width: 457px" class="wp-caption alignleft"&gt;
    &lt;img src="https://i.etsystatic.com/13030919/r/il/792e79/1146518143/il_1588xN.1146518143_f6ui.jpg" alt="Texto alternativo #1" width="447" height="486"&gt;
    &lt;p class="wp-caption-text"&gt;Esta es la leyenda de la fotografía&lt;/p&gt;
  &lt;/div&gt;
  &lt;div id="idTextPanel" class="jqDnR"&gt;
    &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
    &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
    &lt;p&gt;Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.&lt;/p&gt;
    &lt;p&gt;Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.&lt;/p&gt;
    &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
    &lt;p&gt;«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas. Intentó en vano esta operación numerosas veces; cerró los ojos para no tener que ver aquella confusa agitación de patas, que no cesó hasta que notó en el costado un dolor leve y punzante, un dolor jamás sentido hasta entonces. – ¡Qué cansada es la profesión que he elegido! -se dijo-. Siempre de viaje. Las preocupaciones son mucho mayores cuando se trabaja&lt;/p&gt;
  &lt;/div&gt;
  &lt;div id="idTextPanel" class="jqDnR"&gt;
  &lt;div style="width: 375px" class="wp-caption alignright"&gt;
    &lt;img src="https://www.ancient-origins.net/sites/default/files/Merlin-the-wizard.jpg" alt="Texto alternativo #2" width="365" height="462"&gt;
    &lt;p class="wp-caption-text"&gt;Esta es la leyenda de la fotografía&lt;/p&gt;
  &lt;/div&gt;
  &lt;p&gt;Una mañana, tras un sueño intranquilo, Gregorio Samsa se despertó convertido en un monstruoso insecto.&lt;/p&gt;
  &lt;p&gt;Estaba echado de espaldas sobre un duro caparazón y, al alzar la cabeza, vio su vientre convexo y oscuro, surcado por curvadas callosidades, sobre el que casi no se aguantaba la colcha, que estaba a punto de escurrirse hasta el suelo.&lt;/p&gt;
  &lt;p&gt;Numerosas patas, penosamente delgadas en comparación con el grosor normal de sus piernas, se agitaban sin concierto. – ¿Qué me ha ocurrido? No estaba soñando. Su habitación, una habitación normal, aunque muy pequeña, tenía el aspecto habitual.&lt;/p&gt;
  &lt;p&gt;Sobre la mesa había desparramado un muestrario de paños – Samsa era viajante de comercio-, y de la pared colgaba una estampa recientemente recortada de una revista ilustrada y puesta en un marco dorado.&lt;/p&gt;
  &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
  &lt;p&gt;«Bueno -pensó-; ¿y si siguiese durmiendo un rato y me olvidase de todas estas locuras? » Pero no era posible, pues Gregorio tenía la costumbre de dormir sobre el lado derecho, y su actual estado no le permitía adoptar tal postura. Por más que se esforzara volvía a quedar de espaldas.&lt;/p&gt;        
  &lt;blockquote&gt;
    &lt;p&gt;La estampa mostraba a una mujer tocada con un gorro de pieles, envuelta en una estola también de pieles, y que, muy erguida, esgrimía un amplio manguito, asimismo de piel, que ocultaba todo su antebrazo. Gregorio miró hacia la ventana; estaba nublado, y sobre el cinc del alféizar repiqueteaban las gotas de lluvia, lo que le hizo sentir una gran melancolía.&lt;/p&gt;
  &lt;/blockquote&gt;        
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
.wysiwyg{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  /*------------------------------------------------------*/
  /*---------------------- CONTENIDO ---------------------*/
  /*------------------------------------------------------*/
  p,ol,ul{
    @extend .font-16;
    margin: 18px 0;
  }
  a{
    padding: 0 5px;
    box-shadow: inset 0px -3px 0 0 $negro;
    @extend .bold;
    &:hover{
      color: $blanco;
      box-shadow: inset 0px -20px 0 0 $negro;
    }
  }
  /*------------------------------------------------------*/
  /*------------------------- CITA -----------------------*/
  /*------------------------------------------------------*/
  blockquote{
    padding-left: 2.5%;
    margin-top: 20px;
    margin-bottom: 20px;
    @extend .italic;
    @extend .wrap-xl;
    @extend .relative;
    &:before{
      position: absolute;
      height: calc(100% - 46px);
      top: 26px;
      left: 0;
      width: 4px;
      background-color: $negro;
    }
    p,ol,ul{
      @extend .font-32;
    }
  }
  /*------------------------------------------------------*/
  /*------------------------ MEDIA -----------------------*/
  /*------------------------------------------------------*/
  p.wp-caption-text,
  .gallery-caption{
    margin: 10px 0;
    color: $azul-oscuro;
    @extend .italic;
    @extend .font-11;
  }
  .wp-caption{
    &.aligncenter{
      margin: 0 auto;
    }    
    &.alignnone{
      width: 100% !important;
      img{
        width: 100%;
        height: auto;
      }
    }
    &.alignleft,
    &.alignright{
      width: 35% !important;
      img{
        width: 100%;
        height: auto;
      }
    }
    &.alignleft{
      margin: 0 20px 20px 0;
      @extend .left;
    }
    &.alignright{
      margin: 0 0 20px 20px;
      @extend .right;
    }
  }
  img{
    height: auto;
    &.alignleft,
    &.alignright{
      width: 35%;
    }
    &.alignleft{
      float: left;
      margin: 0 18px 18px 0;
    }
    &.alignright{
      float: right;
      margin: 0 0 18px 18px;
    }
    &.aligncenter{
      margin: 0 auto;
      display: block;
    }
    &.alignnone{
      @extend .wrap-xl;
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #paragraph -->





	<section id="media">
		<div class="tabs">
			<p class="secondary-title">Media</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>En el caso de las galerías fotográficas, se ocupa la librería <a href="https://sachinchoolur.github.io/lightGallery/" target="_blank">lightgallery.js</a> (components/merlin/js/lightgallery).</p>
				</div>

				<section class="wysiwyg">
					<div style="width: 219px" class="wp-caption aligncenter">
						<img src="https://i.pinimg.com/originals/73/9f/74/739f74e0950ef5f9244406891e0280c0.jpg" alt="Texto alternativo #3" width="209" height="300"><p class="wp-caption-text">Esta es una foto pequeña</p>
					</div>
					<p>&nbsp;</p>
					<div id="attachment_29" style="width: 1317px" class="wp-caption alignnone">
						<img aria-describedby="caption-attachment-29" class="size-full wp-image-29" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" alt="" srcset="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg 1307w, https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-697x800.jpg 697w, https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-768x881.jpg 768w" sizes="(max-width: 1307px) 100vw, 1307px" width="1307" height="1500">
						<p id="caption-attachment-29" class="wp-caption-text">Esta es una foto a todo el ancho</p>
					</div>
					<p>&nbsp;</p>
					<p>
						<iframe title="Merlín - El Mago (Documental)" src="https://www.youtube.com/embed/wZLURWX48zk?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="500" height="281" frameborder="0"></iframe>
					</p>
					<div id="gallery-1" class="gallery galleryid-3 gallery-columns-3 gallery-size-thumbnail">
						<dl class="gallery-item">
							<dt class="gallery-icon portrait">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-29" width="400" height="400">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-29">
								Esta es una foto a todo el ancho
							</dd>
						</dl>
						<dl class="gallery-item">
							<dt class="gallery-icon landscape">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6-400x360.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-34" width="400" height="360">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-34">
								Título de la imagen número 2
							</dd>
						</dl>
						<dl class="gallery-item">
							<dt class="gallery-icon portrait">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-33" width="400" height="400">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-33">
								Título de la imagen número 3
							</dd>
						</dl>
						<br style="clear: both">
						<dl class="gallery-item">
							<dt class="gallery-icon portrait">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-32" width="400" height="400">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-32">
								Título de la imagen número 4
							</dd>
						</dl>
						<dl class="gallery-item">
							<dt class="gallery-icon landscape">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-31" width="400" height="400">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-31">
								Título de la imagen número 5
							</dd>
						</dl>
						<dl class="gallery-item">
							<dt class="gallery-icon portrait">
								<a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="gallery-link">
									<img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-30" width="220" height="272">
								</a>
							</dt>
							<dd class="wp-caption-text gallery-caption" id="gallery-1-30">
								The romance of King Arthur and his knights of the Round Table. Abridged from Malory’s Morte d’Arthur by Alfred W. Pollard. Illustrated by Arthur Rackham. Published 1920 by Macmillan in New York.
							</dd>
						</dl>
						<br style="clear: both">
					</div>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
&lt;section class="wysiwyg"&gt;
  &lt;div style="width: 219px" class="wp-caption aligncenter"&gt;
    &lt;img src="https://i.pinimg.com/originals/73/9f/74/739f74e0950ef5f9244406891e0280c0.jpg" alt="Texto alternativo #3" width="209" height="300"&gt;&lt;p class="wp-caption-text"&gt;Esta es una foto pequeña&lt;/p&gt;
  &lt;/div&gt;
  &lt;p&gt;&nbsp;&lt;/p&gt;
  &lt;div id="attachment_29" style="width: 1317px" class="wp-caption alignnone"&gt;
    &lt;img aria-describedby="caption-attachment-29" class="size-full wp-image-29" src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" alt="" srcset="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg 1307w, https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-697x800.jpg 697w, https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-768x881.jpg 768w" sizes="(max-width: 1307px) 100vw, 1307px" width="1307" height="1500"&gt;
    &lt;p id="caption-attachment-29" class="wp-caption-text"&gt;Esta es una foto a todo el ancho&lt;/p&gt;
  &lt;/div&gt;
  &lt;p&gt;&nbsp;&lt;/p&gt;
  &lt;p&gt;
    &lt;iframe title="Merlín - El Mago (Documental)" src="https://www.youtube.com/embed/wZLURWX48zk?feature=oembed" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" width="500" height="281" frameborder="0"&gt;&lt;/iframe&gt;
  &lt;/p&gt;
  &lt;div id="gallery-1" class="gallery galleryid-3 gallery-columns-3 gallery-size-thumbnail"&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-1-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-29" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-29"&gt;
        Esta es una foto a todo el ancho
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon landscape"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-6-400x360.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-34" width="400" height="360"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-34"&gt;
        Título de la imagen número 2
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-5-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-33" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-33"&gt;
        Título de la imagen número 3
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;br style="clear: both"&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-4-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-32" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-32"&gt;
        Título de la imagen número 4
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon landscape"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-3-400x400.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-31" width="400" height="400"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-31"&gt;
        Título de la imagen número 5
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;dl class="gallery-item"&gt;
      &lt;dt class="gallery-icon portrait"&gt;
        &lt;a href="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="gallery-link"&gt;
          &lt;img src="https://want.cl/merlin/wp-content/uploads/2019/11/merlin-2.jpg" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-30" width="220" height="272"&gt;
        &lt;/a&gt;
      &lt;/dt&gt;
      &lt;dd class="wp-caption-text gallery-caption" id="gallery-1-30"&gt;
        The romance of King Arthur and his knights of the Round Table. Abridged from Malory’s Morte d’Arthur by Alfred W. Pollard. Illustrated by Arthur Rackham. Published 1920 by Macmillan in New York.
      &lt;/dd&gt;
    &lt;/dl&gt;
    &lt;br style="clear: both"&gt;
  &lt;/div&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
.wysiwyg{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  /*------------------------------------------------------*/
  /*------------------------ MEDIA -----------------------*/
  /*------------------------------------------------------*/
  img{
    height: auto;
    &.alignleft,
    &.alignright{
      width: 35%;
    }
    &.alignleft{
      float: left;
      margin: 0 18px 18px 0;
    }
    &.alignright{
      float: right;
      margin: 0 0 18px 18px;
    }
    &.aligncenter{
      margin: 0 auto;
      display: block;
    }
    &.alignnone{
      @extend .wrap-xl;
    }
  }
  iframe{
    height: 65vh;
    width: 100%;
  }
  .gallery{
    grid-gap: $font * 10;
    .gallery-item{
      width: 100% !important;
      height: 100%;
      margin: 0 !important;
      overflow: hidden;
      @extend .relative;
      .gallery-icon{
        @extend .left;
        width: 100%;
        height: 25vmin;
        img{
          object-fit: cover;
          width: 100%;
          height: 100%;
          @extend .transition-slow;
        }
        a{
          padding: 0;
          box-shadow: none;
          width: 100%;
          height: 100%;
          overflow: hidden;
          @extend .left;
          &:hover{
            img{
              @include scale(1.15);
            }
          }
        }
      }
      .gallery-caption{
        width: 100%;
        @include clearfix;
      }
    }
    br{
      display: none;
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #media -->




	<section id="lists">
		<div class="tabs">
			<p class="secondary-title">Listas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<section class="wysiwyg">
					<ul>
						<li>Elemento de la lista #1</li>
						<li>Elemento de la lista #2</li>
						<li>Elemento de la lista #3</li>
						<li>Elemento de la lista #4</li>
					</ul>

					<ol>
						<li>Elemento de la lista #1</li>
						<li>Elemento de la lista #2</li>
						<li>Elemento de la lista #3</li>
						<li>Elemento de la lista #4</li>
					</ol>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;section class="wysiwyg"&gt;
  &lt;ul&gt;
    &lt;li&gt;Elemento de la lista #1&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #2&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #3&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #4&lt;/li&gt;
  &lt;/ul&gt;

  &lt;ol&gt;
    &lt;li&gt;Elemento de la lista #1&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #2&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #3&lt;/li&gt;
    &lt;li&gt;Elemento de la lista #4&lt;/li&gt;
  &lt;/ol&gt;
&lt;/section&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
.wysiwyg{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  /*------------------------------------------------------*/
  /*---------------------- CONTENIDO ---------------------*/
  /*------------------------------------------------------*/
  p,ol,ul{
    @extend .font-16;
    margin: 18px 0;
  }
  /*------------------------------------------------------*/
  /*------------------------ LISTAS ----------------------*/
  /*------------------------------------------------------*/
  ul,ol{
    padding-left: 7%;
    li{
      margin: 4px 0;

    }
  }
  ul{
    li{
      @extend .relative;
      @include icomoon("icon-chevron-derecha");
      &:before{
        font-size: 9px;
        line-height: 10px;
        text-align: center;
        color: $blanco;
        background-color: $negro;
        width: 10px;
        height: 10px;
        left: -18px;
        top: 6.5px;
        @include border-radius(10px);
        @extend .absolute;
      }
    }
  }
  ol{
    list-style: decimal;
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #lists -->


	<section id="tables">
		<div class="tabs">
			<p class="secondary-title">Tablas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#jquery">jQuery</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<p>Se genera automáticamente un div que contiene la tabla (de clase .table-container), de manera que en dispositivos móviles la tabla no haga que el body quede más ancho de lo que debiese <strong>(components/merlin/js/scripts.js)</strong>.</p>
				</div>
				<section class="wysiwyg">
					<table>
						<tbody>
							<tr>
								<td>Cabecera 1</td>
								<td>Cabecera 2</td>
								<td>Cabecera 3</td>
								<td>Cabecera 4</td>
								<td>Cabecera 5</td>
							</tr>
							<tr>
								<td>Contenido 1</td>
								<td>Contenido 2</td>
								<td>Contenido 3</td>
								<td>Contenido 4</td>
								<td>Contenido 5</td>
							</tr>
							<tr>
								<td>Contenido 1</td>
								<td>Contenido 2</td>
								<td>Contenido 3</td>
								<td>Contenido 4</td>
								<td>Contenido 5</td>
							</tr>
							<tr>
								<td>Contenido 1</td>
								<td>Contenido 2</td>
								<td>Contenido 3</td>
								<td>Contenido 4</td>
								<td>Contenido 5</td>
							</tr>
						</tbody>
					</table>
				</section>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;table&gt;
  &lt;tbody&gt;
    &lt;tr&gt;
      &lt;td&gt;Cabecera 1&lt;/td&gt;
      &lt;td&gt;Cabecera 2&lt;/td&gt;
      &lt;td&gt;Cabecera 3&lt;/td&gt;
      &lt;td&gt;Cabecera 4&lt;/td&gt;
      &lt;td&gt;Cabecera 5&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td&gt;Contenido 1&lt;/td&gt;
      &lt;td&gt;Contenido 2&lt;/td&gt;
      &lt;td&gt;Contenido 3&lt;/td&gt;
      &lt;td&gt;Contenido 4&lt;/td&gt;
      &lt;td&gt;Contenido 5&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td&gt;Contenido 1&lt;/td&gt;
      &lt;td&gt;Contenido 2&lt;/td&gt;
      &lt;td&gt;Contenido 3&lt;/td&gt;
      &lt;td&gt;Contenido 4&lt;/td&gt;
      &lt;td&gt;Contenido 5&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td&gt;Contenido 1&lt;/td&gt;
      &lt;td&gt;Contenido 2&lt;/td&gt;
      &lt;td&gt;Contenido 3&lt;/td&gt;
      &lt;td&gt;Contenido 4&lt;/td&gt;
      &lt;td&gt;Contenido 5&lt;/td&gt;
    &lt;/tr&gt;
  &lt;/tbody&gt;
&lt;/table&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="jquery" class="tab-content">
				<pre>
					<code class="js	">
//Ubicación: components/merlin/js/scripts.js
$( "table" ).wrap( "&lt;div class='table-container'&gt;&lt;/div&gt;" );
					</code>
				</pre>
			</div><!-- tab-content -->
			
			
			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_wysiwyg.scss */
.wysiwyg{
  @extend .col-100;
  @extend .left;
  @include clearfix;
  /*------------------------------------------------------*/
  /*------------------------ TABLA -----------------------*/
  /*------------------------------------------------------*/
  table{
    border: none;
    width: 100%;
    margin: 10px 0;
    tr{
      td{
        padding: 15px;
        border: none;
        @extend .font-15;
        @extend .regular;
      }
    }
    tr:first-child{
      background-color: $negro !important;
      td{
        color: $blanco;
        @extend .bold;
      }
    }
    tr:nth-child(even){
      background-color: $blanco;
    }
    tr:nth-child(odd){
      background-color: rgba($negro, 0.04);
    }
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #tables -->
	
	
	<section id="transitions">
		<div class="tabs">
			<p class="primary-title">Animación</p>
			<p class="secondary-title">Transiciones</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<ul>
						<li>Existe un <strong>@mixin</strong> (compontents/merlin/scss/_mixins.scss) llamado <strong>transition</strong> donde se definen las propiedades del mismo nombre.</li>
						<li>Existen 4 clases ya pre-hechas, <strong>creados en compontents/merlin/scss/_animations.scss)</strong>, donde:</li>
						<ul>
							<li>Transición de 0 segundos: .no-transition</li>
							<li>Transición de 0.25 segundos: .transition</li>
							<li>Transición de 0.5 segundos: .transition-slow</li>
							<li>Transición de 0.75 segundos: .transition-slower</li>
						</ul>
						<li>Además puedes manejar velocidades propias llamando al @mixin y modificando manualmente la variable <strong>$ms</strong> (creada en compontents/merlin/scss/_animations.scss), por ejemplo: <strong>@include transition(10s);</strong></li>
					</ul>
				</div>
				<p class="block no-transition">Sin transición (simular al hover)</p>
				<p class="block transition">Transición rápida, 0.25s (simular al hover)</p>
				<p class="block transition-slow">Transición media, 0.5s (simular al hover)</p>
				<p class="block transition-slower">Transición lenta, 0.75s (simular al hover)</p>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;p class="no-transition"&gt;Sin transición (simular al hover)&lt;/p&gt;
&lt;p class="transition"&gt;Transición rápida, 0.25s (simular al hover)&lt;/p&gt;
&lt;p class="transition-slow"&gt;Transición media, 0.5s (simular al hover)&lt;/p&gt;
&lt;p class="transition-slower"&gt;Transición lenta, 0.75s (simular al hover)&lt;/p&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */

@mixin transition($ms) {
  -webkit-transition: all $ms ease-in-out;
  -moz-transition: all $ms ease-in-out;
  -ms-transition: all $ms ease-in-out;
  -o-transition: all $ms ease-in-out;
  transition: all $ms ease-in-out;
}

/* Ubicación del @mixin: components/merlin/scss/_animations.scss */

/*------------------------------------------------------*/
/*-------------------- TRANSICIONES --------------------*/
/*------------------------------------------------------*/
$ms: 0.25s;

.no-transition{
  transition: 0;
}

a,
input,
textarea,
select,
input:focus,
textarea:focus,
select:focus,
.transition{
  transition: $ms;
}

.transition-slow{
  transition: $ms*2;
}

.transition-slower{
  transition: $ms*3;
}

.transicion-personalizada{
  @include transition(7s);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #transitions -->


	<section id="zoom-in">
		<div class="tabs">
			<p class="secondary-title">Zoom in</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom</strong> a la imagen para aplicar el efecto</p>
				</div>
				
				<div class="image-container relative overflow-hidden">
					<img class="cover zoom" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */

/*------------------------------------------------------*/
/*----------------------- ZOOM IN ----------------------*/
/*------------------------------------------------------*/
.zoom{
  animation: zoom $ms * 125 infinite;
  -ms-animation: zoom $ms * 125 infinite;
  -moz-animation: zoom $ms * 125 infinite;
  -webkit-animation: zoom $ms * 125 infinite;
}

  @keyframes zoom{
    0%{
      transform: scale(1);
    }
    50%{
      transform: scale(1.15);
    }
    100%{
      transform: scale(1);
    }
  }

  @-webkit-keyframes zoom{
    0%{
      -webkit-transform: scale(1);
    }
    50%{
      -webkit-transform: scale(1.15);
    }
    100%{
      -webkit-transform: scale(1);
    }
  }

  @-moz-keyframes zoom{
    0%{
      -moz-transform: scale(1);
    }
    50%{
      -moz-transform: scale(1.15);
    }
    100%{
      -moz-transform: scale(1);
    }
  }
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-in -->



	<section id="zoom-out">
		<div class="tabs">
			<p class="secondary-title">Zoom out</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom-out</strong> a la imagen para aplicar el efecto</p>
				</div>

				<div class="image-container relative overflow-hidden">
					<img class="cover zoom-out" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom-out" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */
/*------------------------------------------------------*/
/*---------------------- ZOOM OUT ----------------------*/
/*------------------------------------------------------*/
.zoom-out{
  animation: zoom-out $ms * 125 infinite;
  -ms-animation: zoom-out $ms * 125 infinite;
  -moz-animation: zoom-out $ms * 125 infinite;
  -webkit-animation: zoom-out $ms * 125 infinite;
}

@keyframes zoom-out{
  0%{
    transform: scale(1.15);
  }
  50%{
    transform: scale(1);
  }
  100%{
    transform: scale(1.15);
  }
}

@-webkit-keyframes zoom-out{
  0%{
    -webkit-transform: scale(1.15);
  }
  50%{
    -webkit-transform: scale(1);
  }
  100%{
    -webkit-transform: scale(1.15);
  }
}

@-moz-keyframes zoom-out{
  0%{
    -moz-transform: scale(1.15);
  }
  50%{
    -moz-transform: scale(1);
  }
  100%{
    -moz-transform: scale(1.15);
  }
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-out -->


	<section id="zoom-left-top">
		<div class="tabs">
			<p class="secondary-title">Zoom left top</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom</strong> y <strong>.zoom-left-top</strong> a la imagen para aplicar el efecto</p>
				</div>

				<div class="image-container relative overflow-hidden">
					<img class="cover zoom zoom-left-top" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom zoom-left-top" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */
.zoom-left-top{
  transform-origin: 0% 0%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-left-top -->



	<section id="zoom-left-bottom">
		<div class="tabs">
			<p class="secondary-title">Zoom left bottom</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom</strong> y <strong>.zoom-left-bottom</strong> a la imagen para aplicar el efecto</p>
				</div>

				<div class="image-container relative overflow-hidden">
					<img class="cover zoom zoom-left-bottom" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom zoom-left-bottom" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */
.zoom-left-bottom{
  transform-origin: 0% 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-left-bottom -->


	<section id="zoom-right-top">
		<div class="tabs">
			<p class="secondary-title">Zoom right top</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom</strong> y <strong>.zoom-right-top</strong> a la imagen para aplicar el efecto</p>
				</div>
				<div class="image-container relative overflow-hidden">
					<img class="cover zoom zoom-right-top" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom zoom-right-top" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación: components/merlin/scss/_animations.scss */
.zoom-right-top{
  transform-origin: 100% 0%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-right-top -->


	<section id="zoom-right-bottom">
		<div class="tabs">
			<p class="secondary-title">Zoom right bottom</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>Añadir la clase <strong>.zoom</strong> y <strong>.zoom-right-bottom</strong> a la imagen para aplicar el efecto</p>
				</div>
				<div class="image-container relative overflow-hidden">
					<img class="cover zoom zoom-right-bottom" src="https://source.unsplash.com/random">
				</div><!-- image-container -->
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: Esta clase es dependiente de la clase .zoom*/
&lt;div class="image-container relative overflow-hidden"&gt;
  &lt;img class="cover zoom zoom-right-bottom" src="https://source.unsplash.com/random"&gt;
&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="sass">
/* Ubicación: components/merlin/scss/_animations.scss */
.zoom-right-bottom{
  transform-origin: 100% 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #zoom-right-bottom -->




	<section id="border-radius">
		<div class="tabs">
			<p class="secondary-title">Border Radius</p>
			<ul class="tabs-triggers">
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin border-radius($top-left:null, $top-right:null, $bottom-right:null, $bottom-left:null){
    -webkit-border-radius: $top-left $top-right $bottom-right $bottom-left;
    -moz-border-radius: $top-left $top-right $bottom-right $bottom-left;
    -ms-border-radius: $top-left $top-right $bottom-right $bottom-left;
    border-radius: $top-left $top-right $bottom-right $bottom-left;  
}

/*------------------------------------------------------*/
/*--------------- ¿Cómo utilizar el mixin? -------------*/
/*------------------------------------------------------*/
.elemento{
    @include border-radius(10px);
}
.elemento-2{
    @include border-radius(10px 20px 30px 40px);
}


					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #border-radius -->



	<section id="parallax">
		<div class="tabs">
			<p class="secondary-title">Parallax</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Se trabaja con la librería externa <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/js/simpleParallax.min.js" target="_blank">simpleParallax.js</a>. Debes crear el elemento img de clase <strong>.parallax</strong>.</p>
				<div class="image-container relative overflow-hidden">
					<img class="parallax" src="https://source.unsplash.com/random">
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
/*Importante: El plugin genera automáticamente un div que contiene al img de clase .simpleParallax. Los detalles de este div como el del img, se detallan en la pestaña CSS  */
&lt;img class="parallax" src="https://source.unsplash.com/random"&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del CSS: components/merlin/scss/_general.scss */
.simpleParallax{
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
}

.simpleParallax > * {
  object-fit: cover;
  width: 100%;
  height: 100%;
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #parallax -->




	<section id="grayscale">
		<div class="tabs">
			<p class="secondary-title">Filtro blanco y negro</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<p class="message-block text-center">Para aplicar el filtro blanco y negro, añadir la clase <strong>.grayscale</strong>. En caso de revertir el efecto, añadir la clase <strong>.no-grayscale</strong>.</p>
				<div class="image-container">
					<img class="grayscale cover" src="https://source.unsplash.com/random">
				</div>
				<div class="image-container">
					<img class="no-grayscale cover" src="https://source.unsplash.com/random">
				</div>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;div class="grayscale cover" style="https://source.unsplash.com/random"&gt;&lt;/div&gt;
&lt;div class="no-grayscale cover" style="https://source.unsplash.com/random"&gt;&lt;/div&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del CSS: components/merlin/scss/_general.scss */
/*------------------------------------------------------*/
/*---------------------- GRÁFICA -----------------------*/
/*------------------------------------------------------*/
.grayscale{
  -webkit-filter: grayscale(100%);
  filter: grayscale(100%);
}
.no-grayscale{
  -webkit-filter: grayscale(0%);
  filter: grayscale(0%);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #grayscale -->






	<section id="gradients">
		<div class="tabs">
			<p class="secondary-title">Gradientes</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#css">CSS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block">
					<p>Cada vez que se requiera generar un gradiente, se debe utilizar el <strong>@mixin linear-gradient</strong>, el cual maneja 3 parámetros:</p>
					<ul>
						<li><strong>$color-inicial</strong>, un <strong>$color-final</strong> y un <strong>ángulo</strong>.</li>
						<li>El modo de uso es: <strong>@include linear-gradient($color-inicial, $color-final, 0deg);</strong></li>
					</ul>
				</div>
				<div class="grid-column-3 gap-m">
					<div class="block block-gradient block-gradient-1 text-center">
						<p>Gradiente 0º</p>
					</div>
					<div class="block block-gradient block-gradient-2 text-center">
						<p>Gradiente 180º</p>
					</div>
					<div class="block block-gradient block-gradient-3 text-center">
						<p>Gradiente 58º</p>
					</div>
				</div><!-- grid-column-x -->
			</div><!-- tab-content -->

			<div id="css" class="tab-content">
				<pre>
					<code class="css">
/* Ubicación del @mixin: components/merlin/scss/_mixins.scss */
@mixin linear-gradient($color-one,$color-two,$degrees) {
  background: -moz-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -webkit-gradient(linear, left top, right top, color-stop(0%, $color-one), color-stop(100%, $color-two));
  background: -webkit-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -o-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: -ms-linear-gradient($degrees, $color-one 0%, $color-two 100%);
  background: linear-gradient($degrees, $color-one 0%, $color-two 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='$color-one', endColorstr='$color-two',GradientType=1 );
}

/*------------------------------------------------------*/
/*--------------- ¿Cómo utilizar el mixin? -------------*/
/*------------------------------------------------------*/
.elemento{
  @include linear-gradient($color-inicial, $color-final, 0deg);
}
					</code>
				</pre>
			</div><!-- tab-content -->
		</div><!-- tabs -->
	</section><!-- #gradients -->





	<section id="social">
		<div class="tabs">
			<p class="primary-title">Íconos</p>
			<p class="secondary-title">Social</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
				<li><a href="#sass">SASS</a></li>
			</ul>

			<div id="example" class="tab-content">
				<div class="message-block text-center">
					<p>La familia tipográfica para la iconografía utiliza la herramienta <a href="https://icomoon.io/" target="_blank">icomoon.io</a>. Se puede descargar el archivo <a href="<?php echo get_stylesheet_directory_uri(); ?>/components/merlin/icons/selection.json" target="_blank">selection.json</a> para complementarlo o simplemente para "limpiarlo", dejando sólo los necesarios en tu proyecto.</p>
					<br>
					<p>Si eres diseñador, puedes <a href="<?php echo site_url(); ?>/diseno/v.1.5/icons/SVG.zip" target="_blank">descargarte los SVG</a> para comenzar a trabajar.</p>
				</div>

				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-pinterest"></i>
						<span>.icon-pinterest</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-skype"></i>
						<span>.icon-skype</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-spotify"></i>
						<span>.icon-spotify</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-whatsapp"></i>
						<span>.icon-whatsapp</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-facebook"></i>
						<span>.icon-facebook</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-instagram"></i>
						<span>.icon-instagram</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-soundcloud"></i>
						<span>.icon-soundcloud</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-twitter"></i>
						<span>.icon-twitter</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-youtube-isotipo"></i>
						<span>.icon-youtube-isotipo</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-linkedin"></i>
						<span>.icon-linkedin</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-youtube-logotipo"></i>
						<span>.icon-youtube-logotipo</span>
					</li>
				</ul>
			</div><!-- tab-content -->

			<div id="html" class="tab-content">
				<pre>
					<code class="html">
&lt;i class="icon-pinterest"&gt;&lt;/i&gt;
					</code>
				</pre>
			</div><!-- tab-content -->

			<div id="sass" class="tab-content">
				<pre>
					<code class="css">
/*
Ubicacion de la iconografía:
1. components/merlin/icons/icomoon.eot
2. components/merlin/icons/icomoon.svg
3. components/merlin/icons/icomoon.ttf
4. components/merlin/icons/icomoon.woff

Además, si se quiere complementar la familia, se puede descargar el archivo components/merlin/icons/selection.json

Ubicacion del sass: components/merlin/scss/icons.css

Existe un @mixin ubicado en components/merlin/scss/mixins.css
*/
@mixin icomoon($icon, $position: "before", $replace: false) {
  @if $replace {
    font-size: 0;
  }
  &:#{$position} {
    @extend .#{$icon};
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    line-height: 1;
    @if $replace {
      font-size: $font * 16;
    }
    @content;
  }
}


/*
Este @mixin sirve para llamar a los íconos dentro de un elemento en SASS. Por ejemplo:
li{
  @include icomoon("icon-flecha-derecha-mediana");
}
En el ejemplo anterior, cualquier elemento de tipo li, tendrá antecedido el icon-flecha-derecha-mediana
*/
					</code>
				</pre>
			</div><!-- tab-content -->
	</section><!-- #social -->




	<section id="arrows-down">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia abajo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-mediana"></i>
						<span>.icon-flecha-abajo-mediana</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-chevron-abajo"></i>
						<span>.icon-chevron-abajo</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-circulo"></i>
						<span>.icon-flecha-abajo-circulo</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-chevrons-abajo"></i>
						<span>.icon-chevrons-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-descargar-nube"></i>
						<span>.icon-descargar-nube</span>
					</li>

					<li class="block block-icon text-center">
						<i class="icon-descargar"></i>
						<span>.icon-descargar</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-down -->



	<section id="arrows-left">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia la izquierda</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-izquierda-mediana"></i>
						<span>.icon-flecha-izquierda-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-izquierda"></i>
						<span>.icon-chevron-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-izquierda-circulo"></i>
						<span>.icon-flecha-izquierda-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-izquierda"></i>
						<span>.icon-chevrons-izquierda</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-left -->



	<section id="arrows-right">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia la derecha</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-derecha-mediana"></i>
						<span>.icon-flecha-derecha-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-derecha"></i>
						<span>.icon-chevron-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-derecha-circulo"></i>
						<span>.icon-flecha-derecha-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-derecha"></i>
						<span>.icon-chevrons-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-log-in"></i>
						<span>.icon-log-in</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-log-out"></i>
						<span>.icon-log-out</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-right -->



	<section id="arrows-up">
		<div class="tabs">
			<p class="secondary-title">Flechas hacia arriba</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-mediana"></i>
						<span>.icon-flecha-arriba-mediana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevron-arriba"></i>
						<span>.icon-chevron-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-circulo"></i>
						<span>.icon-flecha-arriba-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-chevrons-arriba"></i>
						<span>.icon-chevrons-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-subir"></i>
						<span>.icon-subir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-compartir"></i>
						<span>.icon-compartir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-subir-nube"></i>
						<span>.icon-subir-nube</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-up -->




	<section id="arrows-directional">
		<div class="tabs">
			<p class="secondary-title">Flechas direccionales</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-refrescar"></i>
						<span>.icon-refrescar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-repetir"></i>
						<span>.icon-repetir</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rotar-1"></i>
						<span>.icon-rotar-1</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rotar-2"></i>
						<span>.icon-rotar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-izquierda"></i>
						<span>.icon-flecha-arriba-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-arriba-derecha"></i>
						<span>.icon-flecha-arriba-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-izquierda"></i>
						<span>.icon-flecha-abajo-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flecha-abajo-derecha"></i>
						<span>.icon-flecha-abajo-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-izquierda"></i>
						<span>.icon-virar-abajo-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-derecha"></i>
						<span>.icon-virar-abajo-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-izquierda"></i>
						<span>.icon-virar-arriba-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-derecha"></i>
						<span>.icon-virar-arriba-derecha</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo"></i>
						<span>.icon-virar-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-abajo-2"></i>
						<span>.icon-virar-abajo-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba"></i>
						<span>.icon-virar-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-virar-arriba-2"></i>
						<span>.icon-virar-arriba-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tendencia-arriba"></i>
						<span>.icon-tendencia-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tendencia-abajo"></i>
						<span>.icon-tendencia-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-codigo"></i>
						<span>.icon-codigo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-external-link"></i>
						<span>.icon-external-link</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maximizar-2"></i>
						<span>.icon-maximizar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-minimizar-2"></i>
						<span>.icon-minimizar-2</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #arrows-directional -->





	<section id="controls">
		<div class="tabs">
			<p class="secondary-title">Controles</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-monitor"></i>
						<span>.icon-monitor</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bandera"></i>
						<span>.icon-bandera</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llave"></i>
						<span>.icon-llave</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-trello"></i>
						<span>.icon-trello</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tv"></i>
						<span>.icon-tv</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-candado"></i>
						<span>.icon-candado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-candado-off"></i>
						<span>.icon-candado-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-campana-off"></i>
						<span>.icon-campana-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-campana"></i>
						<span>.icon-campana</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-horizontal"></i>
						<span>.icon-mas-horizontal</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-vertical"></i>
						<span>.icon-mas-vertical</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flash-off"></i>
						<span>.icon-flash-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-flash"></i>
						<span>.icon-flash</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-camara-off"></i>
						<span>.icon-camara-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-camara"></i>
						<span>.icon-camara</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-cast"></i>
						<span>.icon-cast</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ojo-off"></i>
						<span>.icon-ojo-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ojo"></i>
						<span>.icon-ojo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-filtro"></i>
						<span>.icon-filtro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-microfono-off"></i>
						<span>.icon-microfono-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-microfono"></i>
						<span>.icon-microfono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-minimizar"></i>
						<span>.icon-minimizar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maximizar"></i>
						<span>.icon-maximizar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-move"></i>
						<span>.icon-move</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-play"></i>
						<span>.icon-play</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pausa-circulo"></i>
						<span>.icon-pausa-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pausa"></i>
						<span>.icon-pausa</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rebobinar-2"></i>
						<span>.icon-rebobinar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-adelantar-2"></i>
						<span>.icon-adelantar-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rebobinar"></i>
						<span>.icon-rebobinar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-adelantar"></i>
						<span>.icon-adelantar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-telefono"></i>
						<span>.icon-telefono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamada"></i>
						<span>.icon-llamada</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamar"></i>
						<span>.icon-llamar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-llamada-recibida"></i>
						<span>.icon-llamada-recibida</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-phone-missed"></i>
						<span>.icon-phone-missed</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-telefono-off"></i>
						<span>.icon-telefono-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-phone-outgoing"></i>
						<span>.icon-phone-outgoing</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-power"></i>
						<span>.icon-power</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-configuracion"></i>
						<span>.icon-configuracion</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-escudo-off"></i>
						<span>.icon-escudo-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-escudo"></i>
						<span>.icon-escudo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-aleatorio"></i>
						<span>.icon-aleatorio</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-controles"></i>
						<span>.icon-controles</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-stop-circulo"></i>
						<span>.icon-stop-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pulgar-abajo"></i>
						<span>.icon-pulgar-abajo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pulgar-arriba"></i>
						<span>.icon-pulgar-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-video-off"></i>
						<span>.icon-video-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-video"></i>
						<span>.icon-video</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volume-1"></i>
						<span>.icon-volume-1</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volume-2"></i>
						<span>.icon-volume-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volumen-off"></i>
						<span>.icon-volumen-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-volumen"></i>
						<span>.icon-volumen</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-wifi-off"></i>
						<span>.icon-wifi-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-wifi"></i>
						<span>.icon-wifi</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-zoom-in"></i>
						<span>.icon-zoom-in</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-zoom-out"></i>
						<span>.icon-zoom-out</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #controls -->

	<section id="weather">
		<div class="tabs">
			<p class="secondary-title">Clima</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-paragua"></i>
						<span>.icon-paragua</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-llovizna"></i>
						<span>.icon-nube-llovizna</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-rayo"></i>
						<span>.icon-nube-rayo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-off"></i>
						<span>.icon-nube-off</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-lluvia"></i>
						<span>.icon-nube-lluvia</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube-nieve"></i>
						<span>.icon-nube-nieve</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-nube"></i>
						<span>.icon-nube</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-gota"></i>
						<span>.icon-gota</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-luna"></i>
						<span>.icon-luna</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-estrella"></i>
						<span>.icon-estrella</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-sol"></i>
						<span>.icon-sol</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-amanecer"></i>
						<span>.icon-amanecer</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-atardecer"></i>
						<span>.icon-atardecer</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-viento"></i>
						<span>.icon-viento</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #weather -->




	<section id="persons">
		<div class="tabs">
			<p class="secondary-title">Personas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-usuario"></i>
						<span>.icon-usuario</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-check"></i>
						<span>.icon-usuario-check</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-menos"></i>
						<span>.icon-usuario-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-mas"></i>
						<span>.icon-usuario-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuario-eliminar"></i>
						<span>.icon-usuario-eliminar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-usuarios"></i>
						<span>.icon-usuarios</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #persons -->




	<section id="content">
		<div class="tabs">
			<p class="secondary-title">Contenido</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-carpeta-menos"></i>
						<span>.icon-carpeta-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carpeta-mas"></i>
						<span>.icon-carpeta-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carpeta"></i>
						<span>.icon-carpeta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-basurero-lineas"></i>
						<span>.icon-basurero-lineas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-basurero"></i>
						<span>.icon-basurero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-clip"></i>
						<span>.icon-clip</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-papelero"></i>
						<span>.icon-papelero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-libro-abierto"></i>
						<span>.icon-libro-abierto</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-libro"></i>
						<span>.icon-libro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-marcador-de-libro"></i>
						<span>.icon-marcador-de-libro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-caja"></i>
						<span>.icon-caja</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-portapapeles"></i>
						<span>.icon-portapapeles</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar"></i>
						<span>.icon-editar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar-linea"></i>
						<span>.icon-editar-linea</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-editar-cuadrado"></i>
						<span>.icon-editar-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-menos"></i>
						<span>.icon-archivo-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-mas"></i>
						<span>.icon-archivo-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo-texto"></i>
						<span>.icon-archivo-texto</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-archivo"></i>
						<span>.icon-archivo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-inbox"></i>
						<span>.icon-inbox</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-paquete"></i>
						<span>.icon-paquete</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-impresora"></i>
						<span>.icon-impresora</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-link-2"></i>
						<span>.icon-link-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-link"></i>
						<span>.icon-link</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-centro"></i>
						<span>.icon-alinear-centro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-justificado"></i>
						<span>.icon-alinear-justificado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-izquierda"></i>
						<span>.icon-alinear-izquierda</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alinear-derecha"></i>
						<span>.icon-alinear-derecha</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #content -->




	<section id="alerts">
		<div class="tabs">
			<p class="secondary-title">Alertas</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-menos-circulo"></i>
						<span>.icon-menos-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menos-cuadrado"></i>
						<span>.icon-menos-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menos"></i>
						<span>.icon-menos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas"></i>
						<span>.icon-mas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-prohibido"></i>
						<span>.icon-prohibido</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-circulo"></i>
						<span>.icon-equis-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-octagono"></i>
						<span>.icon-equis-octagono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis-cuadrado"></i>
						<span>.icon-equis-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-circulo"></i>
						<span>.icon-alerta-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-octagono"></i>
						<span>.icon-alerta-octagono</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-alerta-triangulo"></i>
						<span>.icon-alerta-triangulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-ayuda-circulo"></i>
						<span>.icon-ayuda-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-info"></i>
						<span>.icon-info</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-equis"></i>
						<span>.icon-equis</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check"></i>
						<span>.icon-check</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check-cuadrado"></i>
						<span>.icon-check-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-check-circle"></i>
						<span>.icon-check-circle</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #alerts -->



	<section id="misc">
		<div class="tabs">
			<p class="secondary-title">Misceláneo</p>
			<ul class="tabs-triggers">
				<li><a href="#example">Ejemplo</a></li>
				<li><a href="#html">HTML</a></li>
			</ul>

			<div id="example" class="tab-content">
				<ul class="grid-column-6 gap-s">
					<li class="block block-icon text-center">
						<i class="icon-arroba"></i>
						<span>.icon-arroba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-medalla"></i>
						<span>.icon-medalla</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-barras-2"></i>
						<span>.icon-grafico-barras-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-barras"></i>
						<span>.icon-grafico-barras</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bateria-cargando"></i>
						<span>.icon-bateria-cargando</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bateria"></i>
						<span>.icon-bateria</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bluetooth"></i>
						<span>.icon-bluetooth</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-maleta"></i>
						<span>.icon-maleta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-calendario"></i>
						<span>.icon-calendario</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-reloj"></i>
						<span>.icon-reloj</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-brujula"></i>
						<span>.icon-brujula</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-copiar"></i>
						<span>.icon-copiar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tarjeta-de-credito"></i>
						<span>.icon-tarjeta-de-credito</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-base-de-datos"></i>
						<span>.icon-base-de-datos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-peso"></i>
						<span>.icon-peso</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pelicula"></i>
						<span>.icon-pelicula</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-regalo"></i>
						<span>.icon-regalo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mundo"></i>
						<span>.icon-mundo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grilla"></i>
						<span>.icon-grilla</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-disco-duro"></i>
						<span>.icon-disco-duro</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-hashtag"></i>
						<span>.icon-hashtag</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-audifonos"></i>
						<span>.icon-audifonos</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-corazon"></i>
						<span>.icon-corazon</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-imagen"></i>
						<span>.icon-imagen</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-capas"></i>
						<span>.icon-capas</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-layout"></i>
						<span>.icon-layout</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-lista"></i>
						<span>.icon-lista</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mail"></i>
						<span>.icon-mail</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-pin"></i>
						<span>.icon-pin</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mapa"></i>
						<span>.icon-mapa</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-menu"></i>
						<span>.icon-menu</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mensaje-circular"></i>
						<span>.icon-mensaje-circular</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mensaje-cuadrado"></i>
						<span>.icon-mensaje-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-puntero"></i>
						<span>.icon-puntero</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-musica"></i>
						<span>.icon-musica</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-navigation-arriba"></i>
						<span>.icon-navigation-arriba</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-navegacion-diagonal"></i>
						<span>.icon-navegacion-diagonal</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-porcentaje"></i>
						<span>.icon-porcentaje</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grafico-torta"></i>
						<span>.icon-grafico-torta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-play-circulo"></i>
						<span>.icon-play-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-circulo"></i>
						<span>.icon-mas-circulo</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mas-cuadrado"></i>
						<span>.icon-mas-cuadrado</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-radio"></i>
						<span>.icon-radio</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-rss"></i>
						<span>.icon-rss</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-diskette"></i>
						<span>.icon-diskette</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-tijera"></i>
						<span>.icon-tijera</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-buscar"></i>
						<span>.icon-buscar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-mail-2"></i>
						<span>.icon-mail-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-servidor"></i>
						<span>.icon-servidor</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-compartir-2"></i>
						<span>.icon-compartir-2</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-bolsa-shopping"></i>
						<span>.icon-bolsa-shopping</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-carrito"></i>
						<span>.icon-carrito</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-sidebar"></i>
						<span>.icon-sidebar</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-etiqueta"></i>
						<span>.icon-etiqueta</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-grabadora"></i>
						<span>.icon-grabadora</span>
					</li>
					<li class="block block-icon text-center">
						<i class="icon-reloj-muneca"></i>
						<span>.icon-reloj-muneca</span>
					</li>
				</ul>
			</div><!-- tab-content -->
	</section><!-- #misc -->
<?php get_footer(); ?>

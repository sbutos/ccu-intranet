<?php
/*
* Template Name: Innovación
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="page-heading innovacion-heading">
            <?php
        $pageThumbImg = get_the_post_thumbnail_url();
        $pageThumbnailID = get_post_thumbnail_ID();
        $alt = get_post_meta ( $pageThumbnailID, '_wp_attachment_image_alt', true );
        ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $pageThumbImg; ?>)"
                title="<?php echo $alt; ?>">
                <div class="veil"></div>
            </div>
            <div class="content">
                <h1><?php the_title(); ?></h1>
                <div class="intro-page">
                    <?php the_field( 'introduccion' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section tabs-page-area">
    <div class="wrap-xl">
        <div id="innovacion-tabs">
            <ul class="tabs-triggers">
                <li>
                    <a href="#modelo"
                        class="btn size-s is-rounded is-celeste is-bordered"><?php the_field( 'nombre_pestana_modelo' ); ?></a>
                </li>
                <li>
                    <a href="#productos"
                        class="btn size-s is-rounded is-celeste is-bordered"><?php the_field( 'nombre_pestana_productos' ); ?></a>
                </li>
                <li>
                    <a href="#programas"
                        class="btn size-s is-rounded is-celeste is-bordered"><?php the_field( 'nombre_pestana_programas' ); ?></a>
                </li>
                <li>
                    <a href="#tendencias" class="btn size-s is-rounded is-celeste is-bordered">Tendencias</a>
                </li>
                <li>
                    <a href="#videos" class="btn size-s is-rounded is-celeste is-bordered">Videos</a>
                </li>
            </ul>
            <div id="modelo" class="tab-content">
                <?php if ( have_rows( 'modelo_innovacion' ) ) : ?>
                <div class="content">
                    <?php while ( have_rows( 'modelo_innovacion' ) ) : the_row(); ?>
                    <div class="heading-box-area">
                        <h3 class="head-title color-celeste"><?php the_sub_field( 'titulo_modelo' ); ?></h3>
                    </div>
                    <?php if ( have_rows( 'modelos' ) ) : ?>
                    <div class="icono-color-area">
                        <?php while ( have_rows( 'modelos' ) ) : the_row(); ?>
                        <?php $fondo_m = get_sub_field( 'fondo_m' ); ?>
                        <?php $icono_m = get_sub_field( 'icono_m' ); ?>
                        <div class="icono-color-box">
                            <div class="photo-bg cover" style="background-image: url(<?php echo $fondo_m['url']; ?>);"
                                title="<?php echo $fondo_m['alt']; ?>">
                                <div class="veil color-<?php the_sub_field( 'color_m' ); ?>"></div>
                            </div>
                            <div class="content-box">
                                <img src="<?php echo $icono_m['url']; ?>" alt="<?php echo $icono_m['alt']; ?>"
                                    class="icono-rel">
                                <h4 class="titulo"><?php the_sub_field( 'titulo_m' ); ?></h4>
                                <div class="bajada">
                                    <?php the_sub_field( 'bajada_m' ); ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
                <div class="flex align-start justify-between">
                    <?php if ( have_rows( 'noticias_innovacion' ) ) : ?>
                    <div class="news-area layout-one-feat col-66">
                        <?php while ( have_rows( 'noticias_innovacion' ) ) : the_row(); ?>
                        <div class="content">
                            <div class="heading-box-area">
                                <h3 class="head-title color-celeste"><?php the_sub_field( 'titulo_n' ); ?></h3>
                                <?php $link_todos_n_term = get_sub_field( 'link_todos_n' ); ?>
                                <?php if ( $link_todos_n_term ) { ?>
                                <a href="<?php echo site_url('/'); ?>repositorio/"
                                    data-this-tax="<?php echo $link_todos_n_term->slug; ?>"
                                    class="btn-ver-todas color-celeste"><span>Ver
                                        Todas</span><i class="icon-chevron-right"></i></a>
                                <?php } ?>
                            </div>

                            <div class="layout-news-area">
                                <?php $noticias_n = get_sub_field( 'noticias_n' ); ?>
                                <?php if ( $noticias_n ): ?>
                                <?php foreach ( $noticias_n as $post ):  ?>
                                <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                                <div class="small-news-area border-radius-m">
                                    <div class="photo cover"
                                        style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                        title="<?php echo $alt; ?>">
                                        <div class="veil"></div>
                                    </div>
                                    <div class="content">
                                        <div class="post-cat-area">
                                            <?php $category_detail=get_the_category($post->ID);//$post->ID
                                    foreach($category_detail as $cd){
                                    echo '<span>'.$cd->cat_name.'</span>';
                                    } ?>
                                        </div>
                                        <div class="content-area">
                                            <div class="post-info">
                                                <span class="fecha"><?php the_date(); ?></span>
                                                <h3 class="post-title">
                                                    <?php the_title(); ?>
                                                </h3>
                                            </div>
                                            <div class="button-area">
                                                <a href="<?php the_permalink(); ?>"
                                                    class="btn is-celeste is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php wp_reset_postdata(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php endif; ?>
                    <?php if ( have_rows( 'eventos_innovacion' ) ) : ?>
                    <div class="slide-events-area col-32">
                        <?php while ( have_rows( 'eventos_innovacion' ) ) : the_row(); ?>
                        <div class="content">
                            <div class="heading-box-area">
                                <h3 class="head-title color-celeste"><?php the_sub_field( 'titulo_e' ); ?></h3>
                            </div>
                            <?php $eventos_slider_e = get_sub_field( 'eventos_slider_e' ); ?>
                            <?php if ( $eventos_slider_e ): ?>
                            <div class="slider-area border-radius-m">
                                <div id="events-arrows" class="arrows">
                                    <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                                    <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
                                </div>
                                <div id="events-slider">
                                    <?php foreach ( $eventos_slider_e as $post ):  ?>
                                    <?php setup_postdata ( $post ); 
                                    $eventThumb = get_the_post_thumbnail_url();
                                    $eventThumbID = get_post_thumbnail_ID();
                                    $eventThumbAlt = get_post_meta ( $eventThumbID, '_wp_attachment_image_alt', true );
                                    ?>
                                    <div class="slide">
                                        <div class="slide-content">
                                            <div class="post-cat-area">
                                                <?php $category_detail = get_the_category($post->ID);
                                                foreach($category_detail as $cd){
                                                echo '<span>'.$cd->cat_name.'</span>';
                                                } ?>
                                            </div>
                                            <div class="photo cover"
                                                style="background-image: url(<?php echo $eventThumb; ?>)"
                                                title="<?php echo $eventThumbAlt; ?>">
                                                <div class="veil"></div>
                                            </div>
                                            <div class="content">
                                                <span class="fecha"><?php the_date(); ?></span>
                                                <h3 class="post-title">
                                                    <?php the_title(); ?>
                                                </h3>
                                                <div class="button-area">
                                                    <a href="<?php the_permalink(); ?>"
                                                        class="btn is-celeste is-rounded is-bordered">
                                                        <?php _e('Saber Más', 'ccu-intranet'); ?>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                                <?php wp_reset_postdata(); ?>
                                <?php endif; ?>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            <div id="productos" class="tab-content">
                <?php $noticias_tax_productos_term = get_field( 'noticias_tax_productos' ); ?>
                <?php if ( $noticias_tax_productos_term ): ?>
                <?php $newsProdTax = $noticias_tax_productos_term->name; ?>
                <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="6" repeater="template_1" post_type="post" posts_per_page="6" pause="true" button_label="Ver Más" taxonomy="category" taxonomy_terms="'.$newsProdTax.'" taxonomy_operator="IN"]'); ?>
                <?php endif; ?>
            </div>
            <div id="programas" class="tab-content">
                <div class="content">
                    <?php if ( have_rows( 'programas_innovacion' ) ) : ?>
                    <div class="heading-box-area">
                        <h3 class="head-title color-celeste"><?php the_field( 'nombre_pestana_programas' ); ?></h3>
                    </div>
                    <div class="programas-innovacion-area">
                        <div class="grid-column-2 gap-l">
                            <?php while ( have_rows( 'programas_innovacion' ) ) : the_row(); ?>
                            <div class="programa-box">
                                <?php $imagen_rel_pi = get_sub_field( 'imagen_rel_pi' ); ?>
                                <?php if ( $imagen_rel_pi ) { ?>
                                <div class="programa-img">
                                    <div class="ccu-logo-corner">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-ccu-c.svg"
                                            alt="">
                                    </div>
                                    <img src="<?php echo $imagen_rel_pi['url']; ?>"
                                        alt="<?php echo $imagen_rel_pi['alt']; ?>">
                                </div>
                                <?php } ?>
                                <div class="programa-info">
                                    <div class="descripcion">
                                        <?php the_sub_field( 'descripcion_pi' ); ?>
                                    </div>
                                    <?php $link_externo_pi = get_sub_field( 'link_externo_pi' ); ?>
                                    <?php if ( $link_externo_pi ) { ?>
                                    <div class="button-area">
                                        <a href="<?php echo $link_externo_pi['url']; ?>"
                                            target="<?php echo $link_externo_pi['target']; ?>"
                                            class="btn is-celeste is-rounded is-bordered size-s">
                                            <?php echo $link_externo_pi['title']; ?>
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php if ( have_rows( 'noticias_programas_innovacion' ) ) : ?>
                <div class="news-area layout-one-feat-two-third">
                    <?php while ( have_rows( 'noticias_programas_innovacion' ) ) : the_row(); ?>
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title color-celeste"><?php the_sub_field( 'titulo_pi' ); ?></h3>
                            <?php $link_todos_pi_term = get_sub_field( 'link_todos_pi' ); ?>
                            <?php if ( $link_todos_pi_term ) { ?>
                            <a href="<?php echo site_url('/'); ?>repositorio/"
                                data-this-tax="<?php echo $link_todos_pi_term->slug; ?>"
                                class="btn-ver-todas color-celeste"><span>Ver
                                    Todas</span><i class="icon-chevron-right"></i></a>
                            <?php } ?>
                        </div>

                        <div class="layout-news-area">
                            <?php $noticias_pi = get_sub_field( 'noticias_pi' ); ?>
                            <?php if ( $noticias_pi ): ?>
                            <?php foreach ( $noticias_pi as $post ):  ?>
                            <?php setup_postdata ( $post );
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <div class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php $category_detail=get_the_category($post->ID);//$post->ID
                                    foreach($category_detail as $cd){
                                    echo '<span>'.$cd->cat_name.'</span>';
                                    } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="post-info">
                                            <span class="fecha"><?php the_date(); ?></span>
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                        <div class="button-area">
                                            <a href="<?php the_permalink(); ?>"
                                                class="btn is-celeste is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
            <div id="tendencias" class="tab-content">
                <?php $news_tax_term = get_field( 'news_tax' ); ?>
                <?php if ( $news_tax_term ): ?>
                <?php $tax_slug = $news_tax_term->slug; ?>
                <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="4" repeater="template_1" post_type="post" posts_per_page="4" pause="true" button_label="Ver Más" taxonomy="category" taxonomy_terms="'.$tax_slug.'" taxonomy_operator="IN"]'); ?>
                <?php endif; ?>
            </div>
            <div id="videos" class="tab-content">
                <?php $video_tax_term = get_field( 'video_tax' ); ?>
                <?php if ( $video_tax_term ): ?>
                <?php $tax_slug = $video_tax_term->slug; ?>
                <?php echo do_shortcode('[ajax_load_more preloaded="true" preloaded_amount="4" repeater="template_1" post_type="videos_cl" posts_per_page="4" pause="true" button_label="Ver Más" taxonomy="cat_video" taxonomy_terms="'.$tax_slug.'" taxonomy_operator="IN"]'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $('#productos .news-area .alm-reveal').prepend(
        '<div class="grid-sizer"></div><div class="gutter-sizer"></div>');
    $('#productos .news-area .alm-reveal .small-news-area:first, #productos .news-area .alm-reveal .small-news-area:nth-child(6n+2)')
        .addClass('grid-size-2');

    $('#tendencias .news-area .alm-reveal').prepend(
        '<div class="grid-sizer"></div><div class="gutter-sizer"></div>');
    $('#tendencias .news-area .alm-reveal .small-news-area:first').addClass('grid-size-3');
    $('#tendencias .news-area .alm-reveal .small-news-area:nth-child(3n+2)').addClass('grid-size-2');

    $('#videos .news-area .alm-reveal').prepend(
        '<div class="grid-sizer"></div><div class="gutter-sizer"></div>');
    $('#videos .news-area .alm-reveal .small-news-area:first').addClass('grid-size-2');
    $('#videos .news-area .alm-reveal .small-news-area:nth-child(4n+2)').addClass('grid-size-2');

    $("#innovacion-tabs").tabs({
        show: 'fade',
        hide: 'fade',
        activate: function(event, ui) {
            newTab = $(ui['newTab'][0]).attr('data-tab');
            console.log($(ui['newTab'][0]).attr('data-tab'));
            newTab = $('#events-slider').slick('setPosition');
            $('.ui-state-default a').addClass('is-bordered');
            $('.ui-tabs-active a').removeClass('is-bordered');
            $('#productos .news-area .alm-reveal').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                gutter: '.gutter-sizer'
            });
            $('#tendencias .news-area .alm-reveal').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                gutter: '.gutter-sizer'
            });

            $('#videos .news-area .alm-reveal').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                gutter: '.gutter-sizer'
            });
        },
        create: function(event, ui) {
            $('.ui-tabs-active a').removeClass('is-bordered');
        }
    });
    $('#events-slider').slick({
        arrows: false,
        dots: false,
        speed: 750
    });
    $('#events-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#events-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#events-slider').slick('slickNext');
            });
        }
    });
    $('.btn-ver-todas').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let permalink = $(this).attr('href');
            let taxonomia = $(this).data('this-tax');
            $("<form method='POST' action='" + permalink +
                    "'><input type='hidden' name='thistax' value='" + taxonomia + "'/></form>")
                .appendTo("body").submit();
        });
    });
    $('#productos .news-area .alm-reveal').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: '.gutter-sizer'
    });
    $('#tendencias .news-area .alm-reveal').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: '.gutter-sizer'
    });
    $('#videos .news-area .alm-reveal').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        gutter: '.gutter-sizer'
    });
});
</script>
<?php get_footer(); ?>
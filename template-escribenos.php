<?php
/*
* Template Name: Escríbenos
*/
get_header();
?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_title(); ?></h3>
            </div>
            <div class="form-box">
                <?php echo do_shortcode('[contact-form-7 id="919" title="Formulario de Contacto"]', false); ?>
            </div>
        </div>
    </div>
</section>
<script>
$(document).ready(function() {
    $(".file-input").each(function() {
        var $input = $(this),
            $label = $("#adj-file"),
            labelVal = $label.html();

        $input.on("change", function(e) {
            var fileName = "";

            if (e.target.value) {
                fileName = e.target.value.split("\\").pop();
            }

            if (fileName) {
                $label.find("span").html(fileName);
            } else {
                $label.html(labelVal);
            }
        });

        // Firefox bug fix
        $input
            .on("focus", function() {
                $input.addClass("has-focus");
            })
            .on("blur", function() {
                $input.removeClass("has-focus");
            });
    });
});
</script>
<?php get_footer(); ?>
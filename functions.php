<?php

function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyCK9PkyUNwQNhK4VMJI5n3RNCqtH9cJgGc';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
/*-------------------------------------------------------------*/
/*--- No mostrar la version de Wordpress dentro del <head> ----*/
/*-------------------------------------------------------------*/
function eliminar_version_wordpress() {
return '';
}
add_filter('the_generator', 'eliminar_version_wordpress');


/*-------------------------------------------------------------*/
/*-------------- Eliminar barra de administración -------------*/
/*-------------------------------------------------------------*/
function quitar_barra_administracion()	{
	return false;
}
add_filter( 'show_admin_bar' , 'quitar_barra_administracion');

/*-------------------------------------------------------------*/
/*---------------------- Eliminar Tags ------------------------*/
/*-------------------------------------------------------------*/
add_action('init', 'remove_tags');
function remove_tags(){
    register_taxonomy('post_tag', array());
}

/*-------------------------------------------------------------*/
/*--------- Permito imagen destacada en los Posts -------------*/
/*-------------------------------------------------------------*/
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );

/*-------------------------------------------------------------*/
/*------------------ Menús personalizados ---------------------*/
/*-------------------------------------------------------------*/
register_nav_menus( array(
	'Menu principal' => 'Menu principal',
	'Menu extra' => 'Menu extra',
	'Menu footer' => 'Menu footer',
));

/*-------------------------------------------------------------*/
/*--- Le añado la clase "active" al elemento actual del menu ---*/
/*-------------------------------------------------------------*/
add_filter('nav_menu_css_class', function ($classes, $item, $args, $depth) {
    //Todas las diferentes clases "active" añadidas por WordPress
    $active = [
        'current-menu-item',
        'current-menu-parent',
        'current-menu-ancestor',
        'current_page_item'
    ];
    //Si coincide, añade la clase "active"
    if ( array_intersect( $active, $classes ) ) {
        $classes[] = 'active';
    }
    return $classes;
}, 10, 4);

/*----------------------------------------------------------------------------*/
/*---------------------------- Eliminar Gutenberg ----------------------------*/
/*----------------------------------------------------------------------------*/
add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*-------------------------------------------------------------*/
/*--------- Compatibilidad del tema con Woocommerce -----------*/
/*-------------------------------------------------------------*/
function my_theme_setup() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'my_theme_setup' );


/*-------------------------------------------------------------*/
/*------------- Añadimos los CSS y JS del Theme ---------------*/
/*-------------------------------------------------------------*/
function merlin_styles_and_scripts() {
	wp_deregister_script( 'jquery' );

	//jQuery
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/components/merlin/js/jquery/jquery.js');

	//jQuery UI
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/components/merlin/js/jquery/jquery-ui.js');

	//Masonry
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/components/custom/js/masonry.pkgd.min.js');

	//Lazy Loading
	wp_enqueue_script( 'lazysizes', get_template_directory_uri() . '/components/merlin/js/lazysizes.js');

	//Slick slider
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/components/merlin/js/slick/slick.min.js');
	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/components/merlin/js/slick/slick.css');

	//Lightgallery
	wp_enqueue_script( 'lightgallery', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.js');
	wp_enqueue_style( 'lightgallery-style', get_template_directory_uri() . '/components/merlin/js/lightgallery/lightgallery.css');

	//AOS
	wp_enqueue_script( 'aos', get_template_directory_uri() . '/components/merlin/js/aos/aos.js');
	wp_enqueue_style( 'aos-style', get_template_directory_uri() . '/components/merlin/js/aos/aos.css');

	//Scripts de Merlín
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/components/merlin/js/scripts.js');
	wp_enqueue_script( 'simple-parallax', get_template_directory_uri() . '/components/merlin/js/simpleParallax.min.js');
	wp_enqueue_script( 'modal', get_template_directory_uri() . '/components/merlin/js/modal.js');
	wp_enqueue_script( 'toggle', get_template_directory_uri() . '/components/merlin/js/toggle.js');
	wp_enqueue_script( 'header', get_template_directory_uri() . '/components/merlin/js/header.js');
	wp_enqueue_script( 'animacion-anclas', get_template_directory_uri() . '/components/merlin/js/animacion-anclas.js');

	//Scripts de Organismos: Sliders
	wp_enqueue_script( 'slider-1', get_template_directory_uri() . '/organisms/sliders/01/js/slider-1.js');
	wp_enqueue_script( 'slider-2', get_template_directory_uri() . '/organisms/sliders/02/js/slider-2.js');
	wp_enqueue_script( 'slider-4', get_template_directory_uri() . '/organisms/sliders/04/js/slider-4.js');
	wp_enqueue_script( 'slider-5', get_template_directory_uri() . '/organisms/sliders/05/js/slider-5.js');
	wp_enqueue_script( 'slider-6', get_template_directory_uri() . '/organisms/sliders/06/js/slider-6.js');
	wp_enqueue_script( 'slider-7', get_template_directory_uri() . '/organisms/sliders/07/js/slider-7.js');

	//Scripts de Organismos: Bloques
		//Bloque #3
		wp_enqueue_script( 'count-up', get_template_directory_uri() . '/organisms/blocks/03/js/count-up.js');
		wp_enqueue_script( 'in-view', get_template_directory_uri() . '/organisms/blocks/03/js/in-view.js');
		wp_enqueue_script( 'block-3', get_template_directory_uri() . '/organisms/blocks/03/js/block-3.js');
		//Bloque #5
		wp_enqueue_script( 'block-5', get_template_directory_uri() . '/organisms/blocks/05/js/block-5.js');
		wp_enqueue_script( 'site-scripts', get_template_directory_uri() . '/components/custom/js/scripts.js');


}

add_action( 'wp_enqueue_scripts', 'merlin_styles_and_scripts' );

// Register Custom Post Type marcas
function custom_post_type_marcas_cl() {

	$labels = array(
		'name'                  => _x( 'Marcas CL', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas CL', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas CL', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas CL', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Chile', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_cl', $args );

}
add_action( 'init', 'custom_post_type_marcas_cl', 0 );

function custom_post_type_marcas_ar() {

	$labels = array(
		'name'                  => _x( 'Marcas AR', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas AR', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas AR', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas AR', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Argentina', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_ar', $args );

}
add_action( 'init', 'custom_post_type_marcas_ar', 0 );

function custom_post_type_marcas_bo() {

	$labels = array(
		'name'                  => _x( 'Marcas BO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas BO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas BO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas BO', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Bolivia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_bo', $args );

}
add_action( 'init', 'custom_post_type_marcas_bo', 0 );

function custom_post_type_marcas_co() {

	$labels = array(
		'name'                  => _x( 'Marcas CO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas CO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas CO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas CO', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Colmbia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_co', $args );

}
add_action( 'init', 'custom_post_type_marcas_co', 0 );

function custom_post_type_marcas_py() {

	$labels = array(
		'name'                  => _x( 'Marcas PY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas PY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas PY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas PY', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Paraguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_py', $args );

}
add_action( 'init', 'custom_post_type_marcas_py', 0 );

function custom_post_type_marcas_uy() {

	$labels = array(
		'name'                  => _x( 'Marcas UY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Marca', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Marcas UY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Marcas UY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Marcas UY', 'ccu-intranet' ),
		'description'           => __( 'Marcas de CCU Uruguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-beer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'marcas_uy', $args );

}
add_action( 'init', 'custom_post_type_marcas_uy', 0 );

// Asignar single-marca.php a todas las marcas por país
add_filter( 'template_include', function( $template ) {
	if ( is_singular( array( 'marcas_cl', 'marcas_ar', 'marcas_bo', 'marcas_co', 'marcas_py', 'marcas_uy' ) ) ) {
	  $locate = locate_template( 'single-marcas.php', false, false );
	  if ( ! empty( $locate ) ) {
		$template = $locate;
	  }
	}
	return $template;
  });

// Register Custom Noticias por país

function custom_post_type_noticias_cl() {

	$labels = array(
		'name'                  => _x( 'Noticias CL', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias CL', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias CL', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias CL', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Chile', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_cl', $args );

}
add_action( 'init', 'custom_post_type_noticias_cl', 0 );

function custom_post_type_noticias_ar() {

	$labels = array(
		'name'                  => _x( 'Noticias AR', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias AR', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias AR', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias AR', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Argentina', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_ar', $args );

}
add_action( 'init', 'custom_post_type_noticias_ar', 0 );

function custom_post_type_noticias_bo() {

	$labels = array(
		'name'                  => _x( 'Noticias BO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias BO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias BO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias BO', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Bolivia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_bo', $args );

}
add_action( 'init', 'custom_post_type_noticias_bo', 0 );

function custom_post_type_noticias_co() {

	$labels = array(
		'name'                  => _x( 'Noticias CO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias CO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias CO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias CO', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Colmbia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_co', $args );

}
add_action( 'init', 'custom_post_type_noticias_co', 0 );

function custom_post_type_noticias_py() {

	$labels = array(
		'name'                  => _x( 'Noticias PY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias PY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias PY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias PY', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Paraguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_py', $args );

}
add_action( 'init', 'custom_post_type_noticias_py', 0 );

function custom_post_type_noticias_uy() {

	$labels = array(
		'name'                  => _x( 'Noticias UY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Noticia', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Noticias UY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Noticias UY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Noticias UY', 'ccu-intranet' ),
		'description'           => __( 'Noticias de CCU Uruguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'noticias_uy', $args );

}
add_action( 'init', 'custom_post_type_noticias_uy', 0 );

// Register Custom Page Type
function custom_page_type_cl() {

	$labels = array(
		'name'                  => _x( 'Páginas CL', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas CL', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas CL', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Páginas', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Chile', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'chile'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_cl', $args );

}
add_action( 'init', 'custom_page_type_cl', 0 );

function custom_page_type_ar() {

	$labels = array(
		'name'                  => _x( 'Páginas AR', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas AR', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas AR', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Página', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Argentina', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'argentina'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_ar', $args );

}
add_action( 'init', 'custom_page_type_ar', 0 );


function custom_page_type_bo() {

	$labels = array(
		'name'                  => _x( 'Páginas BO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas BO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas BO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Página', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Bolivia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'bolivia'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_bo', $args );

}
add_action( 'init', 'custom_page_type_bo', 0 );

function custom_page_type_co() {

	$labels = array(
		'name'                  => _x( 'Páginas CO', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas CO', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas CO', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Página', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Colombia', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'colombia'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_co', $args );
	
}
add_action( 'init', 'custom_page_type_co', 0 );

function custom_page_type_py() {

	$labels = array(
		'name'                  => _x( 'Páginas PY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas PY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas PY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Página', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Paraguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'paraguay'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_py', $args );

}
add_action( 'init', 'custom_page_type_py', 0 );

function custom_page_type_uy() {

	$labels = array(
		'name'                  => _x( 'Páginas UY', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Página', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Páginas UY', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Páginas UY', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Página', 'ccu-intranet' ),
		'description'           => __( 'Páginas CCU Uruguay', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'query_var'             => 'page_type',
		'rewrite'               => array('slug' => 'uruguay'),
		'capability_type'       => 'page',
	);
	register_post_type( 'pagina_uy', $args );

}
add_action( 'init', 'custom_page_type_uy', 0 );

function custom_post_type_videos_cl() {

	$labels = array(
		'name'                  => _x( 'Videos CL', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Video', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Videos CL', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Videos CL', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Videos CL', 'ccu-intranet' ),
		'description'           => __( 'Videos de CCU Chile', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-video-alt2',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'videos_cl', $args );

}
add_action( 'init', 'custom_post_type_videos_cl', 0 );

// Asignar single-marca.php a todas las marcas por país
add_filter( 'template_include', function( $template ) {
	if ( is_singular( array( 'videos_cl') ) ) {
	  $locate = locate_template( 'single-videos.php', false, false );
	  if ( ! empty( $locate ) ) {
		$template = $locate;
	  }
	}
	return $template;
  });

function custom_post_type_cambios() {

	$labels = array(
		'name'                  => _x( 'Cambios', 'Post Type General Name', 'ccu-intranet' ),
		'singular_name'         => _x( 'Cambio', 'Post Type Singular Name', 'ccu-intranet' ),
		'menu_name'             => __( 'Cambios', 'ccu-intranet' ),
		'name_admin_bar'        => __( 'Cambios', 'ccu-intranet' ),
		'archives'              => __( 'Archivo', 'ccu-intranet' ),
		'attributes'            => __( 'Atributos', 'ccu-intranet' ),
		'parent_item_colon'     => __( 'Item Relacionado:', 'ccu-intranet' ),
		'all_items'             => __( 'Todos', 'ccu-intranet' ),
		'add_new_item'          => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'new_item'              => __( 'Nuevo', 'ccu-intranet' ),
		'edit_item'             => __( 'Editar', 'ccu-intranet' ),
		'update_item'           => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'             => __( 'Ver', 'ccu-intranet' ),
		'view_items'            => __( 'Ver', 'ccu-intranet' ),
		'search_items'          => __( 'Buscar', 'ccu-intranet' ),
		'not_found'             => __( 'No encontrado', 'ccu-intranet' ),
		'not_found_in_trash'    => __( 'No encontrado en la papelera', 'ccu-intranet' ),
		'featured_image'        => __( 'Imagen Destacada', 'ccu-intranet' ),
		'set_featured_image'    => __( 'Elegir imagen destacada', 'ccu-intranet' ),
		'remove_featured_image' => __( 'Remove featured image', 'ccu-intranet' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'ccu-intranet' ),
		'insert_into_item'      => __( 'Insertar', 'ccu-intranet' ),
		'uploaded_to_this_item' => __( 'Cargar', 'ccu-intranet' ),
		'items_list'            => __( 'Lista de Items', 'ccu-intranet' ),
		'items_list_navigation' => __( 'Items list navigation', 'ccu-intranet' ),
		'filter_items_list'     => __( 'Filtrar lista', 'ccu-intranet' ),
	);
	$args = array(
		'label'                 => __( 'Cambios organizacionales', 'ccu-intranet' ),
		'description'           => __( 'Cambios organizacionales', 'ccu-intranet' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'cambios', $args );

}
add_action( 'init', 'custom_post_type_cambios', 0 );

// Register Custom Taxonomy
function marca_taxonomy_pais() {

	$labels = array(
		'name'                       => _x( 'Países', 'Taxonomy General Name', 'ccu-intranet' ),
		'singular_name'              => _x( 'País', 'Taxonomy Singular Name', 'ccu-intranet' ),
		'menu_name'                  => __( 'Países', 'ccu-intranet' ),
		'all_items'                  => __( 'Todos', 'ccu-intranet' ),
		'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
		'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
		'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
		'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'                  => __( 'Ver', 'ccu-intranet' ),
		'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
		'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
		'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
		'popular_items'              => __( 'Populares', 'ccu-intranet' ),
		'search_items'               => __( 'Buscar', 'ccu-intranet' ),
		'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
		'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
		'items_list'                 => __( 'Lista', 'ccu-intranet' ),
		'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'pais', array( 'marcas_cl', ' marcas_ar', ' marcas_bo', ' marcas_co', ' marcas_py', ' marcas_uy' ), $args );

}
add_action( 'init', 'marca_taxonomy_pais', 0 );

// Register Custom Taxonomy
function marca_taxonomy_tipo() {

	$labels = array(
		'name'                       => _x( 'Tipos', 'Taxonomy General Name', 'ccu-intranet' ),
		'singular_name'              => _x( 'tipo', 'Taxonomy Singular Name', 'ccu-intranet' ),
		'menu_name'                  => __( 'Tipos', 'ccu-intranet' ),
		'all_items'                  => __( 'Todos', 'ccu-intranet' ),
		'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
		'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
		'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
		'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'                  => __( 'Ver', 'ccu-intranet' ),
		'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
		'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
		'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
		'popular_items'              => __( 'Populares', 'ccu-intranet' ),
		'search_items'               => __( 'Buscar', 'ccu-intranet' ),
		'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
		'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
		'items_list'                 => __( 'Lista', 'ccu-intranet' ),
		'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'tipo', array( 'marcas_cl', ' marcas_ar', ' marcas_bo', ' marcas_co', ' marcas_py', ' marcas_uy' ), $args );

}
add_action( 'init', 'marca_taxonomy_tipo', 0 );

// Register Custom Taxonomy
function video_taxonomy_cat() {

	$labels = array(
		'name'                       => _x( 'Categorías', 'Taxonomy General Name', 'ccu-intranet' ),
		'singular_name'              => _x( 'Categoría', 'Taxonomy Singular Name', 'ccu-intranet' ),
		'menu_name'                  => __( 'Categorías', 'ccu-intranet' ),
		'all_items'                  => __( 'Todos', 'ccu-intranet' ),
		'parent_item'                => __( 'Item Relacionado', 'ccu-intranet' ),
		'parent_item_colon'          => __( 'Item Relacionado:', 'ccu-intranet' ),
		'new_item_name'              => __( 'Añadir nuevo', 'ccu-intranet' ),
		'add_new_item'               => __( 'Añadir nuevo', 'ccu-intranet' ),
		'edit_item'                  => __( 'Editar', 'ccu-intranet' ),
		'update_item'                => __( 'Actualizar', 'ccu-intranet' ),
		'view_item'                  => __( 'Ver', 'ccu-intranet' ),
		'separate_items_with_commas' => __( 'Separar con coma', 'ccu-intranet' ),
		'add_or_remove_items'        => __( 'Añadir o Eliminar', 'ccu-intranet' ),
		'choose_from_most_used'      => __( 'Elegir de los más usados', 'ccu-intranet' ),
		'popular_items'              => __( 'Populares', 'ccu-intranet' ),
		'search_items'               => __( 'Buscar', 'ccu-intranet' ),
		'not_found'                  => __( 'No encontrado', 'ccu-intranet' ),
		'no_terms'                   => __( 'No hay', 'ccu-intranet' ),
		'items_list'                 => __( 'Lista', 'ccu-intranet' ),
		'items_list_navigation'      => __( 'Lista de navegación', 'ccu-intranet' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'cat_video', array( 'videos_cl', ' videos_ar', ' videos_bo', ' videos_co', ' videos_py', ' videos_uy' ), $args );

}
add_action( 'init', 'video_taxonomy_cat', 0 );

function merlin_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
        //  if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>Anterior</a>";
        if($paged > 1 && $showitems < $pages) { echo "<a href='".get_pagenum_link($paged - 1)."' class='arrow-page'><i class='icon-chevron-left'></i></a>"; } else {
			echo "<span class='no-link-page at-first-page'><i class='icon-chevron-left'></i></span>";
		}
		echo "<div class='num-pages'>";
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='num current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='num inactive' >".$i."</a>";
             }
         }
		 echo "</div>\n";
		 if ($paged < $pages && $showitems < $pages) { echo "<a href='".get_pagenum_link($paged + 1)."' class='arrow-page'><i class='icon-chevron-right'></i></a>"; }
		 else {
			echo "<span class='no-link-page at-last-page'><i class='icon-chevron-right'></i></span>";
		 } 
        //  if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}
add_action('init', 'merlin_pagination');

add_filter( 'comment_form_fields', 'move_comment_field' );
function move_comment_field( $fields ) {
    $comment_field = $fields['comment'];
    $author_field = $fields['author'];
    $email_field = $fields['email'];
    $url_field = $fields['url'];
    $cookies_field = $fields['cookies'];
    unset( $fields['comment'] );
    unset( $fields['author'] );
    unset( $fields['email'] );
    unset( $fields['url'] );
    unset( $fields['cookies'] );
    $fields['author'] = $author_field;
    $fields['email'] = $email_field;
    $fields['url'] = $url_field;
    $fields['comment'] = $comment_field;
    $fields['cookies'] = $cookies_field;
    return $fields;
}

function psot_comment_form_avatar()
{
  ?>
<div class="comment-avatar">
    <?php 
     $current_user = wp_get_current_user();
     if ( ($current_user instanceof WP_User) ) {
        echo get_avatar( $current_user->user_email, 32 );
     }
     ?>
</div>
<?php
}

if ( is_user_logged_in() ) {
	add_action( 'comment_form_logged_in', 'psot_comment_form_avatar' );
	add_action( 'comment_form_before_fields', 'psot_comment_form_avatar' );
}

function my_comment_time_ago_function() {
	return sprintf( esc_html__( 'Hace %s', 'textdomain' ), human_time_diff(get_comment_time ( 'U' ), current_time( 'timestamp' ) ) );
	}

add_filter( 'get_comment_date', 'my_comment_time_ago_function' );

function ccu_type_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    <div id="comment-<?php comment_ID(); ?>" class="comment-block">
        <div class="profile-img-area">
            <div class="profile-img">
                <?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>
            </div>
        </div>
        <div class="comment-area">
            <div class="comment-author vcard">
                <?php printf(__('<cite class="fn">%s</cite>'), get_comment_author_link()); ?>
            </div>
            <div class="comment-meta commentmetadata">
                <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
                    <?php printf(__('%1$s'), get_comment_date()); ?>
                </a><?php edit_comment_link(__('(Edit)'),'  ','') ?>
            </div>
        </div>
        <div class="comment-content-area">
            <?php if ($comment->comment_approved == '0') : ?>
            <em><?php _e('Tu comentario espera aprobación.') ?></em>
            <br />
            <?php endif; ?>
            <?php comment_text() ?>

            <div class="reply">
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
        </div>
    </div>
</li>
<?php
}

add_action('wp_enqueue_scripts', 'cambios_insertar_js');

function cambios_insertar_js(){

	wp_register_script('cambios_script', get_template_directory_uri(). '/components/custom/js/cambios.js', array('jquery'), '1', true );
	wp_enqueue_script('cambios_script');

	wp_localize_script('cambios_script','cambios_vars',['ajaxurl'=>admin_url('admin-ajax.php')]);
}

add_action('wp_ajax_nopriv_cambios_ajax_readmore','cambios_enviar_contenido');
add_action('wp_ajax_cambios_ajax_readmore','cambios_enviar_contenido');

function cambios_enviar_contenido()
{

	$pid        = intval($_POST['id_post']);
    $the_query  = new WP_Query(array('post_type' => 'cambios', 'p' => $pid));

    if ($the_query->have_posts()) {
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $data = get_template_part('components/template-parts/cambio');

        }
    } 
    else {
        echo '<div id="postdata">'.__('No hay contenido', THEME_NAME).'</div>';
    }
    wp_reset_postdata();

	echo '<div id="postdata">'.$data.'</div>';
	die();
}

?>
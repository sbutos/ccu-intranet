<?php get_header() ?>
<?php
while ( have_posts() ) : the_post(); ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_caja_producto' ); ?></h3>
            </div>
        </div>
        <div class="page-heading single-marca-heading">
            <?php $imagen_fondo = get_field( 'imagen_fondo' ); ?>
            <div class="bg-image cover" style="background-image: url(<?php echo $imagen_fondo['url']; ?>)"
                title="<?php echo $imagen_fondo['alt']; ?>">
                <div class="veil"></div>
            </div>
            <div class="content-area">
                <?php $logo = get_field( 'logo' ); ?>
                <?php if ( $logo ) { ?>
                <div class="content">
                    <div class="marca-logo">
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>">
                    </div>
                </div>
                <?php } ?>
                <div class="content">
                    <div class="marca-info">
                        <p class="descripcion"><?php the_field( 'intro_marca' ); ?></p>
                        <div class="marca-rrss">
                            <?php $sitio_web_m = get_field( 'sitio_web_m' ); ?>
                            <?php if ( $sitio_web_m ) { ?>
                            <a href="<?php echo $sitio_web_m['url']; ?>" target="<?php echo $sitio_web_m['target']; ?>"
                                class="link web-type"><i
                                    class="icon-share2"></i><span><?php echo $sitio_web_m['title']; ?></span></a>
                            <?php } ?>
                            <?php $facebook_m = get_field( 'facebook_m' ); ?>
                            <?php if ( $facebook_m ) { ?>
                            <a href="<?php echo $facebook_m['url']; ?>" target="<?php echo $facebook_m['target']; ?>"
                                class="link social-type"><i
                                    class="icon-facebook-square"></i><span><?php echo $facebook_m['title']; ?></span></a>
                            <?php } ?>
                            <?php $twitter_m = get_field( 'twitter_m' ); ?>
                            <?php if ( $twitter_m ) { ?>
                            <a href="<?php echo $twitter_m['url']; ?>" target="<?php echo $twitter_m['target']; ?>"
                                class="link social-type"><i
                                    class="icon-twitter-square"></i><span><?php echo $twitter_m['title']; ?></span></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="wrap-xl">
        <div class="flex justify-between align-start">
            <?php $videos_rel = get_field( 'videos_rel' ); ?>
            <?php if ( $videos_rel ): ?>
            <div class="col-66">
                <div class="content">
                    <div class="heading-box-area">
                        <h3 class="head-title">Videos</h3>
                    </div>
                    <div class="videos-slider-area border-radius-m">
                        <div id="video-arrows" class="arrows">
                            <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                            <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
                        </div>
                        <div id="videos-slider">
                            <?php foreach ( $videos_rel as $post ):  ?>
                            <?php setup_postdata ( $post ); 
                            $newsThumbImg = get_the_post_thumbnail_url();
                            $newsThumbnailID = get_post_thumbnail_ID();
                            $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                            ?>
                            <div class="slide">
                                <a href="#" class="video-box">
                                    <div class="photo cover"
                                        style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                        title="<?php echo $alt; ?>">
                                        <div class="veil"></div>
                                    </div>
                                    <div class="video-content">
                                        <h4 class="video-name"><?php the_title(); ?></h4>
                                    </div>
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                </a>
                            </div>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php $iframe_rrss = get_field( 'redes_sociales_m' ); ?>
            <?php if($iframe_rrss) { ?>
            <div class="col-32">
                <div class="twitter-container">
                    <div class="content">
                        <div class="heading-box-area">
                            <h3 class="head-title">Redes Sociales</h3>
                        </div>
                        <?php echo $iframe_rrss; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php if ( have_rows( 'producto_m' ) ) : ?>
<section class="section">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title">Productos de <?php the_title(); ?></h3>
            </div>
            <div class="all-productos-marca">
                <?php while ( have_rows( 'producto_m' ) ) : the_row(); ?>
                <div class="producto-area">
                    <div class="producto-data">
                        <div class="producto-info">
                            <h5 class="producto-name">
                                <?php the_sub_field( 'nombre_p_m' ); ?>
                            </h5>
                            <div class="producto-desc">
                                <?php the_sub_field( 'intro_p_m' ); ?>
                            </div>
                        </div>
                        <?php if ( have_rows( 'caracteristicas' ) ) : ?>
                        <div class="producto-specs-area">
                            <h6 class="titulo">Características</h6>
                            <div class="producto-specs">
                                <?php while ( have_rows( 'caracteristicas' ) ) : the_row(); ?>
                                <div class="producto-specs-box">
                                    <?php $field = get_sub_field_object( 'tipo_caracterisitica' );
                                    $value = $field['value'];
                                    $label = $field['choices'][ $value ]; ?>
                                    <div class="icono">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/specs/<?php echo $value; ?>.png"
                                            alt="">
                                    </div>
                                    <div class="tipo-box">
                                        <span class="tipo"><?php echo $label; ?></span>
                                        <span class="desc"><?php the_sub_field( 'valor_caracteristica' ); ?></span>
                                    </div>
                                </div>
                                <?php endwhile; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php if ( have_rows( 'formatos' ) ) : ?>
                    <div class="producto-slider-area">
                        <div class="producto-formato-slider">
                            <?php while ( have_rows( 'formatos' ) ) : the_row(); ?>
                            <div class="slide">
                                <div class="producto-formato">
                                    <?php $imagen_formato = get_sub_field( 'imagen_formato' ); ?>
                                    <div class="producto-imagen">
                                        <img src="<?php echo $imagen_formato['url']; ?>"
                                            alt="<?php echo $imagen_formato['alt']; ?>">
                                    </div>
                                    <?php $desc_formato = get_sub_field( 'desc_formato' ); ?>
                                    <?php if($desc_formato) { ?>
                                    <div class="producto-nombre">
                                        <h5><?php echo $desc_formato; ?></h5>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php endwhile; ?>
<script>
$(document).ready(function() {
    $('#videos-slider').slick({
        arrows: false,
        dots: false,
        speed: 750
    });
    $('#video-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#videos-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#videos-slider').slick('slickNext');
            });
        }
    });
    $('.producto-formato-slider').each(function(index, element) {
        $(element).slick({
            arrows: false,
            dots: true,
            speed: 750
        });
    });
});
</script>
<?php get_footer() ?>
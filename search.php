<?php
/*
Template Name: Resultados de búsqueda
*/
?>

<?php get_header(); ?>
<?php

$searchedCat = $_POST['searched_cat'];

$categories = get_categories();
if($categories){ ?>
<section class="section categories-container">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title">
                    Recomendados
                </h3>
            </div>
            <div class="categories-area">
                <?php foreach($categories as $category) {
                    echo '<a href="#" class="btn is-verde is-rounded is-bordered cat-trigger" data-cat="'.$category->slug.'">' . $category->name . '</a>';
                } ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php if ( have_posts() ) : ?>
<section class="section repositorio-area">
    <div class="wrap-xl">
        <div class="news-area layout-one-third">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title">
                        <?php if (!empty($searchedCat)) { ?>
                        Resultados de búsqueda para: <?php echo $searchedCat ?>
                        <?php } else { ?>
                        <?php printf( __( 'Resultados de búsqueda para: %s', 'base' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?>
                        <?php } ?>
                    </h3>
                </div>

                <div class="layout-news-area">
                    <?php
	                while ( have_posts() ) : the_post();
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="small-news-area border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="post-cat-area">
                                <?php
                            $categories = get_the_category();
                            $comma      = ' ';
                            $output     = '';
                            
                            if ( $categories ) {
                                foreach ( $categories as $category ) {
                                    $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                }
                                echo trim( $output, $comma );
                            } ?>
                            </div>
                            <div class="content-area">
                                <div class="post-info">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                                <div class="button-area">
                                    <a href="<?php the_permalink(); ?>"
                                        class="btn is-verde is-rounded size-s"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    endwhile;
                    // Previous/next page navigation.
                // If no content, include the "No posts found" template.
                    else :
                    ?>
                </div>
                <div class="pagination-area">
                    <?php merlin_pagination($query_repo->max_num_pages, 3); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php _e('No hemos encontrado nada :-(', 'base'); ?>
<?php
		endif;
		?>
<script>
$(document).ready(function() {
    $('.cat-trigger').each(function(index, element) {
        $(this).click(function(e) {
            e.preventDefault();
            let thisCat = $(this).data('cat');
            let thisCatText = $(this).text();
            let permalink = '<?php echo site_url('/') ?>' + '?s=&category_name=' + thisCat;

            $("<form method='POST' action='" + permalink +
                "'><input type='hidden' name='searched_cat' value='" + thisCatText +
                "'/></form>").appendTo("body").submit();
        });
    });
});
</script>
<?php get_footer(); ?>
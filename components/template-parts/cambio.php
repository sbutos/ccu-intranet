<?php
// Pasar ACF de texto a Variables fijas 
$titlePestanaMovimientos = get_field('titulo_pestana_mi');
$titlePestanaNuevos = get_field('titulo_pestana_ni');
$titlePestanaGerentes = get_field('titulo_pestana_g');

function sanitizar($string) {
    $new_url = sanitize_title($string);
    echo $new_url;
}
?>
<div id="cambios-tabs">
    <ul class="tabs-triggers">
        <li><a href="#<?php sanitizar($titlePestanaMovimientos); ?>"><?php echo $titlePestanaMovimientos; ?></a></li>
        <li><a href="#<?php sanitizar($titlePestanaNuevos); ?>"><?php echo $titlePestanaNuevos; ?></a></li>
        <li><a href="#<?php sanitizar($titlePestanaGerentes); ?>"><?php echo $titlePestanaGerentes; ?></a></li>
    </ul>
    <div id="<?php sanitizar($titlePestanaMovimientos); ?>" class="tab-content">
        <?php if ( have_rows( 'personas_mi' ) ) : ?>
        <div class="grid-column-2 gap-m">
            <?php while ( have_rows( 'personas_mi' ) ) : the_row(); ?>
            <div class="persona-caja">
                <div class="content-persona">
                    <?php $foto_persona_ci = get_sub_field( 'foto_persona_ci' ); ?>
                    <?php if ( $foto_persona_ci ) { ?>
                    <div class="foto-persona">
                        <img src="<?php echo $foto_persona_ci['url']; ?>"
                            alt="<?php echo $foto_persona_ci['alt']; ?>" />
                    </div>
                    <?php } ?>
                    <div class="info-persona">
                        <h5 class="nombre">
                            <?php the_sub_field( 'nombre_persona_ci' ); ?>
                        </h5>
                        <p class="cargo">
                            <?php the_sub_field( 'cargo_persona_ci' ); ?>
                        </p>
                        <p class="data">
                            <?php the_sub_field( 'descripcion_persona_ci' ); ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php else : ?>
        No hay cambios
        <?php endif; ?>
    </div>
    <div id="<?php sanitizar($titlePestanaNuevos); ?>" class="tab-content">
        <?php if ( have_rows( 'personas_ni' ) ) : ?>
        <div class="grid-column-2 gap-m">
            <?php while ( have_rows( 'personas_ni' ) ) : the_row(); ?>
            <div class="persona-caja">
                <div class="content-persona">
                    <?php $foto_persona_ni = get_sub_field( 'foto_persona_ni' ); ?>
                    <?php if ( $foto_persona_ni ) { ?>
                    <div class="foto-persona">
                        <img src="<?php echo $foto_persona_ni['url']; ?>"
                            alt="<?php echo $foto_persona_ni['alt']; ?>" />
                    </div>
                    <?php } ?>
                    <div class="info-persona">
                        <h5 class="nombre">
                            <?php the_sub_field( 'nombre_persona_ni' ); ?>
                        </h5>
                        <p class="cargo">
                            <?php the_sub_field( 'cargo_persona_ni' ); ?>
                        </p>
                        <p class="data">
                            <?php the_sub_field( 'descripcion_persona_ni' ); ?>
                        </p>
                    </div>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php else : ?>
        No hay cambios
        <?php endif; ?>
    </div>
    <div id="<?php sanitizar($titlePestanaGerentes); ?>" class="tab-content">
        <?php if ( have_rows( 'personas_g' ) ) : ?>
        <div class="slider-area">
            <div id="gerentes-arrows" class="arrows">
                <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
            </div>
            <div id="slider-gerentes">
                <?php while ( have_rows( 'personas_g' ) ) : the_row(); ?>
                <div class="slide">
                    <div class="content">
                        <?php $foto_persona_g = get_sub_field( 'foto_persona_g' ); ?>
                        <?php if ( $foto_persona_g ) { ?>
                        <div class="foto-gerente">
                            <img src="<?php echo $foto_persona_g['url']; ?>"
                                alt="<?php echo $foto_persona_g['alt']; ?>" />
                        </div>
                        <?php } ?>
                        <div class="info-gerente">
                            <h5 class="nombre"><?php the_sub_field( 'nombre_persona_g' ); ?></h5>
                            <h6 class="cargo"><?php the_sub_field( 'cargo_persona_g' ); ?></h6>
                            <p class="evento"><?php the_sub_field( 'evento_persona_g' ); ?></p>
                            <p class="data"><?php the_sub_field( 'descripcion_persona_ni' ); ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <?php else : ?>
        No hay cambios
        <?php endif; ?>
    </div>
</div>
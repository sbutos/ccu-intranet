function timeDisplay(zone, divSet) {
    var currentTime = new Date();
    currentTime.setHours(currentTime.getHours() + zone);
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridiem = " ";

    if (hours >= 12) {
        hours = hours - 12;
        meridiem = "pm";
    } else {
        meridiem = "am";
    }
    if (hours === 0) {
        hours = 12;
    }
    if (hours < 10) {
        hours = "0" + hours;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }

    var clockDiv = document.getElementById(divSet);
    clockDiv.innerText = hours + ":" + minutes + ":" + seconds + " " + meridiem;
}

function dateDisplay(divSet) {
    var currentDate = new Date();
    var meses = [
        "enero",
        "febrero",
        "marzo",
        "abril",
        "mayo",
        "junio",
        "julio",
        "agosto",
        "septiembre",
        "octubre",
        "noviembre",
        "diciembre",
    ];
    var diasemana = [
        "Domingo",
        "Lunes",
        "Martes",
        "Miércoles",
        "Jueves",
        "Viernes",
        "Sábado",
    ];
    var weekday = diasemana[currentDate.getDay()];
    var day = currentDate.getDate();
    var month = meses[currentDate.getMonth()];
    var year = currentDate.getFullYear();
    var dateDiv = document.getElementById(divSet);
    dateDiv.innerText = weekday + ", " + day + " de " + month + " de " + year;
}
$(document).ready(function () {
    dateDisplay("current-date");
    $("#time-slider").slick({
        autoplay: true,
        arrows: false,
        infinite: false,
        speed: 500,
        slidesToShow: 1,
    });

    $("#time-area .prev").click(function (e) {
        e.preventDefault();
        $("#time-slider").slick("slickPrev");
    });
    $("#time-area .next").click(function (e) {
        e.preventDefault();
        $("#time-slider").slick("slickNext");
    });

    setInterval(function () {
        $("#time-slider .slide").each(function (index, element) {
            let timeZone = $(this).children(".time-data").data("timezone");
            let countryName = $(this)
                .children(".time-data")
                .children(".time")
                .attr("id");
            timeDisplay(timeZone, countryName);
        });
    }, 1000);

    $('.user-area').click(function (e) { 
        e.preventDefault();
        
        $('.user-menu').toggleClass('active-menu');
        $(this).toggleClass('active-menu');
    });
});

(function($){

	$(document).on('change','#post-date-selector',function(e){
        e.preventDefault();
         
        var id = this.value;
        
        console.log(id);

		$.ajax({
			url : cambios_vars.ajaxurl,
			type: 'post',
			data: {
				action : 'cambios_ajax_readmore',
				id_post: id
			},
			beforeSend: function(){
				$('#post-cambios').html('Cargando ...');
			},
			success: function(resultado){
                 $('#post-cambios').html(resultado);		
			}

		});

	});

})(jQuery);
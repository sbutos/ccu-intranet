<?php
/*
* Template Name: Repositorio Videos
*/
$catchTaxonomia = $_POST['thistax'];
get_header(); ?>

<?php
// WP_Query arguments
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array(
	'post_type'         => array( 'videos_cl' ),
    'posts_per_page'    => '9',
    'paged'             => $paged
);

if(!empty($catchTaxonomia)){
    $taxFilter = array(
        array(
            'taxonomy' => 'cat_video',
            'field'    => 'slug',
            'terms'    => $catchTaxonomia,
            'operator' => 'IN'
        )
    );
    $args['tax_query'] = $taxFilter;
}

// The Query
$query_repo = new WP_Query( $args );
 
if ( $query_repo->have_posts() ) { ?>
<section class="section repositorio-area">
    <div class="wrap-xl">
        <div class="news-area layout-one-third">
            <div class="content">
                <div class="heading-box-area">
                    <?php if(!empty($catchTaxonomia)){ ?>
                    <?php $term = get_term_by('slug', $catchTaxonomia, 'category'); $name = $term->name; ?>
                    <h3 class="head-title">
                        Todas los videos de <?php echo $name; ?>
                    </h3>
                    <?php } else { ?>
                    <h3 class="head-title">
                        Todas los videos
                    </h3>
                    <?php } ?>
                </div>

                <div class="layout-news-area">
                    <?php
	                while ( $query_repo->have_posts() ) { $query_repo->the_post(); 
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                    <div class="small-news-area border-radius-m">
                        <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                            title="<?php echo $alt; ?>">
                            <div class="veil"></div>
                        </div>
                        <div class="content">
                            <div class="post-cat-area">
                                <?php
                            $categories = get_the_category();
                            $comma      = ' ';
                            $output     = '';
                            
                            if ( $categories ) {
                                foreach ( $categories as $category ) {
                                    $output .= '<span>#' . $category->cat_name . '</span>' . $comma;
                                }
                                echo trim( $output, $comma );
                            } ?>
                            </div>
                            <div class="content-area">
                                <div class="post-info">
                                    <span class="fecha"><?php the_date(); ?></span>
                                    <h3 class="post-title">
                                        <?php the_title(); ?>
                                    </h3>
                                </div>
                                <div class="button-area">
                                    <a href="<?php the_permalink(); ?>"
                                        class="btn is-verde is-rounded size-s"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="pagination-area">
                    <?php merlin_pagination($query_repo->max_num_pages, 3); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } else { ?>
<section class="section repositorio-area">
    <div class="wrap-xl">
        <p>No hay videos para mostrar.</p>
    </div>
</section>
<?php } 
            
    wp_reset_postdata();
?>

<?php get_footer(); ?>
<?php
/*
*
* Template Name: Al Día CCU
*
*/
?>
<?php get_header(); ?>
<section class="section noticias-area">
    <div class="wrap-xl">
        <div class="noticias-slide-area">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_field( 'titulo_aldia' ); ?></h3>
                    <?php $link_todas_aldia = get_field( 'link_todas_aldia' ); ?>
                    <?php if ( $link_todas_aldia ) { ?>
                    <a href="<?php echo $link_todas_aldia['url']; ?>"
                        target="<?php echo $link_todas_aldia['target']; ?>"
                        class="btn-ver-todas"><span><?php echo $link_todas_aldia['title']; ?></span><i
                            class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>

                <div class="slider-area">
                    <?php $noticias_aldia = get_field( 'noticias_aldia' ); ?>
                    <?php if ( $noticias_aldia ): ?>
                    <div id="noticias-arrows" class="arrows">
                        <a href="#" class="arrow prev"><i class="icon-chevron-left"></i></a>
                        <a href="#" class="arrow next"><i class="icon-chevron-right"></i></a>
                    </div>
                    <div id="noticias-slider">
                        <?php foreach ( $noticias_aldia as $post ):  ?>
                        <?php setup_postdata ( $post );
                    $newsThumbImg = get_the_post_thumbnail_url();
                    $newsThumbnailID = get_post_thumbnail_ID();
                    $alt = get_post_meta ( $newsThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                        <div class="slide">
                            <div class="small-news-area border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $newsThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="post-cat-area">
                                        <?php $category_detail=get_the_category($post->ID);//$post->ID
                            foreach($category_detail as $cd){
                            echo '<span>'.$cd->cat_name.'</span>';
                            } ?>
                                    </div>
                                    <div class="content-area">
                                        <div class="post-info">
                                            <span class="fecha"><?php the_date(); ?></span>
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                        <div class="button-area">
                                            <a href="<?php the_permalink(); ?>"
                                                class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section video-gal-area">
    <div class="wrap-xl">
        <div class="videos-slide-area">
            <div class="content">
                <div class="heading-box-area">
                    <h3 class="head-title"><?php the_field( 'titulo_videos' ); ?></h3>
                    <?php $link_todas_videos = get_field( 'link_todas_videos' ); ?>
                    <?php if ( $link_todas_videos ) { ?>
                    <a href="<?php echo $link_todas_videos['url']; ?>"
                        target="<?php echo $link_todas_videos['target']; ?>"
                        class="btn-ver-todas"><span><?php echo $link_todas_videos['title']; ?></span><i
                            class="icon-chevron-right"></i></a>
                    <?php } ?>
                </div>

                <div class="slider-area">
                    <?php $videos_gal = get_field( 'videos_gal' ); ?>
                    <?php if ( $videos_gal ): $m = 1; ?>
                    <a href="#" id="video-arrow-prev" class="arrow prev"><i class="icon-chevron-left"></i></a>
                    <a href="#" id="video-arrow-next" class="arrow next"><i class="icon-chevron-right"></i></a>
                    <div id="videos-slider">
                        <?php foreach ( $videos_gal as $post ):  ?>
                        <?php setup_postdata ( $post );
                    $videoThumbImg = get_the_post_thumbnail_url();
                    $videoThumbnailID = get_post_thumbnail_ID();
                    $videoAlt = get_post_meta ( $videoThumbnailID, '_wp_attachment_image_alt', true );
                    ?>
                        <div class="slide">
                            <a href="<?php the_permalink(); ?>" class="small-news-area video-type border-radius-m">
                                <div class="photo cover" style="background-image: url(<?php echo $videoThumbImg; ?>);"
                                    title="<?php echo $videoAlt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="content-area">
                                        <div class="play-area modal-trigger" data-id="modal-video-<?php echo $m; ?>"
                                            data-video-url="<?php the_field('id_video_youtube'); ?>">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/play.svg" alt="">
                                        </div>
                                        <div class="post-info">
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php $m++; endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section fotos-gal-area">
    <div class="wrap-xl">
        <div class="content">
            <div class="heading-box-area">
                <h3 class="head-title"><?php the_field( 'titulo_fotos' ); ?></h3>
                <?php $link_todas_fotos = get_field( 'link_todas_fotos' ); ?>
                <?php if ( $link_todas_fotos ) { ?>
                <a href="<?php echo $link_todas_fotos['url']; ?>" target="<?php echo $link_todas_fotos['target']; ?>"
                    class="btn-ver-todas"><span><?php echo $link_todas_fotos['title']; ?></span><i
                        class="icon-chevron-right"></i></a>
                <?php } ?>
            </div>
            <?php $post_object = get_field( 'feat_galeria' ); ?>
            <?php if ( $post_object ): ?>
            <?php $post = $post_object; ?>
            <?php setup_postdata( $post );
            $featGalThumbImg = get_the_post_thumbnail_url();
            $featGalThumbnailID = get_post_thumbnail_ID();
            $featGalAlt = get_post_meta ( $featGalThumbnailID, '_wp_attachment_image_alt', true );
            ?>
            <div class="foto-galeria-feat">
                <div class="small-news-area border-radius-m">
                    <div class="photo cover" style="background-image: url(<?php echo $featGalThumbImg; ?>);"
                        title="<?php echo $featGalAlt; ?>">
                        <div class="veil"></div>
                    </div>
                    <div class="content">
                        <div class="content-area">
                            <div class="post-info">
                                <span class="fecha"><?php the_date(); ?></span>
                                <h3 class="post-title">
                                    <?php the_title(); ?>
                                </h3>
                                <div class="post-excerpt">
                                    <?php the_excerpt(); ?>
                                </div>
                            </div>
                            <div class="button-area">
                                <a href="<?php the_permalink(); ?>"
                                    class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            <div class="otras-gal-slide-area">
                <div class="slider-area">
                    <?php $galerias_foto = get_field( 'galerias_foto' ); ?>
                    <?php if ( $galerias_foto ): ?>
                    <a href="#" id="foto-gal-arrow-prev" class="arrow prev"><i class="icon-chevron-left"></i></a>
                    <a href="#" id="foto-gal-arrow-next" class="arrow next"><i class="icon-chevron-right"></i></a>
                    <div id="fotos-gal-slider">
                        <?php foreach ( $galerias_foto as $post ):  ?>
                        <?php setup_postdata ( $post );
                        $fotosGalThumbImg = get_the_post_thumbnail_url();
                        $fotosGalThumbnailID = get_post_thumbnail_ID();
                        $alt = get_post_meta ( $fotosGalThumbnailID, '_wp_attachment_image_alt', true );
                        ?>
                        <div class="slide">
                            <div class="small-news-area border-radius-m">
                                <div class="photo cover"
                                    style="background-image: url(<?php echo $fotosGalThumbImg; ?>);"
                                    title="<?php echo $alt; ?>">
                                    <div class="veil"></div>
                                </div>
                                <div class="content">
                                    <div class="content-area">
                                        <div class="post-info">
                                            <span class="fecha"><?php the_date(); ?></span>
                                            <h3 class="post-title">
                                                <?php the_title(); ?>
                                            </h3>
                                        </div>
                                        <div class="button-area">
                                            <a href="<?php the_permalink(); ?>"
                                                class="btn is-verde is-rounded"><?php _e('Ver Más', 'ccu-intranet'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                    <?php wp_reset_postdata(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $videos_gal = get_field( 'videos_gal' ); ?>
<?php if ( $videos_gal ): $v = 1; ?>
<?php foreach ( $videos_gal as $post ):  ?>
<?php setup_postdata ( $post ); ?>
<div data-id="modal-video-<?php echo $v; ?>" class="modal">
    <i class="close icon-equis"></i>
    <div class="content-modal contenido wp-content">
        <div class="iframeVideo relative">
        </div>
    </div>
    <div class="modal-background"></div>
</div>
<?php $v++; endforeach; ?>
<?php endif; ?>
<script>
$(document).ready(function() {
    $('#noticias-slider').slick({
        arrows: false,
        dots: false,
        speed: 750
    });
    $('#noticias-arrows .arrow').each(function(index, element) {
        if ($(this).hasClass('prev')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#noticias-slider').slick('slickPrev');
            });

        } else if ($(this).hasClass('next')) {
            $(this).click(function(e) {
                e.preventDefault();
                $('#noticias-slider').slick('slickNext');
            });
        }
    });

    $('#videos-slider').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1
    });
    $('#video-arrow-prev').click(function(e) {
        e.preventDefault();
        $('#videos-slider').slick('slickPrev');
    });
    $('#video-arrow-next').click(function(e) {
        e.preventDefault();
        $('#videos-slider').slick('slickNext');
    });

    $('#fotos-gal-slider').slick({
        arrows: false,
        dots: false,
        speed: 750,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1
    });
    $('#foto-gal-arrow-prev').click(function(e) {
        e.preventDefault();
        $('#fotos-gal-slider').slick('slickPrev');
    });
    $('#foto-gal-arrow-next').click(function(e) {
        e.preventDefault();
        $('#fotos-gal-slider').slick('slickNext');
    });
});
</script>
<?php get_footer(); ?>